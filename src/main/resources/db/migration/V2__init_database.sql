ALTER TABLE _booking_detail
    ADD COLUMN total_price_room_origin numeric(38, 2);

ALTER TABLE _booking_detail
    ADD COLUMN discount double precision;