create table _bed_type
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    name               varchar(255)
);

alter table _bed_type
    owner to postgres;

create table _booking_process
(
    id                      uuid not null
        primary key,
    booking_for_me          boolean,
    business_travel         boolean,
    check_in_date           date,
    check_out_date          date,
    country                 varchar(255),
    electronic_confirm      boolean,
    email                   varchar(255),
    estimated_check_in_time varchar(255),
    first_name              varchar(255),
    full_name               varchar(255),
    hotel_id                uuid,
    jwt_token               varchar(255),
    last_name               varchar(255),
    location                varchar(255),
    order_car               boolean,
    order_taxi              boolean,
    phone_number            varchar(255),
    pick_up_service         boolean,
    quantity_adult          integer,
    quantity_children       integer,
    quantity_room           integer,
    receive_marketing_email boolean,
    special_requirements    varchar(255),
    status                  varchar(255)
);

alter table _booking_process
    owner to postgres;

create table _category
(
    id            uuid not null
        primary key,
    is_deleted    boolean,
    status        varchar(255),
    category_name varchar(255)
);

alter table _category
    owner to postgres;

create table _general_terms
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    content            varchar(255),
    name               varchar(255)
);

alter table _general_terms
    owner to postgres;

create table _group
(
    id                 uuid        not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    group_name         varchar(50) not null
        constraint uk_3p0qr4t8ngnv5e62e7iwan9ft
            unique
);

alter table _group
    owner to postgres;

create table _history_booking
(
    id             uuid not null
        primary key,
    is_deleted     boolean,
    status         varchar(255),
    bed_name       varchar(255),
    booking_code   varchar(255),
    booking_date   date,
    full_name      varchar(255),
    hotel_id       uuid,
    pin_code       varchar(255),
    room_name      varchar(255),
    room_type_name varchar(255),
    total_day      bigint
);

alter table _history_booking
    owner to postgres;

create table _hotel_image_session
(
    id        uuid not null
        primary key,
    jwt_token varchar(255),
    url_image varchar(255)
);

alter table _hotel_image_session
    owner to postgres;

create table _hotel_payment_method
(
    id                    uuid not null
        primary key,
    is_deleted            boolean,
    status                varchar(255),
    created_by            varchar(255),
    created_date          timestamp(6),
    last_modified_by      varchar(255),
    last_modified_date    timestamp(6),
    commission_percentage double precision,
    credit_or_debit_card  boolean,
    invoice_address       varchar(255),
    name_on_invoice       varchar(255)
);

alter table _hotel_payment_method
    owner to postgres;

create table _hotel_payment_method_session
(
    id                     uuid not null
        primary key,
    jwt_token              varchar(255),
    city                   varchar(255),
    country                varchar(255),
    credit_or_debit_card   boolean,
    district_address       varchar(255),
    flag_receiving_address boolean,
    invoice_address        varchar(255),
    name_on_invoice        varchar(255),
    street_address         varchar(255)
);

alter table _hotel_payment_method_session
    owner to postgres;

create table _hotel_policy_session
(
    id                  uuid not null
        primary key,
    jwt_token           varchar(255),
    additional_polices  varchar(255),
    booking_policy      varchar(255),
    cancellation_policy varchar(255),
    pet_policy          varchar(255),
    smoking_policy      varchar(255)
);

alter table _hotel_policy_session
    owner to postgres;

create table _hotel_type
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    name               varchar(255),
    url_image          varchar(255)
);

alter table _hotel_type
    owner to postgres;

create table _permission
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    permission_code    varchar(255)
);

alter table _permission
    owner to postgres;

create table _permission_group
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    group_id           uuid
        constraint fkd29plfuftpgbxsgd8rkrq395b
            references _group,
    permission_id      uuid
        constraint fk5lnux0avm6xeemqtvad4hv8ex
            references _permission
);

alter table _permission_group
    owner to postgres;

create table _policy_confirmation
(
    id                                         uuid not null
        primary key,
    is_deleted                                 boolean,
    status                                     varchar(255),
    created_by                                 varchar(255),
    created_date                               timestamp(6),
    last_modified_by                           varchar(255),
    last_modified_date                         timestamp(6),
    accept_general_terms_privacy               integer,
    certification_of_legal_business_operations integer
);

alter table _policy_confirmation
    owner to postgres;

create table _role
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    role_code          varchar(255),
    role_name          varchar(255)
);

alter table _role
    owner to postgres;

create table _permission_role
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    permission_id      uuid
        constraint fkrangnxtw0y3m6qcyoqoune26a
            references _permission,
    role_id            uuid
        constraint fkanswcq0es5l3aoc2gk7bt2bd
            references _role
);

alter table _permission_role
    owner to postgres;

create table _room_info
(
    id            uuid not null
        primary key,
    jwt_token     varchar(255),
    quantity_room integer,
    room_id       uuid
);

alter table _room_info
    owner to postgres;

create table _room_name
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    name               varchar(255)
);

alter table _room_name
    owner to postgres;

create table _room_session
(
    id               uuid not null
        primary key,
    jwt_token        varchar(255),
    bed_name         varchar(255),
    max_occupancy    integer,
    price_per_night  numeric(38, 2),
    quantity_room    integer,
    room_area        double precision,
    room_name        varchar(255),
    room_name_custom varchar(255),
    room_type_id     uuid
);

alter table _room_session
    owner to postgres;

create table _room_type
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    name               varchar(255)
);

alter table _room_type
    owner to postgres;

create table _safety_security_policy
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    content            varchar(255),
    name               varchar(255)
);

alter table _safety_security_policy
    owner to postgres;

create table _service_amenity_room_id
(
    id                      uuid not null
        primary key,
    jwt_token               varchar(255),
    service_amenity_room_id uuid
);

alter table _service_amenity_room_id
    owner to postgres;

create table _service_and_amenity
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    description        varchar(255),
    name               varchar(255),
    type               varchar(255)
);

alter table _service_and_amenity
    owner to postgres;

create table _user
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    address            varchar(255),
    avatar             varchar(255),
    dob                date,
    email              varchar(255),
    first_name         varchar(255),
    gender             varchar(255),
    last_name          varchar(255),
    password           varchar(255),
    phone_number       varchar(255)
);

alter table _user
    owner to postgres;

create table _confirmation_token
(
    id           uuid         not null
        primary key,
    is_deleted   boolean,
    status       varchar(255),
    confirmed_at timestamp(6),
    created_at   timestamp(6) not null,
    expires_at   timestamp(6) not null,
    token        varchar(255) not null,
    user_id      uuid         not null
        constraint fkb6qoy145tam622w526dfsom9f
            references _user
);

alter table _confirmation_token
    owner to postgres;

create table _history_search
(
    id               uuid not null
        primary key,
    is_deleted       boolean,
    status           varchar(255),
    adults           integer,
    check_in_search  timestamp(6),
    check_out_search timestamp(6),
    children         integer,
    location_search  varchar(255),
    room             integer,
    user_id          uuid
        constraint fk20mq2a7yi6va48nbvlx5gy2e0
            references _user
);

alter table _history_search
    owner to postgres;

create table _hotel
(
    id                      uuid not null
        primary key,
    is_deleted              boolean,
    status                  varchar(255),
    created_by              varchar(255),
    created_date            timestamp(6),
    last_modified_by        varchar(255),
    last_modified_date      timestamp(6),
    activation_date         date,
    city                    varchar(255),
    contact_person          varchar(255),
    count_view              integer,
    country                 varchar(255),
    description             varchar(255),
    district_address        varchar(255),
    name                    varchar(255),
    phone_number            varchar(255),
    phone_number_two        varchar(255),
    postal_code             varchar(255),
    rating                  double precision,
    street_address          varchar(255),
    hotel_payment_method_id uuid
        constraint fks9sguhf88kbvhn1hqsdsbdkxe
            references _hotel_payment_method,
    hotel_type_id           uuid
        constraint fkh5pcetv3ucoejb1br0mqmmjcp
            references _hotel_type,
    policy_confirmation_id  uuid
        constraint fkojwgh42elp1wkwl07jttp7bpe
            references _policy_confirmation,
    user_id                 uuid
        constraint fk1jek29kalrs0nec97nvpfyd0u
            references _user
);

alter table _hotel
    owner to postgres;

create table _billing_address
(
    id          uuid not null
        primary key,
    is_deleted  boolean,
    status      varchar(255),
    address     varchar(255),
    city        varchar(255),
    country     varchar(255),
    postal_code varchar(255),
    state       varchar(255),
    hotel_id    uuid
        constraint fktmrjvbckgms135eus47jpaold
            references _hotel,
    user_id     uuid
        constraint fkedouakr4em46jqfrjfcglevw1
            references _user
);

alter table _billing_address
    owner to postgres;

create table _booking
(
    id                      uuid not null
        primary key,
    is_deleted              boolean,
    status                  varchar(255),
    address                 varchar(255),
    booking_code            varchar(255),
    booking_date            date,
    booking_for_me          boolean,
    business_travel         boolean,
    check_in_date           date,
    check_out_date          date,
    country                 varchar(255),
    electronic_confirm      boolean,
    email                   varchar(255),
    estimated_check_in_time varchar(255),
    first_name              varchar(255),
    full_name               varchar(255),
    last_name               varchar(255),
    location                varchar(255),
    order_car               boolean,
    order_taxi              boolean,
    phone_number            varchar(255),
    pick_up_service         boolean,
    pin_code                varchar(255),
    quantity_adult          integer,
    quantity_children       integer,
    quantity_room           integer,
    receive_marketing_email boolean,
    special_requirements    varchar(255),
    total_price             numeric(38, 2),
    hotel_id                uuid
        constraint fkq7hb3mb1mhxct5yfgr3m814uh
            references _hotel,
    user_id                 uuid
        constraint fkevlb9fwo6einjj7nyrp4xhws6
            references _user
);

alter table _booking
    owner to postgres;

create table _hotel_favourite
(
    id         uuid not null
        primary key,
    is_deleted boolean,
    status     varchar(255),
    hotel_id   uuid
        constraint fkoxj1yi7r109bllv2racvewyv7
            references _hotel,
    user_id    uuid
        constraint fk6an2x5miiuc1n1xa5rojroha3
            references _user
);

alter table _hotel_favourite
    owner to postgres;

create table _hotel_image
(
    id          uuid not null
        primary key,
    is_deleted  boolean,
    status      varchar(255),
    upload_date timestamp(6),
    url_image   varchar(255),
    hotel_id    uuid
        constraint fk2uvk9iki7kik3660nedqvvd2m
            references _hotel
);

alter table _hotel_image
    owner to postgres;

create table _hotel_policy
(
    id                  uuid not null
        primary key,
    is_deleted          boolean,
    status              varchar(255),
    additional_polices  varchar(255),
    booking_policy      varchar(255),
    cancellation_policy varchar(255),
    pet_policy          varchar(255),
    smoking_policy      varchar(255),
    hotel_id            uuid
        constraint fkw4deh42skniaonvkji7fx6nf
            references _hotel
);

alter table _hotel_policy
    owner to postgres;

create table _payment
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    payment_amount     numeric(38, 2),
    payment_date       timestamp(6),
    payment_method     varchar(255),
    billing_address_id uuid
        constraint fk4hid4xcsy38ff1qdrgrm2721r
            references _billing_address,
    booking_id         uuid
        constraint fk9ftwird242u9qdotigelqixf7
            references _booking
);

alter table _payment
    owner to postgres;

create table _promotion
(
    id               uuid not null
        primary key,
    is_deleted       boolean,
    status           varchar(255),
    description      varchar(255),
    discount_percent double precision,
    end_date         date,
    name             varchar(255),
    start_date       date,
    hotel_id         uuid
        constraint fk1evb21wf5qxxwauh0lxkaix57
            references _hotel
);

alter table _promotion
    owner to postgres;

create table _reservation_detail
(
    id                                   uuid not null
        primary key,
    is_deleted                           boolean,
    status                               varchar(255),
    booking_date                         date,
    booking_for                          boolean,
    business_travel                      boolean,
    car_rental                           boolean,
    country                              varchar(255),
    electronic_confirmation              boolean,
    email                                varchar(255),
    estimated_checkin_time               varchar(255),
    first_name                           varchar(255),
    floor_option                         integer,
    full_name                            varchar(255),
    last_name                            varchar(255),
    phone_number                         varchar(255),
    pickup_service                       boolean,
    receive_promotional_emails           boolean,
    receive_promotional_emails_transport boolean,
    special_requirements                 varchar(255),
    taxi_booking                         boolean,
    user_id                              uuid
        constraint fk586i20de562djnyx9t8jc7uin
            references _user
);

alter table _reservation_detail
    owner to postgres;

create table _revenue
(
    id             uuid not null
        primary key,
    is_deleted     boolean,
    status         varchar(255),
    discount       numeric(38, 2),
    profit         numeric(38, 2),
    revenue_amount numeric(38, 2),
    statistic_day  timestamp(6),
    booking_id     uuid
        constraint fk1hb2lu3jn7uqdy3ydlh922q9b
            references _booking
);

alter table _revenue
    owner to postgres;

create table _review
(
    id             uuid not null
        primary key,
    is_deleted     boolean,
    status         varchar(255),
    bed_name       varchar(255),
    booking_date   date,
    full_name      varchar(255),
    rating         double precision,
    review_comment varchar(512),
    review_date    timestamp(6),
    room_name      varchar(255),
    room_type_name varchar(255),
    total_day      integer,
    hotel_id       uuid
        constraint fk4gf6ip4q929m7hpb1dexiadlq
            references _hotel,
    user_id        uuid
        constraint fkn71flt29sqqalbhlw0t8rd89y
            references _user
);

alter table _review
    owner to postgres;

create table _feedback
(
    id               uuid not null
        primary key,
    is_deleted       boolean,
    status           varchar(255),
    feedback_content varchar(512),
    feedback_date    timestamp(6),
    review_id        uuid
        constraint fkmx7jpbwef3pxrd42usamur6ob
            references _review
);

alter table _feedback
    owner to postgres;

create table _review_category
(
    id          uuid not null
        primary key,
    is_deleted  boolean,
    status      varchar(255),
    category_id uuid
        constraint fksyvpb6abhcesfhdqoiohjan2e
            references _category,
    review_id   uuid
        constraint fkby6xmk37a1swc50p79fkg5omg
            references _review
);

alter table _review_category
    owner to postgres;

create table _room
(
    id               uuid not null
        primary key,
    is_deleted       boolean,
    status           varchar(255),
    bed_name         varchar(255),
    max_occupancy    integer,
    price_per_night  numeric(10, 2),
    quantity_room    integer,
    room_area        double precision,
    room_name        varchar(255),
    room_name_custom varchar(255),
    room_number      varchar(255),
    sale_date        date,
    hotel_id         uuid
        constraint fk4ux9o2prrtwfquupd8wfo5bnj
            references _hotel,
    room_type_id     uuid
        constraint fko0befkf6cmsga0ln1gre2jnmk
            references _room_type
);

alter table _room
    owner to postgres;

create table _booking_detail
(
    id                    uuid not null
        primary key,
    is_deleted            boolean,
    status                varchar(255),
    booking_code          varchar(255),
    check_in_date         date,
    check_out_date        date,
    pin_code              varchar(255),
    quantity_room_booking integer,
    total_day             bigint,
    total_price_room      numeric(38, 2),
    booking_id            uuid
        constraint fkgxctlhfcosffsaq0ja8egj8f6
            references _booking,
    room_id               uuid
        constraint fkrr87ab909efai30u2y1qpjmim
            references _room
);

alter table _booking_detail
    owner to postgres;

create table _service_and_amenity_room
(
    id                      uuid not null
        primary key,
    is_deleted              boolean,
    status                  varchar(255),
    children_old            varchar(255),
    children_sleep_in_cribs boolean,
    extra_bed               integer,
    price_guest_adults      numeric(38, 2),
    price_guest_children    numeric(38, 2),
    type_of_guest_adults    boolean,
    type_of_guest_children  boolean,
    room_id                 uuid
        constraint fk3ddvggowg6w6s1jfbtn53a9x0
            references _room,
    service_and_amenity_id  uuid
        constraint fkjufqcyeoy5h7oen1apsap9ubk
            references _service_and_amenity
);

alter table _service_and_amenity_room
    owner to postgres;

create table _staff_schedule
(
    id              uuid not null
        primary key,
    is_deleted      boolean,
    status          varchar(255),
    shift_end_time  timestamp(6),
    start_date_time timestamp(6),
    work_date       timestamp(6),
    user_id         uuid
        constraint fkniqsmhsp3jmvfqow648brqant
            references _user
);

alter table _staff_schedule
    owner to postgres;

create table _token
(
    id         uuid not null
        primary key,
    is_deleted boolean,
    status     varchar(255),
    expired    boolean,
    revoked    boolean,
    token      varchar(255),
    token_type varchar(255),
    _user_id   uuid
        constraint fksiq5y79q8no3cmkd73ouv7vc5
            references _user
);

alter table _token
    owner to postgres;

create table _user_role
(
    id                 uuid not null
        primary key,
    is_deleted         boolean,
    status             varchar(255),
    created_by         varchar(255),
    created_date       timestamp(6),
    last_modified_by   varchar(255),
    last_modified_date timestamp(6),
    role_id            uuid
        constraint fkdx8tn9uhur0t4yx3awxc99q1k
            references _role,
    user_id            uuid
        constraint fksa99se25erlgqmbdawypj2fiw
            references _user
);

alter table _user_role
    owner to postgres;

create table hotel_session
(
    id               uuid not null
        primary key,
    jwt_token        varchar(255),
    city             varchar(255),
    contact_person   varchar(255),
    country          varchar(255),
    district_address varchar(255),
    hotel_type_id    uuid,
    name             varchar(255),
    phone_number     varchar(255),
    phone_number_two varchar(255),
    postal_code      varchar(255),
    rating           double precision,
    street_address   varchar(255)
);

alter table hotel_session
    owner to postgres;

create table service_amenity_room_session
(
    id                      uuid not null
        primary key,
    jwt_token               varchar(255),
    children_old            varchar(255),
    children_sleep_in_cribs boolean,
    extra_bed               integer,
    price_guest_adults      numeric(38, 2),
    price_guest_children    numeric(38, 2),
    type_of_guest_adults    boolean,
    type_of_guest_children  boolean
);

alter table service_amenity_room_session
    owner to postgres;

CREATE
EXTENSION IF NOT EXISTS pgcrypto;

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO _group(id, created_date, created_by, last_modified_date, last_modified_by, group_name,
                   is_deleted, description, STATUS)
VALUES ('7470e5bf-3c77-4a35-8f2f-04d81cebb33b', NOW(), 'ADMIN', NOW(), 'ADMIN', 'ADMIN_SUPER', false,
        'Permission group demo!..', 'ACTIVE'),
       ('b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_HOTEL', false,
        'Permission group demo!..', 'ACTIVE'),
       ('796fc401-c794-4709-97fd-9464711c0376', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_ROOM', false,
        'Permission group demo!..', 'ACTIVE'),
       ('369fb5b1-2639-4360-92fe-24b0d0f735ab', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_GUEST', false,
        'Permission group demo!..', 'ACTIVE'),
       ('11e00073-2084-4606-8989-a461aa348c9c', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_CONTENT', false,
        'Permission group demo!..', 'ACTIVE'),
       ('acf4f01f-3c2b-4cd9-aa69-ae12ba4a49ad', NOW(), 'ADMIN', NOW(), 'ADMIN', 'ADMIN_MASTER', false,
        'Permission group demo!..', 'ACTIVE'),
       ('56b0aeb2-e396-4321-acaf-195e1b16de9c', NOW(), 'ADMIN', NOW(), 'ADMIN', 'ADMIN', false,
        'Permission group demo!..', 'ACTIVE'),
       ('43c025db-5407-4440-beb3-1bc8fc78b226', NOW(), 'ADMIN', NOW(), 'ADMIN', 'HOTEL_OWNER', false,
        'Permission group demo!..', 'ACTIVE'),
       ('cdf97b05-4681-4c79-aa1a-769439a772d9', NOW(), 'ADMIN', NOW(), 'ADMIN', 'GUEST', false,
        'Permission group demo!..', 'ACTIVE');


INSERT INTO _permission(id, created_date, created_by, last_modified_date, last_modified_by, permission_code, is_deleted,
                        description, status)
VALUES ('d7c6cae8-d327-4820-9c21-e8ee6e7009f9', NOW(), 'ADMIN', NOW(), 'ADMIN', 'admin_master', false,
        'Permission demo!..', 'ACTIVE'),
       ('d7c6cae8-d327-4820-9c21-e8ee6e7009f8', NOW(), 'ADMIN', NOW(), 'ADMIN', 'admin_read', false,
        'Permission demo!..', 'ACTIVE'),
       ('fdd2e876-b4ff-4a92-899d-03e3a4e46a12', NOW(), 'ADMIN', NOW(), 'ADMIN', 'admin_write', false,
        'Permission demo!..', 'ACTIVE'),
       ('5f3b01f8-8e0c-4cbd-916f-493bea65d80b', NOW(), 'ADMIN', NOW(), 'ADMIN', 'admin_update', false,
        'Permission demo!..', 'ACTIVE'),
       ('8a4d25a8-ddbb-4ff0-a432-6ab76c4a5c46', NOW(), 'ADMIN', NOW(), 'ADMIN', 'admin_delete', false,
        'Permission demo!..', 'ACTIVE'),
       ('67f9cb1d-2f35-4b5c-9764-3897f66d36da', NOW(), 'ADMIN', NOW(), 'ADMIN', 'guest_read', false,
        'Permission demo!..', 'ACTIVE'),
       ('3cfd8a31-b5e2-4895-9157-ff0f51effa61', NOW(), 'ADMIN', NOW(), 'ADMIN', 'guest_write', false,
        'Permission demo!..', 'ACTIVE'),
       ('93577141-49b5-4cc9-a978-2bf9b527e6c8', NOW(), 'ADMIN', NOW(), 'ADMIN', 'guest_update', false,
        'Permission demo!..', 'ACTIVE'),
       ('7cd907ed-4a19-4105-8425-4d77ee0bd52d', NOW(), 'ADMIN', NOW(), 'ADMIN', 'guest_delete', false,
        'Permission demo!..', 'ACTIVE'),
       ('a8c9d851-7355-4d45-89c2-f91626e31e36', NOW(), 'ADMIN', NOW(), 'ADMIN', 'content_read', false,
        'Permission demo!..', 'ACTIVE'),
       ('40759665-1e68-4df8-817a-a11c63b67b8e', NOW(), 'ADMIN', NOW(), 'ADMIN', 'content_write', false,
        'Permission demo!..', 'ACTIVE'),
       ('f4fac41a-a6c0-45db-b7fa-7c9fb9fe75b4', NOW(), 'ADMIN', NOW(), 'ADMIN', 'content_update', false,
        'Permission demo!..', 'ACTIVE'),
       ('5e2f1944-caab-4b1d-851a-4dafdc7312fd', NOW(), 'ADMIN', NOW(), 'ADMIN', 'content_delete', false,
        'Permission demo!..', 'ACTIVE'),
       ('c0993a84-3b0b-4ac7-9518-75607e1ae306', NOW(), 'ADMIN', NOW(), 'ADMIN', 'manager_image', false,
        'Permission demo!..', 'ACTIVE'),
       ('86aa25ca-9719-414d-8b7f-2918a6e67da9', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_create_info', false,
        'Permission demo!..', 'ACTIVE'),
       ('c7dac939-a532-427d-89ec-f706e1bb11d4', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_view_info', false,
        'Permission demo!..', 'ACTIVE'),
       ('3f77537c-035a-4183-ae88-e4d313901dea', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_update_info', false,
        'Permission demo!..', 'ACTIVE'),
       ('97866e57-e6a7-41f6-baee-ea32282d3773', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_create_contact', false,
        'Permission demo!..', 'ACTIVE'),
       ('6b2bcff6-89fe-4854-8d7c-3768b6ffd959', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_view_contact', false,
        'Permission demo!..', 'ACTIVE'),
       ('b48e280c-ef93-4a47-bd15-a36f8012379e', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_update_contact', false,
        'Permission demo!..', 'ACTIVE'),
       ('0967b0d4-db29-449c-9f6d-3b5c6c87aa39', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_create_info', false,
        'Permission demo!..', 'ACTIVE'),
       ('04d289fa-509f-4847-9517-3167a9fc9304', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_view_info', false,
        'Permission demo!..', 'ACTIVE'),
       ('efe9d590-d2d0-469f-80f3-579c7f065ce8', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_update_info', false,
        'Permission demo!..', 'ACTIVE'),
       ('a7f4a7dc-60e1-4660-adcb-871d80d06f6c', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_delete', false,
        'Permission demo!..', 'ACTIVE'),
       ('a5e15c8a-a7da-42f8-8371-78d9c6608e2f', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_update_status', false,
        'Permission demo!..', 'ACTIVE'),
       ('18755e62-93fc-4048-9df8-d7bac7da893c', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_create_policy', false,
        'Permission demo!..', 'ACTIVE'),
       ('14300eda-cc47-4069-b3de-042de1741e64', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_update_policy', false,
        'Permission demo!..', 'ACTIVE'),
       ('be5a7090-a89c-4095-b5ff-0ed198453078', NOW(), 'ADMIN', NOW(), 'ADMIN', 'room_view_policy', false,
        'Permission demo!..', 'ACTIVE'),
       ('0df3b90e-57db-4c92-a1ca-2145fc5b6ad2', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_update_status', false,
        'Permission demo!..', 'ACTIVE'),
       ('458d8f72-cbd4-466c-ac16-eca0957e3231', NOW(), 'ADMIN', NOW(), 'ADMIN', 'admin_admin', false,
        'Permission demo!..', 'ACTIVE'),
       ('dc594e18-908f-44c8-a686-8f9dd3d31725', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hotel_owner', false,
        'Permission demo!..', 'ACTIVE'),
       ('79c61e11-1f6d-4ef4-b771-27c9f9d853c9', NOW(), 'ADMIN', NOW(), 'ADMIN', 'user_read', false,
        'Permission demo!..', 'ACTIVE');

INSERT INTO _permission_group(id, created_date, created_by, last_modified_date, last_modified_by, group_id,
                              permission_id)
VALUES ('4cf6dc71-87da-49f4-92ae-34c9c3221b20', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        '86aa25ca-9719-414d-8b7f-2918a6e67da9'),
       ('0fde97b3-f120-4750-b2d3-01c9f6e0be44', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        'c7dac939-a532-427d-89ec-f706e1bb11d4'),
       ('147d6840-ea79-499f-90af-53d1e290d8d8', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        '3f77537c-035a-4183-ae88-e4d313901dea'),
       ('c959fdbf-18c7-4f49-b7d1-6cc366cff662', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        '97866e57-e6a7-41f6-baee-ea32282d3773'),
       ('092ec6f5-8811-44be-811a-c810b828e3a3', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        '6b2bcff6-89fe-4854-8d7c-3768b6ffd959'),
       ('c9164b3e-e2a7-4caa-b291-c362d1db9e65', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        'b48e280c-ef93-4a47-bd15-a36f8012379e'),
       ('304b8214-4b28-4d38-b049-0f3d237ae89e', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        'c0993a84-3b0b-4ac7-9518-75607e1ae306'),
       ('1ce12867-f62b-4e13-ad7f-0b9e4808a081', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b4b5b7ae-25b6-4fbe-9e1d-3226ba7db190',
        '0df3b90e-57db-4c92-a1ca-2145fc5b6ad2'),
       ('8b125073-182a-4447-96a7-bae89e03ac6b', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        '0967b0d4-db29-449c-9f6d-3b5c6c87aa39'),
       ('0187f501-9b6e-4fad-8010-c96c1e579a5b', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        '04d289fa-509f-4847-9517-3167a9fc9304'),
       ('eab1d965-1ec9-4cc5-a80e-218148879266', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        'efe9d590-d2d0-469f-80f3-579c7f065ce8'),
       ('9d8ab244-18d8-48f5-ac2e-9ed6a59f53bc', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        'a7f4a7dc-60e1-4660-adcb-871d80d06f6c'),
       ('746a35d8-e60b-4559-820f-c6e81c9aafbd', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        'a5e15c8a-a7da-42f8-8371-78d9c6608e2f'),
       ('5cea5083-7207-4ae7-929a-516f256d4e17', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        '18755e62-93fc-4048-9df8-d7bac7da893c'),
       ('e98e3435-5d9f-4dcb-bbe7-f489f728e2c5', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        '14300eda-cc47-4069-b3de-042de1741e64'),
       ('0791346b-0c7c-493f-aae5-fb0be21773c0', NOW(), 'ADMIN', NOW(), 'ADMIN', '796fc401-c794-4709-97fd-9464711c0376',
        'be5a7090-a89c-4095-b5ff-0ed198453078'),
       ('9c8caa04-5034-4843-8286-e9f8ae7cd880', NOW(), 'ADMIN', NOW(), 'ADMIN', '369fb5b1-2639-4360-92fe-24b0d0f735ab',
        '67f9cb1d-2f35-4b5c-9764-3897f66d36da'),
       ('5f6f9e33-9ef3-4514-8fc4-4ad37ae2a633', NOW(), 'ADMIN', NOW(), 'ADMIN', '369fb5b1-2639-4360-92fe-24b0d0f735ab',
        '3cfd8a31-b5e2-4895-9157-ff0f51effa61'),
       ('fafa93fc-e8c7-46b6-ad56-5671053311f7', NOW(), 'ADMIN', NOW(), 'ADMIN', '369fb5b1-2639-4360-92fe-24b0d0f735ab',
        '93577141-49b5-4cc9-a978-2bf9b527e6c8'),
       ('e07b1d29-29b8-41f6-ada7-eca84c013845', NOW(), 'ADMIN', NOW(), 'ADMIN', '369fb5b1-2639-4360-92fe-24b0d0f735ab',
        '7cd907ed-4a19-4105-8425-4d77ee0bd52d'),
       ('79d70118-bbd4-4fbc-81fa-a532247ac44b', NOW(), 'ADMIN', NOW(), 'ADMIN', '11e00073-2084-4606-8989-a461aa348c9c',
        'a8c9d851-7355-4d45-89c2-f91626e31e36'),
       ('681c78ac-0133-4d7f-acdb-77185dfc7be9', NOW(), 'ADMIN', NOW(), 'ADMIN', '11e00073-2084-4606-8989-a461aa348c9c',
        '40759665-1e68-4df8-817a-a11c63b67b8e'),
       ('54facf73-1799-432e-a701-c40cc4ba9e40', NOW(), 'ADMIN', NOW(), 'ADMIN', '11e00073-2084-4606-8989-a461aa348c9c',
        'f4fac41a-a6c0-45db-b7fa-7c9fb9fe75b4'),
       ('f5a4eb88-93fb-4dac-9f9f-0f1b203970b9', NOW(), 'ADMIN', NOW(), 'ADMIN', '11e00073-2084-4606-8989-a461aa348c9c',
        '5e2f1944-caab-4b1d-851a-4dafdc7312fd'),
       ('199c272c-1c18-4742-8f5b-0e75c82021b7', NOW(), 'ADMIN', NOW(), 'ADMIN', '56b0aeb2-e396-4321-acaf-195e1b16de9c',
        'd7c6cae8-d327-4820-9c21-e8ee6e7009f8'),
       ('68e472cf-9cc6-496f-8340-ef64c5594145', NOW(), 'ADMIN', NOW(), 'ADMIN', '56b0aeb2-e396-4321-acaf-195e1b16de9c',
        'fdd2e876-b4ff-4a92-899d-03e3a4e46a12'),
       ('b2a3a82d-ba55-4c00-9d32-89d5eba118e2', NOW(), 'ADMIN', NOW(), 'ADMIN', '56b0aeb2-e396-4321-acaf-195e1b16de9c',
        '5f3b01f8-8e0c-4cbd-916f-493bea65d80b'),
       ('0d225bec-399a-43bc-909f-51b591ae2be6', NOW(), 'ADMIN', NOW(), 'ADMIN', '56b0aeb2-e396-4321-acaf-195e1b16de9c',
        '8a4d25a8-ddbb-4ff0-a432-6ab76c4a5c46'),
       ('82191201-667c-4867-91f7-cb16e4c97651', NOW(), 'ADMIN', NOW(), 'ADMIN', '7470e5bf-3c77-4a35-8f2f-04d81cebb33b',
        'd7c6cae8-d327-4820-9c21-e8ee6e7009f9'),
       ('84eba5c6-be2d-4f5f-b287-7dee9adce85b', NOW(), 'ADMIN', NOW(), 'ADMIN', 'acf4f01f-3c2b-4cd9-aa69-ae12ba4a49ad',
        '458d8f72-cbd4-466c-ac16-eca0957e3231'),
       ('ade9d7f1-0689-4adc-9913-c10c08038190', NOW(), 'ADMIN', NOW(), 'ADMIN', '43c025db-5407-4440-beb3-1bc8fc78b226',
        'dc594e18-908f-44c8-a686-8f9dd3d31725'),
       ('c69b93d7-34ff-49e4-a506-b6b48b7e81f8', NOW(), 'ADMIN', NOW(), 'ADMIN', 'cdf97b05-4681-4c79-aa1a-769439a772d9',
        '79c61e11-1f6d-4ef4-b771-27c9f9d853c9');

INSERT INTO _role(id, created_date, created_by, last_modified_date, last_modified_by, role_name, role_code, is_deleted,
                  status)
VALUES ('be5a7090-a89c-4095-b5ff-0ed198453078', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_HOTEL', 'manager_hotel', false,
        'ACTIVE'),
       ('33d6fcb3-66a4-4996-badb-5fc071c2510b', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_GUEST', 'manager_guest', false,
        'ACTIVE'),
       ('42f807f8-4506-4835-aeee-05b181593344', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_ROOM', 'manager_room', false,
        'ACTIVE'),
       ('51dc5a66-98e1-4d88-84da-824df090e653', NOW(), 'ADMIN', NOW(), 'ADMIN', 'MANAGER_CONTENT', 'manager_content',
        false,
        'ACTIVE'),
       ('693ce4f5-698f-4058-ac4c-ea16281f43ce', NOW(), 'ADMIN', NOW(), 'ADMIN', 'ADMIN_SUPER', 'admin_master', false,
        'ACTIVE'),
       ('d3940dfd-c678-4f60-a6f2-3283bb3e2435', NOW(), 'ADMIN', NOW(), 'ADMIN', 'ADMIN_ADMIN', 'admin_admin', false,
        'ACTIVE'),
       ('61ded9df-0002-489c-950a-13ccbbdae691', NOW(), 'ADMIN', NOW(), 'ADMIN', 'ADMIN', 'admin', false,
        'ACTIVE'),
       ('c81465b3-0f1c-4d1c-9ba7-f8f08f57ca08', NOW(), 'ADMIN', NOW(), 'ADMIN', 'HOTEL_OWNER', 'hotel_owner', false,
        'ACTIVE'),
       ('e464a5e6-9235-4b80-b113-6bdd5401345a', NOW(), 'ADMIN', NOW(), 'ADMIN', 'GUEST', 'guest', false,
        'ACTIVE');


INSERT INTO _permission_role(id, created_date, created_by, last_modified_date, last_modified_by, permission_id, role_id,
                             is_deleted, status)
VALUES ('4cae89da-01c6-4a99-83f8-8101fbca01f3', NOW(), 'ADMIN', NOW(), 'ADMIN', 'c0993a84-3b0b-4ac7-9518-75607e1ae306',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('0435e3ed-c915-4495-8e34-14e4a7aeedd2', NOW(), 'ADMIN', NOW(), 'ADMIN', '86aa25ca-9719-414d-8b7f-2918a6e67da9',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('9842eeff-54ab-405f-85b8-77557a5e23ff', NOW(), 'ADMIN', NOW(), 'ADMIN', 'c7dac939-a532-427d-89ec-f706e1bb11d4',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('953f80fa-5316-4326-b74c-904d1a04b760', NOW(), 'ADMIN', NOW(), 'ADMIN', '3f77537c-035a-4183-ae88-e4d313901dea',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('f666e2c7-61d3-486a-a2fa-f39b22522138', NOW(), 'ADMIN', NOW(), 'ADMIN', '97866e57-e6a7-41f6-baee-ea32282d3773',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('dbb72cbe-b952-42a7-a711-28df4b90c533', NOW(), 'ADMIN', NOW(), 'ADMIN', '6b2bcff6-89fe-4854-8d7c-3768b6ffd959',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('27860906-4531-457c-89c2-c531a6f44a17', NOW(), 'ADMIN', NOW(), 'ADMIN', 'b48e280c-ef93-4a47-bd15-a36f8012379e',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('7f720e7c-4eb0-4036-a3d2-b8f4484f0635', NOW(), 'ADMIN', NOW(), 'ADMIN', '0df3b90e-57db-4c92-a1ca-2145fc5b6ad2',
        'be5a7090-a89c-4095-b5ff-0ed198453078', false, 'ACTIVE'),
       ('ea8c0d07-1381-4ee7-82f3-b8d519f70ba7', NOW(), 'ADMIN', NOW(), 'ADMIN', '67f9cb1d-2f35-4b5c-9764-3897f66d36da',
        '33d6fcb3-66a4-4996-badb-5fc071c2510b', false, 'ACTIVE'),
       ('491ff965-7fbb-46cc-8085-eea912ca6f85', NOW(), 'ADMIN', NOW(), 'ADMIN', '3cfd8a31-b5e2-4895-9157-ff0f51effa61',
        '33d6fcb3-66a4-4996-badb-5fc071c2510b', false, 'ACTIVE'),
       ('766bae58-1efb-4e45-9a5f-03b2f20059d4', NOW(), 'ADMIN', NOW(), 'ADMIN', '93577141-49b5-4cc9-a978-2bf9b527e6c8',
        '33d6fcb3-66a4-4996-badb-5fc071c2510b', false, 'ACTIVE'),
       ('67f5827e-845d-4507-aa87-1bf61aa22285', NOW(), 'ADMIN', NOW(), 'ADMIN', '7cd907ed-4a19-4105-8425-4d77ee0bd52d',
        '33d6fcb3-66a4-4996-badb-5fc071c2510b', false, 'ACTIVE'),
       ('9d09176e-1c1f-4e00-9880-9ef7152ebe00', NOW(), 'ADMIN', NOW(), 'ADMIN', '0967b0d4-db29-449c-9f6d-3b5c6c87aa39',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('23dcaead-6adf-4a19-84cf-c411d1d48cd1', NOW(), 'ADMIN', NOW(), 'ADMIN', '04d289fa-509f-4847-9517-3167a9fc9304',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('853cd3e8-7135-4390-8116-2ec94894dcc1', NOW(), 'ADMIN', NOW(), 'ADMIN', 'efe9d590-d2d0-469f-80f3-579c7f065ce8',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('12e9768e-2657-47a8-902c-f2a14ea18ca9', NOW(), 'ADMIN', NOW(), 'ADMIN', 'a7f4a7dc-60e1-4660-adcb-871d80d06f6c',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('f633ae89-89db-4b9a-9b58-7caf3e5a4d69', NOW(), 'ADMIN', NOW(), 'ADMIN', 'a5e15c8a-a7da-42f8-8371-78d9c6608e2f',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('d6315847-0a2b-4a5f-bc22-ad03df4c46e6', NOW(), 'ADMIN', NOW(), 'ADMIN', '18755e62-93fc-4048-9df8-d7bac7da893c',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('95e1a40d-d387-4f90-ac84-5405e486a83c', NOW(), 'ADMIN', NOW(), 'ADMIN', '14300eda-cc47-4069-b3de-042de1741e64',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('aeababc6-9e5a-426d-a843-c62d1e2c6629', NOW(), 'ADMIN', NOW(), 'ADMIN', 'be5a7090-a89c-4095-b5ff-0ed198453078',
        '42f807f8-4506-4835-aeee-05b181593344', false, 'ACTIVE'),
       ('c41df7c8-dbe8-4eb7-9b65-96e8a41a3098', NOW(), 'ADMIN', NOW(), 'ADMIN', 'a8c9d851-7355-4d45-89c2-f91626e31e36',
        '51dc5a66-98e1-4d88-84da-824df090e653', false, 'ACTIVE'),
       ('76b721c8-2085-4e8b-8aac-b5b12dc83e3b', NOW(), 'ADMIN', NOW(), 'ADMIN', '40759665-1e68-4df8-817a-a11c63b67b8e',
        '51dc5a66-98e1-4d88-84da-824df090e653', false, 'ACTIVE'),
       ('810d62dd-48a4-4079-bb02-9642c7b74ba3', NOW(), 'ADMIN', NOW(), 'ADMIN', 'f4fac41a-a6c0-45db-b7fa-7c9fb9fe75b4',
        '51dc5a66-98e1-4d88-84da-824df090e653', false, 'ACTIVE'),
       ('fa189529-d7a6-499a-9080-e5e56c5d19fa', NOW(), 'ADMIN', NOW(), 'ADMIN', '5e2f1944-caab-4b1d-851a-4dafdc7312fd',
        '51dc5a66-98e1-4d88-84da-824df090e653', false, 'ACTIVE'),
       ('009370ef-bb1c-4ca6-a54c-04df42e3ee24', NOW(), 'ADMIN', NOW(), 'ADMIN', 'd7c6cae8-d327-4820-9c21-e8ee6e7009f9',
        '693ce4f5-698f-4058-ac4c-ea16281f43ce', false, 'ACTIVE'),
       ('94cf8846-c726-4396-9bd8-9ff8ff784b82', NOW(), 'ADMIN', NOW(), 'ADMIN', '458d8f72-cbd4-466c-ac16-eca0957e3231',
        'd3940dfd-c678-4f60-a6f2-3283bb3e2435', false, 'ACTIVE'),
       ('6f5bf8d1-f9a3-4fb0-a0e8-0edc038495c1', NOW(), 'ADMIN', NOW(), 'ADMIN', 'd7c6cae8-d327-4820-9c21-e8ee6e7009f8',
        '61ded9df-0002-489c-950a-13ccbbdae691', false, 'ACTIVE'),
       ('baf2e3fd-cce5-4463-86f3-e0c97869ea61', NOW(), 'ADMIN', NOW(), 'ADMIN', 'fdd2e876-b4ff-4a92-899d-03e3a4e46a12',
        '61ded9df-0002-489c-950a-13ccbbdae691', false, 'ACTIVE'),
       ('db4852a2-26d1-4ef5-8da4-1d0fee790cab', NOW(), 'ADMIN', NOW(), 'ADMIN', '5f3b01f8-8e0c-4cbd-916f-493bea65d80b',
        '61ded9df-0002-489c-950a-13ccbbdae691', false, 'ACTIVE'),
       ('1241a484-5e98-4959-b6db-fb03d96b979a', NOW(), 'ADMIN', NOW(), 'ADMIN', '8a4d25a8-ddbb-4ff0-a432-6ab76c4a5c46',
        '61ded9df-0002-489c-950a-13ccbbdae691', false, 'ACTIVE'),
       ('7d05222b-b456-451d-8f5c-26e9bfbbe3db', NOW(), 'ADMIN', NOW(), 'ADMIN', 'dc594e18-908f-44c8-a686-8f9dd3d31725',
        'c81465b3-0f1c-4d1c-9ba7-f8f08f57ca08', false, 'ACTIVE'),
       ('8b314470-70ce-44fa-84ca-6e2204959b43', NOW(), 'ADMIN', NOW(), 'ADMIN', '79c61e11-1f6d-4ef4-b771-27c9f9d853c9',
        'e464a5e6-9235-4b80-b113-6bdd5401345a', false, 'ACTIVE');

INSERT INTO _user(id, created_date, created_by, last_modified_date, last_modified_by, email, password, first_name,
                  last_name,
                  phone_number, is_deleted, status)
VALUES ('2235b728-1f45-40ae-97af-432e97f6fde6', NOW(), 'ADMIN', NOW(), 'ADMIN', 'hoangduynam20996@gmail.com',
        crypt('123@bBbb', gen_salt('bf', 8)), 'Nam', 'Hoang', '0968997434', false, 'ACTIVE'),
       ('190fd771-0553-40f8-b7cc-4902011027db', NOW(), 'ADMIN', NOW(), 'ADMIN', 'namhdps25979@fpt.edu.vn',
        crypt('123@bBbb', gen_salt('bf', 8)), 'Nam', 'Hoang', '0968997434', false, 'ACTIVE'),
       ('3e712619-3c1b-4a6e-be22-64ad81adff4f', NOW(), 'ADMIN', NOW(), 'ADMIN', 'boldkillbase@gmail.com',
        crypt('123@bBbb', gen_salt('bf', 8)), 'Nam', 'Hoang', '0968997434', false, 'ACTIVE');

INSERT INTO _user_role(id, created_date, created_by, last_modified_date, last_modified_by, user_id, role_id, is_deleted,
                       status)
VALUES ('8d47e4cf-4c17-40b8-97cb-71add1eaf117', NOW(), 'ADMIN', NOW(), 'ADMIN', '2235b728-1f45-40ae-97af-432e97f6fde6',
        '693ce4f5-698f-4058-ac4c-ea16281f43ce', false, 'ACTIVE'),
       ('81f2cce7-6fa3-42cb-b5b2-8e18f90a75a3', NOW(), 'ADMIN', NOW(), 'ADMIN', '190fd771-0553-40f8-b7cc-4902011027db',
        'd3940dfd-c678-4f60-a6f2-3283bb3e2435', false, 'ACTIVE'),
       ('d94f9d8c-b95e-4d9e-ad46-e43693d7002e', NOW(), 'ADMIN', NOW(), 'ADMIN', '3e712619-3c1b-4a6e-be22-64ad81adff4f',
        '61ded9df-0002-489c-950a-13ccbbdae691', false, 'ACTIVE');

INSERT INTO _hotel_type(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                        description, name, url_image)
VALUES ('ebe9449e-065a-4fd1-86fe-213d60775f37', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No description!..',
        'Hotel', 'https://pistachiohotel.com/UploadFile/Gallery/Overview/a2.jpg'),
       ('b34c6ee3-604b-425c-806e-3dbe0ea2c31d', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No description!..',
        'Motel',
        'https://www.lottehotel.com/content/dam/lotte-hotel/lotte/saigon/accommodation/suite/deluxe-suite/4265-14-1920-room-LTHO.jpg.thumb.768.768.jpg'),
       ('11034811-3199-4367-b7aa-7eac4103d29e', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No description!..',
        'Resort biển', 'https://saigontourist.com.vn/files/images/luu-tru/luu-tru-mien-nam/hotel-grand-saigon-1.jpg'),
       ('9d18bb9d-0be1-4663-94de-907aee8b64ab', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No description!..',
        'Homestay',
        'https://image.bnews.vn/MediaUpload/Org/2016/12/05/100203_amoy-boutique-hotel-singapore-03.jpg'),
       ('198cdf07-6ad5-4332-b135-ddff2fff6099', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No description!..',
        'Boutique Hotel',
        'https://owa.bestprice.vn/images/hotels/large/mai-thang-hotel-da-lat-60d69fd04568b-848x477.jpg'),
       ('6d58bb87-5880-4a53-b696-4a68cc1423a8', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No description!..',
        'Villa hoặc Biệt thự nghỉ dưỡng',
        'https://decoxdesign.com/upload/images/cac-loai-hinh-khach-san-03-decox-design.jpg');

INSERT INTO _hotel_payment_method(id, is_deleted, status, created_by, created_date, last_modified_by,
                                  last_modified_date, commission_percentage, credit_or_debit_card, invoice_address,
                                  name_on_invoice)
VALUES ('ad915a26-db1f-41ae-b59b-443287caf7e6', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 15.0, false, 'ADMIN',
        'ADMIN');

INSERT INTO _policy_confirmation(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                                 accept_general_terms_privacy, certification_of_legal_business_operations)
VALUES ('08928054-18e2-4a32-af89-92947c56415b', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 1, 1);

INSERT INTO _hotel(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                   activation_date, city, contact_person, count_view, country, description, district_address, name,
                   phone_number, phone_number_two, postal_code, rating, street_address, user_id,
                   hotel_payment_method_id, hotel_type_id, policy_confirmation_id)
VALUES ('1f0e19f2-6c8b-4da4-a4fb-06577a873ff3', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quan 12', 'The Grand Hyatt', '0968997434', '0968997434', '820000',
        5,
        'Quang trung', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        'ebe9449e-065a-4fd1-86fe-213d60775f37', '08928054-18e2-4a32-af89-92947c56415b'),
       ('cdba71a9-55ba-4a47-8d27-192ac3d7763d', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quan 1', 'Hotel Ritz', '0968997434', '0968997434', '840000', 5,
        '175-177-179 Le Thanh Ton Street', '2235b728-1f45-40ae-97af-432e97f6fde6',
        'ad915a26-db1f-41ae-b59b-443287caf7e6',
        'ebe9449e-065a-4fd1-86fe-213d60775f37', '08928054-18e2-4a32-af89-92947c56415b'),

       ('c3a651e7-7c9d-4cda-9290-3b506b3426ff', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quan 1', 'Hilton Garden Inn', '0968997434', '0968997434', '820000',
        5,
        '138/13 Bùi Thị Xuân', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '11034811-3199-4367-b7aa-7eac4103d29e', '08928054-18e2-4a32-af89-92947c56415b'),
       ('674adc28-e956-4cd4-b4ee-30dbab31b21b', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quan 3', 'Marriott Marquis', '0968997434', '0968997434', '840000',
        5,
        '192 Pasteur Street, Ward 6, District 3', '2235b728-1f45-40ae-97af-432e97f6fde6',
        'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '11034811-3199-4367-b7aa-7eac4103d29e', '08928054-18e2-4a32-af89-92947c56415b'),
       ('4a982b08-3f90-49bf-b99a-a81a228c5e65', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quận Bình Thạnh', 'InterContinental', '0968997434', '0968997434',
        '820000', 5,
        'No. 40/8/3 Pham Viet Chanh Street', '2235b728-1f45-40ae-97af-432e97f6fde6',
        'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '11034811-3199-4367-b7aa-7eac4103d29e', '08928054-18e2-4a32-af89-92947c56415b'),
       ('a2c92d15-262a-4c5c-9b63-0606cfc305fc', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quan 4', 'Sheraton Grand', '0968997434', '0968997434', '840000', 5,
        '132 Đường Bến Vân Đồn', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '11034811-3199-4367-b7aa-7eac4103d29e', '08928054-18e2-4a32-af89-92947c56415b'),
       ('befebaec-5def-4dd4-8fe3-2c68ebbb99eb', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quận Bình Thạnh', 'Four Seasons', '0968997434', '0968997434',
        '820000', 5,
        '720A Đường Điện Biên Phủ', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '11034811-3199-4367-b7aa-7eac4103d29e', '08928054-18e2-4a32-af89-92947c56415b'),
       ('5eff2197-2936-4597-8d34-4eaa6b6abdda', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quan 10', 'The Peninsula', '0968997434', '0968997434', '840000', 5,
        '497/5 Sư Vạn Hạnh, Phường 12', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '198cdf07-6ad5-4332-b135-ddff2fff6099', '08928054-18e2-4a32-af89-92947c56415b'),
       ('1bac80fc-e760-43e9-bbf3-63a3f6701e22', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'TÂN BÌNH', 'Mandarin Oriental', '0968997434', '0968997434',
        '820000', 5,
        '09 CỬU LONG, PHƯỜNG 2', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '198cdf07-6ad5-4332-b135-ddff2fff6099', '08928054-18e2-4a32-af89-92947c56415b'),
       ('492573e8-6bd3-4bb6-a86a-dd3e9c03013e', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), NOW(), 'HCM',
        'Duy Nam', 0, 'Viet Nam', 'No description', 'Quận Gò Vấp', 'Wynn Resorts', '0968997434', '0968997434', '840000',
        5,
        '84 Đường Số 1', '2235b728-1f45-40ae-97af-432e97f6fde6', 'ad915a26-db1f-41ae-b59b-443287caf7e6',
        '198cdf07-6ad5-4332-b135-ddff2fff6099', '08928054-18e2-4a32-af89-92947c56415b');

INSERT INTO _hotel_image(id, is_deleted, status, upload_date, url_image, hotel_id)
VALUES ('17b94ff5-2468-4597-bcdd-5ef7dc5af195', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('0d3ca738-8dbd-47f5-95d6-d91887f5e185', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('e745d6b8-4952-4aff-9954-d4aaab48e29f', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('60338e1f-76dc-40f6-b81f-08ee58aed1d1', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('646b3885-91f0-4de1-b13f-1a72498e81e2', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('1e7b7fcc-13d9-4c85-b460-0f94b9578827', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('6b1346f5-7681-4453-a915-e97ce8d56575', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('3c8f3f1d-5c54-4dc7-a36d-264ae6d2f1e6', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('51ab8817-44e1-4f31-94ed-2362c5bf6bfc', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('ad92e237-a073-49a5-b95c-3552b1fdad3d', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/258154/pexels-photo-258154.jpeg',
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       ('5f851643-1abf-4818-8465-cfe32685ecd3', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('1671f094-69df-4838-948b-2c9576f7ce2c', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('e9cc09c7-351f-4bf0-ba82-01137b158ae2', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('461453fd-41eb-4bf5-9c0d-2b2515c867a9', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('4d1fa1e5-d508-47ca-bc0f-1c01200d2d57', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('0dc0db83-616f-4f1e-9ea6-25011ca2ff6e', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('c61e4624-2d80-4628-b32a-61560f327a38', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('4b007d61-a258-4c96-b0bb-3ab23ceea8a4', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('b38c5698-6659-48c5-8b7b-ae28938bec7b', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       ('c479bac4-626b-49cf-92d4-392b00015b46', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),

       ('09ff15cd-c824-4b8c-8b27-a241df1ab6e9', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('ef57e313-79ad-45ef-a26d-185b5966eb9f', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('dee1eea4-6bf2-49b7-a7a3-3fc648d36171', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('93a9f069-a388-4e5d-bfaf-952e3a61591a', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('25b4bbf0-a89a-4238-b0b1-b84587745110', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('2986f1fd-2a4c-430e-86a7-5f7c9cdeca71', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('5d1b8273-c435-4341-9900-4ea9dc4ef202', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('c2ba224c-832e-439c-a031-536dfa5fd9f3', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('72fa9e66-2e1f-401f-8b05-e1055c1b8450', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       ('6fe5b291-3a12-49a8-8fe7-cd4fe6e7f6b0', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg',
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),

       ('ae442b2c-6e09-4a76-8ec5-7008bb0be21a', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('ac91ba81-c3a1-4f9c-98b2-dc792f2bee44', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('f8836501-b297-4f19-af58-5e3abe310b0c', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('79597d53-90c4-4975-bb48-a1605d37fb96', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('c4c15266-b84d-4b21-be30-19e10a419569', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('a8ff6dd4-0075-4649-8f32-4e0e1fd15ca5', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('9f6a3ebd-11d4-41cd-99c3-2fe45a7b66de', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('cc03a2de-6a86-4c10-949b-0d88aeaa7ab9', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('614d73fe-9452-401d-b527-3c5ffabcb857', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       ('04d4511d-813b-4119-8843-2ba161d9d544', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg',
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),

       ('ddd2ec1c-ab70-4898-9653-d07ec2f1c709', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('9f500368-bd71-4481-9e29-12dd9a74fe38', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('7865e2ef-b416-4492-9e81-790d296e1db4', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('63038fd3-8d46-48ee-8f2d-cab89b565a69', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('61f8d2c9-9f50-4c9e-871b-6fecc277f942', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('86f2b6d1-321a-4748-b238-4a0d75169efd', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('c53b4e01-0127-4d70-a6c0-9f200ee5dcb8', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('3cf6fde8-9773-4121-bb93-df9d321b1e0b', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('d556a3bf-aa35-44f1-8348-2621bda67b8c', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       ('f73aa2e7-729d-4cea-b006-559850667464', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/941861/pexels-photo-941861.jpeg',
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),

       ('a1da34ba-df71-4997-afec-8c3d1d8bc7ab', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('671f6502-3abb-4547-9aab-51f0da5171d5', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('17bbeb6e-0108-4607-a828-cb788c24e469', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('97e5889f-3069-4336-9cb2-1ba23f33956a', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('a58a4ea9-98f8-431e-96f7-f4756aec843e', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('08384295-2c8f-4fbe-a484-9a86d72b9a8d', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('6085c79b-458e-44ea-af6c-741712a1c065', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('e3f5dfef-2633-4565-ac9f-4a2127937923', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('ea5ba44b-e644-4e57-94b9-0a77ad689055', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       ('d5c18df7-86b2-408e-88f3-21684679d3e1', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/775219/pexels-photo-775219.jpeg',
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),

       ('4d47d3c6-6b7a-4eb3-8fb3-4405bc40bfb6', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('fe42d338-5c5e-4ed4-970b-297853187ec0', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('7ed595be-01a5-41d0-bbc6-8635f612bd38', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('a538d195-8fb8-41ab-b04a-a3be2b05d366', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('16ffb830-21c1-4f50-8f53-52f754b804bc', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('2379105b-2c0d-45f9-aa67-8212a95b00c6', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('3b6ad5ba-a2d4-441a-b032-5b378d32662f', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('a5bd0d16-419d-45b4-b33c-a1482ae62724', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('58ae7344-7fd1-4e7e-af07-d0e576c5fda7', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       ('12367a20-3157-4bf6-98f8-4bb12a21288c', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/221457/pexels-photo-221457.jpeg',
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),

       ('d855863f-6149-4b2e-8f01-a4f9583c09bc', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('afd0598c-1d31-4741-b81c-7e52d350fedb', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('866c0f26-80c8-4789-9da1-c41db73134bb', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('74a75f2d-8696-4f18-b9b3-0b3f53f3e4fb', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('b06cb34b-0d28-4c1f-be41-3fef469ce774', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('9a084af6-89f4-4759-b036-36b25c50a55c', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('31a8c107-82cb-4a18-9df1-b5d29d277f70', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('4d529aa8-d740-4543-b2e7-525a673b33c6', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('77d854f8-c0a4-4933-a2b3-ce967b3e9370', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       ('fac1cfba-d582-4d00-b2f2-a22d8bf848a3', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/260922/pexels-photo-260922.jpeg',
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),

       ('e5b064de-3153-441a-8832-ae47ad082e3e', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('ecf69545-a7b8-4c66-b5ab-6a0e4c0266cf', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('d2256c0c-19aa-4700-967a-95e2d20fd583', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('4b5e4de1-8a75-4495-aeec-284264cfcd13', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('d68ace79-1c0f-4158-b7e3-28b91f2f378b', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('59d913a8-b1d8-4b92-935c-0f0a05a0be7a', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('d62ac96f-03ba-4ba4-8314-fe6293789246', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('fdcc9746-ef68-4e06-ac40-cf1d1de5ea0a', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('1eb7b76b-b0ca-455b-97a9-e5df559cb0c2', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       ('9ec3e18d-4b3e-4b68-a9b4-076154b2e517', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/53577/hotel-architectural-tourism-travel-53577.jpeg',
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),

       ('da290e2f-b79e-4d08-851b-8dc3af66672a', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('9d2e830f-2da3-4c54-b7db-ef2f70063f56', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('707ec2d8-fae5-4d77-a9a1-1ae84a31fc2d', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('6606eb32-2302-46da-8511-116b56ed4c9e', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('5dbb9232-9557-4b8d-bdbd-6b7885550d96', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('366271a5-54e7-4614-9d6f-0a1d6a3f3de8', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('49d1fd12-ac48-4077-8174-08e4dd166633', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('79ab05da-e364-4c31-a911-4d1fe04f4829', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('b5f20eaf-3c6c-4365-8435-893ad9f849cb', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e'),
       ('efd4cef4-5cef-4396-84e5-04c1fd211ebc', false, 'ACTIVE', NOW(),
        'https://images.pexels.com/photos/2869215/pexels-photo-2869215.jpeg',
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e');

INSERT INTO _hotel_favourite(id, is_deleted, status, hotel_id, user_id)
VALUES ('31800beb-9215-4d86-95d0-2a71a5feb871', false, 'ACTIVE', 'cdba71a9-55ba-4a47-8d27-192ac3d7763d',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('4c50b227-212e-4ada-a1ee-367c0794a2ff', false, 'ACTIVE', '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('bfa9e812-1fa4-4a6b-8686-f15124636585', false, 'ACTIVE', 'c3a651e7-7c9d-4cda-9290-3b506b3426ff',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('c6667d54-f4b8-4336-b263-c93749839de2', false, 'ACTIVE', '674adc28-e956-4cd4-b4ee-30dbab31b21b',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('20d80a48-507c-4983-b1f9-11071a05fa34', false, 'ACTIVE', '4a982b08-3f90-49bf-b99a-a81a228c5e65',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('5d1eba8f-41b0-45c4-acd5-f375f3ddeec3', false, 'ACTIVE', 'a2c92d15-262a-4c5c-9b63-0606cfc305fc',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('955811db-200f-4207-8eb3-3d40e4815010', false, 'ACTIVE', 'befebaec-5def-4dd4-8fe3-2c68ebbb99eb',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('8fd88cc8-21b2-4105-94f9-e473ad08f2f5', false, 'ACTIVE', '5eff2197-2936-4597-8d34-4eaa6b6abdda',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('45885366-6957-4aac-b43b-dd87eebd75de', false, 'ACTIVE', '1bac80fc-e760-43e9-bbf3-63a3f6701e22',
        '2235b728-1f45-40ae-97af-432e97f6fde6'),
       ('c848d830-e5fc-4e4c-a330-5a620623050c', false, 'ACTIVE', '492573e8-6bd3-4bb6-a86a-dd3e9c03013e',
        '2235b728-1f45-40ae-97af-432e97f6fde6');

INSERT INTO _hotel_policy(id, is_deleted, status, additional_polices, booking_policy, cancellation_policy, pet_policy,
                          smoking_policy, hotel_id)
VALUES (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', 'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', 'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', 'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', 'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       (uuid_generate_v4(), false, 'ACTIVE', 'additionalPolices demo', 'bookingPolicy demo',
        'cancellationPolicy demo', 'petPolicy demo', 'smokingPolicy demo', '492573e8-6bd3-4bb6-a86a-dd3e9c03013e');

INSERT INTO _room_type(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                       description, name)
VALUES ('7810dfc4-8dbe-440d-bff3-673acd3e2828', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Standard'),
       ('ac34ba49-6a0a-40f2-9e0b-b32595704be8', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Deluxe'),
       ('c7b79eef-62d5-42d4-93b4-7c962f1504ef', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Family'),
       ('0669e0ef-a638-427e-9dd9-4635aad4c473', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Themed'),
       ('b54c1f5d-7683-4c54-8e4b-108bba8cd40c', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Studio');

INSERT INTO _room_name(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                       description, name)
VALUES ('e32cd0f5-6972-4bf8-b279-cdb05d10a74a', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Phòng Executive Suite'),
       ('1b6d2dd9-c80e-4513-8d3b-07bae9c9f7d4', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Phòng Deluxe Ocean View'),
       ('86582524-68e1-44f0-b82f-6390e6cd6710', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Phòng Standard Double'),
       ('b7974720-757b-41ab-bee5-62945857f149', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Phòng Family Suite'),
       ('ef923c3d-5623-497c-8b61-05465e654a52', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Phòng Studio Loft');


INSERT INTO _bed_type(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                      description, name)
VALUES ('91548d25-b178-4b79-aa2e-cb800ba5dd95', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Giường đơn (Single bed)'),
       ('95d671b7-a5e7-461a-8695-7cbccc1cac5f', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Giường đôi (Double bed)'),
       ('b84f96a9-0739-4ecb-acfe-7976f7b2866c', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Giường King-size (King bed)'),
       ('8d70de08-05f4-45d4-99e5-1743c70e8500', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Giường Queen-size (Queen bed)'),
       ('8d080b83-e2b9-42f8-950b-a7c37440eca2', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description',
        'Giường Sofa bed');

INSERT INTO _room(id, is_deleted, status, bed_name, max_occupancy, price_per_night, quantity_room, room_area, room_name,
                  room_name_custom, room_number, sale_date, hotel_id, room_type_id)
VALUES ('8bd9a16b-283f-4aea-9165-1a6d316f683c', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Deluxe Ocean View Room', 'Room name custom demo 1', 'A', NOW(), '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('ec235cd5-ffa5-4496-bc7e-326f0c480c56', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Executive Suite', 'Room name custom demo 1', 'A', NOW(), '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('3c2a1d8d-5093-41ce-935f-f00f4b42d912', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Garden View Bungalow', 'Room name custom demo 1', 'A', NOW(), '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('cef33eca-9857-476d-832d-68ce11e4062a', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Family Villa', 'Room name custom demo 1', 'A', NOW(), 'cdba71a9-55ba-4a47-8d27-192ac3d7763d',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('d42b9795-f595-4b59-9f9f-d4a68834defe', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Junior Spa Suite', 'Room name custom demo 1', 'A', NOW(), 'cdba71a9-55ba-4a47-8d27-192ac3d7763d',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('42bb23cb-ed97-4d45-9cc5-d4e8cf6f94ee', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Luxury Poolside Chalet', 'Room name custom demo 1', 'A', NOW(), 'cdba71a9-55ba-4a47-8d27-192ac3d7763d',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('8f5f8ffc-fa80-47e2-9d19-4e13163b0c8d', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Deluxe Ocean View Room', 'Room name custom demo 1', 'A', NOW(), 'c3a651e7-7c9d-4cda-9290-3b506b3426ff',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('e56164f8-6acf-4826-a7d3-e7fbc8bf8b08', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Executive Suite', 'Room name custom demo 1', 'A', NOW(), 'c3a651e7-7c9d-4cda-9290-3b506b3426ff',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('98654e69-2817-41d7-be82-b0b00c922989', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Garden View Bungalow', 'Room name custom demo 1', 'A', NOW(), 'c3a651e7-7c9d-4cda-9290-3b506b3426ff',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('ea20e86d-9e1c-4ff0-9810-ee96b249bc5f', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Family Villa', 'Room name custom demo 1', 'A', NOW(), '674adc28-e956-4cd4-b4ee-30dbab31b21b',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('442d7315-a274-4783-b7ac-026d4352be51', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Junior Spa Suite', 'Room name custom demo 1', 'A', NOW(), '674adc28-e956-4cd4-b4ee-30dbab31b21b',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('0eb3bd07-e024-4027-8ca7-6732b54b6601', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Luxury Poolside Chalet', 'Room name custom demo 1', 'A', NOW(), '674adc28-e956-4cd4-b4ee-30dbab31b21b',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('baba2ccd-a14b-438a-abca-48a16ddc5d29', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Junior Spa Suite', 'Room name custom demo 1', 'A', NOW(), '4a982b08-3f90-49bf-b99a-a81a228c5e65',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('69770c8b-8a50-40e4-a226-123a28e49c1d', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Luxury Poolside Chalet', 'Room name custom demo 1', 'A', NOW(), '4a982b08-3f90-49bf-b99a-a81a228c5e65',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('65527181-71ea-4073-826d-4a3893c00d65', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Oceanfront Penthouse', 'Room name custom demo 1', 'A', NOW(), '4a982b08-3f90-49bf-b99a-a81a228c5e65',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('12ff57e1-7643-4f5b-89e6-ead1c7ef2df7', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Presidential Suite', 'Room name custom demo 1', 'A', NOW(), 'a2c92d15-262a-4c5c-9b63-0606cfc305fc',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('03be8f84-09f8-40c4-b9b8-587eb702002c', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Romantic Honeymoon Hideaway', 'Room name custom demo 1', 'A', NOW(), 'a2c92d15-262a-4c5c-9b63-0606cfc305fc',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('be1c7e20-e309-4efc-9e4c-b29e11c79f9b', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Superior Mountain View Room', 'Room name custom demo 1', 'A', NOW(), 'a2c92d15-262a-4c5c-9b63-0606cfc305fc',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('25fd32df-62b1-4cd2-b7de-be4e98c71561', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Junior Spa Suite', 'Room name custom demo 1', 'A', NOW(), 'befebaec-5def-4dd4-8fe3-2c68ebbb99eb',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('0266f60e-35b4-44f8-b278-706689ec954a', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Luxury Poolside Chalet', 'Room name custom demo 1', 'A', NOW(), 'befebaec-5def-4dd4-8fe3-2c68ebbb99eb',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('de8a1456-8fc6-443e-9dfe-d7e742d673b9', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Oceanfront Penthouse', 'Room name custom demo 1', 'A', NOW(), 'befebaec-5def-4dd4-8fe3-2c68ebbb99eb',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('a79eeaec-a220-4daa-a845-c37dea82b1b3', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Presidential Suite', 'Room name custom demo 1', 'A', NOW(), '5eff2197-2936-4597-8d34-4eaa6b6abdda',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('f014350e-6b0e-4a61-9061-f26e77403f02', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Romantic Honeymoon Hideaway', 'Room name custom demo 1', 'A', NOW(), '5eff2197-2936-4597-8d34-4eaa6b6abdda',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('edb89d1c-c215-487b-a68c-9417880db5af', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Superior Mountain View Room', 'Room name custom demo 1', 'A', NOW(), '5eff2197-2936-4597-8d34-4eaa6b6abdda',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('75457bf3-775e-4c21-b836-2042cbef0ba5', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Coastal View Retreat', 'Room name custom demo 1', 'A', NOW(), '1bac80fc-e760-43e9-bbf3-63a3f6701e22',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('dd9b5345-1606-4cb4-a499-2812492046bf', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Urban Chic Studio', 'Room name custom demo 1', 'A', NOW(), '1bac80fc-e760-43e9-bbf3-63a3f6701e22',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('08d5648e-1f1a-4cd4-bb7d-495ae4130c66', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Tranquil Garden Suite', 'Room name custom demo 1', 'A', NOW(), '1bac80fc-e760-43e9-bbf3-63a3f6701e22',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c'),

       ('eae4188f-30f3-45db-9f96-1c2066a960df', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Premier Lakeside Room', 'Room name custom demo 1', 'A', NOW(), '492573e8-6bd3-4bb6-a86a-dd3e9c03013e',
        '7810dfc4-8dbe-440d-bff3-673acd3e2828'),
       ('b51622cb-b452-4523-9c92-a68b8a58c363', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Mountain View Hideaway', 'Room name custom demo 1', 'A', NOW(), '492573e8-6bd3-4bb6-a86a-dd3e9c03013e',
        'ac34ba49-6a0a-40f2-9e0b-b32595704be8'),
       ('239d2bfe-680d-41cc-918b-3bc12b1d7382', false, 'ACTIVE', 'Bed name demo', 4, 200000, 1000, 25.0,
        'Seaside Spa Villa', 'Room name custom demo 1', 'A', NOW(), '492573e8-6bd3-4bb6-a86a-dd3e9c03013e',
        'b54c1f5d-7683-4c54-8e4b-108bba8cd40c');

INSERT INTO _service_and_amenity(id, is_deleted, status, created_by, created_date, last_modified_by, last_modified_date,
                                 description, name, type)
VALUES ('3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Internet', 'amenity'),
       ('9adac62b-64b2-44c8-b2f7-a2e8dd179cea', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Ho boi', 'amenity'),
       ('45fd9263-9b93-4554-b7df-a8dc499578a8', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Wifi free', 'amenity'),
       ('78bad9b7-4987-4f9e-b89d-bb70594d6007', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Tivi', 'amenity'),
       ('e46cc49e-d847-46c9-8eda-c2e79eddedf9', false, 'ACTIVE', 'ADMIN', NOW(), 'ADMIN', NOW(), 'No Description!..',
        'Tu lanh', 'amenity');


INSERT INTO _service_and_amenity_room(id, is_deleted, status, children_old, children_sleep_in_cribs, extra_bed,
                                      price_guest_adults, price_guest_children, type_of_guest_adults,
                                      type_of_guest_children, room_id, service_and_amenity_id)
VALUES (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8bd9a16b-283f-4aea-9165-1a6d316f683c', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8bd9a16b-283f-4aea-9165-1a6d316f683c', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8bd9a16b-283f-4aea-9165-1a6d316f683c', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8bd9a16b-283f-4aea-9165-1a6d316f683c', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8bd9a16b-283f-4aea-9165-1a6d316f683c', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ec235cd5-ffa5-4496-bc7e-326f0c480c56', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ec235cd5-ffa5-4496-bc7e-326f0c480c56', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ec235cd5-ffa5-4496-bc7e-326f0c480c56', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ec235cd5-ffa5-4496-bc7e-326f0c480c56', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ec235cd5-ffa5-4496-bc7e-326f0c480c56', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '3c2a1d8d-5093-41ce-935f-f00f4b42d912', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '3c2a1d8d-5093-41ce-935f-f00f4b42d912', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '3c2a1d8d-5093-41ce-935f-f00f4b42d912', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '3c2a1d8d-5093-41ce-935f-f00f4b42d912', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '3c2a1d8d-5093-41ce-935f-f00f4b42d912', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'cef33eca-9857-476d-832d-68ce11e4062a', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'cef33eca-9857-476d-832d-68ce11e4062a', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'cef33eca-9857-476d-832d-68ce11e4062a', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'cef33eca-9857-476d-832d-68ce11e4062a', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'cef33eca-9857-476d-832d-68ce11e4062a', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'd42b9795-f595-4b59-9f9f-d4a68834defe', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'd42b9795-f595-4b59-9f9f-d4a68834defe', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'd42b9795-f595-4b59-9f9f-d4a68834defe', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'd42b9795-f595-4b59-9f9f-d4a68834defe', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'd42b9795-f595-4b59-9f9f-d4a68834defe', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '42bb23cb-ed97-4d45-9cc5-d4e8cf6f94ee', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '42bb23cb-ed97-4d45-9cc5-d4e8cf6f94ee', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '42bb23cb-ed97-4d45-9cc5-d4e8cf6f94ee', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '42bb23cb-ed97-4d45-9cc5-d4e8cf6f94ee', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '42bb23cb-ed97-4d45-9cc5-d4e8cf6f94ee', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8f5f8ffc-fa80-47e2-9d19-4e13163b0c8d', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8f5f8ffc-fa80-47e2-9d19-4e13163b0c8d', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8f5f8ffc-fa80-47e2-9d19-4e13163b0c8d', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8f5f8ffc-fa80-47e2-9d19-4e13163b0c8d', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '8f5f8ffc-fa80-47e2-9d19-4e13163b0c8d', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'e56164f8-6acf-4826-a7d3-e7fbc8bf8b08', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'e56164f8-6acf-4826-a7d3-e7fbc8bf8b08', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'e56164f8-6acf-4826-a7d3-e7fbc8bf8b08', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'e56164f8-6acf-4826-a7d3-e7fbc8bf8b08', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'e56164f8-6acf-4826-a7d3-e7fbc8bf8b08', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ea20e86d-9e1c-4ff0-9810-ee96b249bc5f', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ea20e86d-9e1c-4ff0-9810-ee96b249bc5f', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ea20e86d-9e1c-4ff0-9810-ee96b249bc5f', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ea20e86d-9e1c-4ff0-9810-ee96b249bc5f', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'ea20e86d-9e1c-4ff0-9810-ee96b249bc5f', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '442d7315-a274-4783-b7ac-026d4352be51', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '442d7315-a274-4783-b7ac-026d4352be51', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '442d7315-a274-4783-b7ac-026d4352be51', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '442d7315-a274-4783-b7ac-026d4352be51', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '442d7315-a274-4783-b7ac-026d4352be51', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0eb3bd07-e024-4027-8ca7-6732b54b6601', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0eb3bd07-e024-4027-8ca7-6732b54b6601', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0eb3bd07-e024-4027-8ca7-6732b54b6601', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0eb3bd07-e024-4027-8ca7-6732b54b6601', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0eb3bd07-e024-4027-8ca7-6732b54b6601', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'baba2ccd-a14b-438a-abca-48a16ddc5d29', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'baba2ccd-a14b-438a-abca-48a16ddc5d29', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'baba2ccd-a14b-438a-abca-48a16ddc5d29', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'baba2ccd-a14b-438a-abca-48a16ddc5d29', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'baba2ccd-a14b-438a-abca-48a16ddc5d29', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '69770c8b-8a50-40e4-a226-123a28e49c1d', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '69770c8b-8a50-40e4-a226-123a28e49c1d', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '69770c8b-8a50-40e4-a226-123a28e49c1d', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '69770c8b-8a50-40e4-a226-123a28e49c1d', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '69770c8b-8a50-40e4-a226-123a28e49c1d', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '65527181-71ea-4073-826d-4a3893c00d65', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '65527181-71ea-4073-826d-4a3893c00d65', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '65527181-71ea-4073-826d-4a3893c00d65', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '65527181-71ea-4073-826d-4a3893c00d65', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '65527181-71ea-4073-826d-4a3893c00d65', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '12ff57e1-7643-4f5b-89e6-ead1c7ef2df7', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '12ff57e1-7643-4f5b-89e6-ead1c7ef2df7', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '12ff57e1-7643-4f5b-89e6-ead1c7ef2df7', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '12ff57e1-7643-4f5b-89e6-ead1c7ef2df7', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '12ff57e1-7643-4f5b-89e6-ead1c7ef2df7', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '03be8f84-09f8-40c4-b9b8-587eb702002c', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '03be8f84-09f8-40c4-b9b8-587eb702002c', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '03be8f84-09f8-40c4-b9b8-587eb702002c', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '03be8f84-09f8-40c4-b9b8-587eb702002c', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '03be8f84-09f8-40c4-b9b8-587eb702002c', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'be1c7e20-e309-4efc-9e4c-b29e11c79f9b', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'be1c7e20-e309-4efc-9e4c-b29e11c79f9b', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'be1c7e20-e309-4efc-9e4c-b29e11c79f9b', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'be1c7e20-e309-4efc-9e4c-b29e11c79f9b', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'be1c7e20-e309-4efc-9e4c-b29e11c79f9b', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '25fd32df-62b1-4cd2-b7de-be4e98c71561', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '25fd32df-62b1-4cd2-b7de-be4e98c71561', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '25fd32df-62b1-4cd2-b7de-be4e98c71561', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '25fd32df-62b1-4cd2-b7de-be4e98c71561', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '25fd32df-62b1-4cd2-b7de-be4e98c71561', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0266f60e-35b4-44f8-b278-706689ec954a', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0266f60e-35b4-44f8-b278-706689ec954a', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0266f60e-35b4-44f8-b278-706689ec954a', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0266f60e-35b4-44f8-b278-706689ec954a', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '0266f60e-35b4-44f8-b278-706689ec954a', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'de8a1456-8fc6-443e-9dfe-d7e742d673b9', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'de8a1456-8fc6-443e-9dfe-d7e742d673b9', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'de8a1456-8fc6-443e-9dfe-d7e742d673b9', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'de8a1456-8fc6-443e-9dfe-d7e742d673b9', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'de8a1456-8fc6-443e-9dfe-d7e742d673b9', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'a79eeaec-a220-4daa-a845-c37dea82b1b3', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'a79eeaec-a220-4daa-a845-c37dea82b1b3', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'a79eeaec-a220-4daa-a845-c37dea82b1b3', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'a79eeaec-a220-4daa-a845-c37dea82b1b3', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'a79eeaec-a220-4daa-a845-c37dea82b1b3', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'f014350e-6b0e-4a61-9061-f26e77403f02', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'f014350e-6b0e-4a61-9061-f26e77403f02', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'f014350e-6b0e-4a61-9061-f26e77403f02', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'f014350e-6b0e-4a61-9061-f26e77403f02', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'f014350e-6b0e-4a61-9061-f26e77403f02', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'edb89d1c-c215-487b-a68c-9417880db5af', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'edb89d1c-c215-487b-a68c-9417880db5af', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'edb89d1c-c215-487b-a68c-9417880db5af', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'edb89d1c-c215-487b-a68c-9417880db5af', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'edb89d1c-c215-487b-a68c-9417880db5af', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '75457bf3-775e-4c21-b836-2042cbef0ba5', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '75457bf3-775e-4c21-b836-2042cbef0ba5', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '75457bf3-775e-4c21-b836-2042cbef0ba5', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '75457bf3-775e-4c21-b836-2042cbef0ba5', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '75457bf3-775e-4c21-b836-2042cbef0ba5', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'dd9b5345-1606-4cb4-a499-2812492046bf', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'dd9b5345-1606-4cb4-a499-2812492046bf', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'dd9b5345-1606-4cb4-a499-2812492046bf', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'dd9b5345-1606-4cb4-a499-2812492046bf', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'dd9b5345-1606-4cb4-a499-2812492046bf', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '08d5648e-1f1a-4cd4-bb7d-495ae4130c66', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '08d5648e-1f1a-4cd4-bb7d-495ae4130c66', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '08d5648e-1f1a-4cd4-bb7d-495ae4130c66', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '08d5648e-1f1a-4cd4-bb7d-495ae4130c66', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '08d5648e-1f1a-4cd4-bb7d-495ae4130c66', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'eae4188f-30f3-45db-9f96-1c2066a960df', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'eae4188f-30f3-45db-9f96-1c2066a960df', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'eae4188f-30f3-45db-9f96-1c2066a960df', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'eae4188f-30f3-45db-9f96-1c2066a960df', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'eae4188f-30f3-45db-9f96-1c2066a960df', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'b51622cb-b452-4523-9c92-a68b8a58c363', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'b51622cb-b452-4523-9c92-a68b8a58c363', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'b51622cb-b452-4523-9c92-a68b8a58c363', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'b51622cb-b452-4523-9c92-a68b8a58c363', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        'b51622cb-b452-4523-9c92-a68b8a58c363', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9'),

       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '239d2bfe-680d-41cc-918b-3bc12b1d7382', '3823cc08-7ff8-49eb-b1bb-5b1c6fc5f7e8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '239d2bfe-680d-41cc-918b-3bc12b1d7382', '9adac62b-64b2-44c8-b2f7-a2e8dd179cea'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '239d2bfe-680d-41cc-918b-3bc12b1d7382', '45fd9263-9b93-4554-b7df-a8dc499578a8'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '239d2bfe-680d-41cc-918b-3bc12b1d7382', '78bad9b7-4987-4f9e-b89d-bb70594d6007'),
       (uuid_generate_v4(), false, 'ACTIVE', 'Children >= 12', true, 2, 0.0, 100000, false, true,
        '239d2bfe-680d-41cc-918b-3bc12b1d7382', 'e46cc49e-d847-46c9-8eda-c2e79eddedf9');


INSERT INTO _category(id, is_deleted, status, category_name)
VALUES ('efc5193e-513f-4925-bc22-9936e02ed0fc', false, 'ACTIVE', 'Vị trí'),
       ('4946c937-63e8-4f72-8b20-29e776f7d445', false, 'ACTIVE', 'Tiện nghi phòng'),
       ('602d7d0a-9080-4c72-af42-217cdcea5537', false, 'ACTIVE', 'Sạch sẽ'),
       ('e98f2c77-5ee8-401c-8452-0a6fff322fdd', false, 'ACTIVE', 'An ninh'),
       ('60e49a48-0b58-4077-af20-b42a6fcea16d', false, 'ACTIVE', 'Giá cả');

INSERT INTO _review(id, is_deleted, status, bed_name, booking_date, full_name, rating, review_comment, review_date,
                    room_name, room_type_name, total_day, hotel_id, user_id)
VALUES ('93791f64-8fda-42f7-9301-1816da2c8ed0', false, 'ACTIVE', 'Bed name demo 1', NOW(), 'Duy Nam', 8.0,
        'Room good!..', NOW(), 'Room name demo 1',
        'Room type demo 1', 10, '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3', '2235b728-1f45-40ae-97af-432e97f6fde6');

INSERT INTO _feedback(id, is_deleted, status, feedback_content, feedback_date, review_id)
VALUES (uuid_generate_v4(), false, 'ACTIVE', 'Feed back demo..', NOW(), '93791f64-8fda-42f7-9301-1816da2c8ed0');

INSERT INTO _review_category(id, is_deleted, status, category_id, review_id)
VALUES ('039ec64a-c1df-4e52-aece-5622cbb3e77c', false, 'ACTIVE', 'efc5193e-513f-4925-bc22-9936e02ed0fc',
        '93791f64-8fda-42f7-9301-1816da2c8ed0');

INSERT INTO _promotion(id, is_deleted, status, description, discount_percent, end_date, name, start_date, hotel_id)
VALUES (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 10.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        '1f0e19f2-6c8b-4da4-a4fb-06577a873ff3'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 12.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        'cdba71a9-55ba-4a47-8d27-192ac3d7763d'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 8.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        'c3a651e7-7c9d-4cda-9290-3b506b3426ff'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 12.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        '674adc28-e956-4cd4-b4ee-30dbab31b21b'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 6.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        '4a982b08-3f90-49bf-b99a-a81a228c5e65'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 15.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        'a2c92d15-262a-4c5c-9b63-0606cfc305fc'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 20.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        'befebaec-5def-4dd4-8fe3-2c68ebbb99eb'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 18.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        '5eff2197-2936-4597-8d34-4eaa6b6abdda'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 16.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        '1bac80fc-e760-43e9-bbf3-63a3f6701e22'),
       (uuid_generate_v4(), false, 'ACTIVE', 'No description!..', 19.0, NOW() + INTERVAL '1 DAY', 'HOTELDEAL10', NOW(),
        '492573e8-6bd3-4bb6-a86a-dd3e9c03013e');