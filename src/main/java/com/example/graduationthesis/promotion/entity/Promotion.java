package com.example.graduationthesis.promotion.entity;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_promotion")
public class Promotion extends BaseIDUtil {
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Double discountPercent;
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate endDate;
}
