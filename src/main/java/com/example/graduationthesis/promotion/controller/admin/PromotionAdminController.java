package com.example.graduationthesis.promotion.controller.admin;

import com.example.graduationthesis.constant.PromotionConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.promotion.dto.request.PromotionAddRequest;
import com.example.graduationthesis.promotion.dto.request.PromotionUpdateRequest;
import com.example.graduationthesis.promotion.dto.response.PromotionResponse;
import com.example.graduationthesis.promotion.dto.response.PromotionResponses;
import com.example.graduationthesis.promotion.service.PromotionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + PromotionConstant.API_PROMOTIOM)
@RequiredArgsConstructor
public class PromotionAdminController {

    private final PromotionService promotionService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<PromotionResponse> addPromotion(
            @Validated @RequestBody PromotionAddRequest request
    ) {
        return new ResponseEntity<>(promotionService.addPromotion(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping("/{pid}")
    public ResponseEntity<PromotionResponse> addPromotion(
            @PathVariable("pid") UUID pid,
            @Validated @RequestBody PromotionUpdateRequest request
    ) {
        return new ResponseEntity<>(promotionService.updatePromotion(pid, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @DeleteMapping("/{pid}")
    public ResponseEntity<PromotionResponse> addPromotion(@PathVariable("pid") UUID pid) {
        return new ResponseEntity<>(promotionService.deletePromotion(pid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<PromotionResponses> findAllPromotion(
            @RequestParam("hid") UUID hid,
            @RequestParam(SystemConstant.PARAM_STATUS) Optional<String> status,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(promotionService.findPromotionsByStatusAndHotelId(
                status.orElse("ACTIVE"),
                hid,
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK
        );
    }
}
