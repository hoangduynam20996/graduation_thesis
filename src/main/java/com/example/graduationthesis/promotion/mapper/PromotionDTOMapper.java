package com.example.graduationthesis.promotion.mapper;

import com.example.graduationthesis.promotion.dto.PromotionDTO;
import com.example.graduationthesis.promotion.entity.Promotion;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class PromotionDTOMapper implements
        Function<Promotion, PromotionDTO> {

    private final ModelMapper mapper;

    @Override
    public PromotionDTO apply(Promotion promotion) {
        return mapper.map(promotion, PromotionDTO.class);
    }
}
