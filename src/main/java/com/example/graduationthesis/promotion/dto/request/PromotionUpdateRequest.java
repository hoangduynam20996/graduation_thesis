package com.example.graduationthesis.promotion.dto.request;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class PromotionUpdateRequest {

    @NotNull
    private UUID promotionId;
    @NotBlank
    private String name;
    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    private Double discountPercent;
    @FutureOrPresent
    private LocalDate startDate;
    @FutureOrPresent
    private LocalDate endDate;

    private String description;
}
