package com.example.graduationthesis.promotion.service;

import com.example.graduationthesis.promotion.dto.request.PromotionAddRequest;
import com.example.graduationthesis.promotion.dto.request.PromotionUpdateRequest;
import com.example.graduationthesis.promotion.dto.response.PromotionResponse;
import com.example.graduationthesis.promotion.dto.response.PromotionResponses;

import java.util.UUID;

public interface PromotionService {

    PromotionResponse addPromotion(PromotionAddRequest request);

    PromotionResponse updatePromotion(UUID pid, PromotionUpdateRequest request);

    PromotionResponse deletePromotion(UUID hid);

    PromotionResponses findPromotionsByStatusAndHotelId(String status, UUID hid, Integer currentPage, Integer limitPage);
}
