package com.example.graduationthesis.promotion.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.promotion.dto.PromotionDTO;
import com.example.graduationthesis.promotion.dto.request.PromotionAddRequest;
import com.example.graduationthesis.promotion.dto.request.PromotionUpdateRequest;
import com.example.graduationthesis.promotion.dto.response.PromotionResponse;
import com.example.graduationthesis.promotion.dto.response.PromotionResponses;
import com.example.graduationthesis.promotion.entity.Promotion;
import com.example.graduationthesis.promotion.mapper.PromotionDTOMapper;
import com.example.graduationthesis.promotion.repo.PromotionRepo;
import com.example.graduationthesis.promotion.service.PromotionService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class PromotionServiceImpl implements PromotionService {

    private final HotelRepo hotelRepo;

    private final PromotionRepo promotionRepo;

    private final PromotionDTOMapper promotionDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public PromotionResponse addPromotion(PromotionAddRequest request) {
        Hotel hotel = hotelRepo.findById(request.getHotelId())
                .orElseThrow(() -> new ApiRequestException("find hotel by id " + request.getHotelId() + " not found!.", ""));

        List<Promotion> promotions = getPromotions(hotel.getId());
        if (!promotions.isEmpty())
            promotions.forEach(item -> item.setStatus(SystemConstant.STATUS_ACTIVE));

        promotionRepo.saveAll(promotions);

        Promotion save = promotionRepo.save(
                Promotion.builder()
                        .hotel(hotel)
                        .name(request.getName())
                        .discountPercent(request.getDiscountPercent())
                        .startDate(request.getStartDate())
                        .endDate(request.getEndDate())
                        .description(request.getDescription())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );
        return PromotionResponse.builder()
                .code(ResourceConstant.PMT_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(promotionDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.PMT_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PromotionResponse updatePromotion(UUID pid, PromotionUpdateRequest request) {
        Promotion promotion = getPromotion(pid);

        if (!promotion.getName().equals(request.getName()))
            promotion.setName(request.getName());

        if (!Objects.equals(promotion.getDiscountPercent(), request.getDiscountPercent()))
            promotion.setDiscountPercent(request.getDiscountPercent());

        if (request.getStartDate().equals(LocalDate.now()) || request.getStartDate().isAfter(LocalDate.now()))
            promotion.setStartDate(request.getStartDate());

        if ((request.getEndDate().equals(LocalDate.now()) || request.getEndDate().isAfter(LocalDate.now()))
                && request.getEndDate().isAfter(request.getStartDate()))
            promotion.setEndDate(request.getEndDate());

        if (promotion.getDescription() == null || !promotion.getDescription().equals(request.getDescription()))
            promotion.setDescription(request.getDescription());

        Promotion save = promotionRepo.save(promotion);
        return PromotionResponse.builder()
                .code(ResourceConstant.PMT_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(promotionDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.PMT_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PromotionResponse deletePromotion(UUID pid) {
        Promotion promotion = getPromotion(pid);
        promotion.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        promotion.setIsDeleted(SystemConstant.NO_ACTIVE);

        Promotion save = promotionRepo.save(promotion);
        return PromotionResponse.builder()
                .code(ResourceConstant.PMT_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(promotionDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.PMT_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private Promotion getPromotion(UUID pid) {
        return promotionRepo.findById(pid)
                .orElseThrow(() -> new ApiRequestException("find promotion by id " + pid + " not found!.", ""));

    }

    @Override
    public PromotionResponses findPromotionsByStatusAndHotelId(String status, UUID hid, Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Promotion> promotions = promotionRepo.findPromotionsByStatusAndHotelId(status, hid, pageable)
                .orElseThrow(() -> new ApiRequestException("find promotion by id " + hid + " and status : " + status + " not found!.", ""));
        List<PromotionDTO> promotionDTOS = promotions
                .stream()
                .map(promotionDTOMapper)
                .toList();
        return PromotionResponses.builder()
                .code(ResourceConstant.PMT_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(promotionDTOS)
                .message(getMessageBundle(ResourceConstant.PMT_004))
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, promotions.getTotalPages()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();


    }

    private List<Promotion> getPromotions(UUID hid) {
        return promotionRepo.findPromotionsByStatusAndHotelId(SystemConstant.STATUS_ACTIVE, hid)
                .orElse(null);
    }
}
