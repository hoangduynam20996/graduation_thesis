package com.example.graduationthesis.revenue;

import com.example.graduationthesis.revenue.repo.RevenueRepo;
import com.example.graduationthesis.revenue.service.RevenueService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class RevenueServiceImpl implements RevenueService {

    private final RevenueRepo revenueRepo;
}
