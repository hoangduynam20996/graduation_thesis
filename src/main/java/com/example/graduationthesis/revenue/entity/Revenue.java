package com.example.graduationthesis.revenue.entity;

import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_revenue")
public class Revenue extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private Booking booking;
    @Column
    private LocalDateTime statisticDay;
    @Column
    private BigDecimal revenueAmount;
    @Column
    private BigDecimal profit;
    @Column
    private BigDecimal discount;

}
