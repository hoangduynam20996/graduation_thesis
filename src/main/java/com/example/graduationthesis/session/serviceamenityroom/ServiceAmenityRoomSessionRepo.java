package com.example.graduationthesis.session.serviceamenityroom;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ServiceAmenityRoomSessionRepo extends JpaRepository<ServiceAmenityRoomSession, UUID> {

    Optional<ServiceAmenityRoomSession> findServiceAmenityRoomSessionByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
