package com.example.graduationthesis.session.hotelpolicy;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface HotelPolicySessionRepo extends JpaRepository<HotelPolicySession, UUID> {

    Optional<HotelPolicySession> findHotelPolicySessionByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
