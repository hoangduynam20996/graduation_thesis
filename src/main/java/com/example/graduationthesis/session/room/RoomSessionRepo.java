package com.example.graduationthesis.session.room;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoomSessionRepo extends JpaRepository<RoomSession, UUID> {

    Optional<RoomSession> findRoomSessionByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
