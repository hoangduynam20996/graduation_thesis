package com.example.graduationthesis.session.hotelpaymentmethod;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_hotel_payment_method_session")
@Entity
public class HotelPaymentMethodSession extends AbsBaseIdSession {

    private Boolean creditOrDebitCard;

    private String nameOnInvoice;

    private Boolean flagReceivingAddress;

    private String invoiceAddress;

    private String streetAddress;

    private String districtAddress;

    private String city;

    private String country;

}
