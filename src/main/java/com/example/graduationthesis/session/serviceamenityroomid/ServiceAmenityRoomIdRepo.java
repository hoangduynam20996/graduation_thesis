package com.example.graduationthesis.session.serviceamenityroomid;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ServiceAmenityRoomIdRepo extends JpaRepository<ServiceAmenityRoomId, UUID> {

    Optional<List<ServiceAmenityRoomId>> findServiceAmenityRoomIdByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
