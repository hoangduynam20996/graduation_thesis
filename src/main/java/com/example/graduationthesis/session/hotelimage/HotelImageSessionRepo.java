package com.example.graduationthesis.session.hotelimage;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HotelImageSessionRepo extends JpaRepository<HotelImageSession, UUID> {

    Optional<List<HotelImageSession>> findHotelImageSessionByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
