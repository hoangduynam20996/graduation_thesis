package com.example.graduationthesis.session.hotel;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
public class HotelSession extends AbsBaseIdSession {

    private UUID hotelTypeId;

    private String name;

    private Double rating;

    private String contactPerson;

    private String phoneNumber;

    private String phoneNumberTwo;

    private String streetAddress;

    private String districtAddress;

    private String country;

    private String city;

    private String postalCode;

}
