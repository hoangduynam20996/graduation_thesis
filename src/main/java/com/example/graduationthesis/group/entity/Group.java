package com.example.graduationthesis.group.entity;

import com.example.graduationthesis.permissiongroup.entity.PermissionGroup;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_group")
@Entity
public class Group extends BaseEntityUtil {

    @Column(length = 50, unique = true, nullable = false)
    private String groupName;
    @Column
    private String description;
    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    private List<PermissionGroup> permissionGroups;
}
