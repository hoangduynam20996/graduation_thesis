package com.example.graduationthesis.group.repo;

import com.example.graduationthesis.group.entity.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface GroupRepo extends JpaRepository<Group, UUID> {

    boolean existsByGroupName(String groupName);

    boolean existsByGroupNameNot(String groupName);

    Optional<Group> findGroupByIdAndIsDeleted(UUID id, Boolean isDeleted);

    @Query("""
            SELECT
                o
            FROM
                Group o
            WHERE NOT
                o.groupName = 'ADMIN_MASTER'
            """)
    Page<Group> findAllNotAdminMaster(Pageable pageable);
}
