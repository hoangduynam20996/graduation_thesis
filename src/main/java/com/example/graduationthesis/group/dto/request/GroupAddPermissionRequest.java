package com.example.graduationthesis.group.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class GroupAddPermissionRequest {
    private UUID groupId;

    private List<PermissionIdRequest> permissionIds;

    public record PermissionIdRequest(
            UUID permissionId
    ) {
    }

}
