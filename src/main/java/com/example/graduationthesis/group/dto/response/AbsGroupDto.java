package com.example.graduationthesis.group.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsGroupDto {

    private UUID groupId;

    private String groupName;

    private Boolean isDeleted;

    private String status;

    private String description;
}
