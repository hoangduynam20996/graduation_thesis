package com.example.graduationthesis.group.controller.admin;

import com.example.graduationthesis.constant.GroupConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.group.dto.request.GroupAddPermissionRequest;
import com.example.graduationthesis.group.dto.request.GroupAddRequest;
import com.example.graduationthesis.group.dto.request.GroupAddUserForGroup;
import com.example.graduationthesis.group.dto.request.GroupRequest;
import com.example.graduationthesis.group.dto.response.GroupResponse;
import com.example.graduationthesis.group.dto.response.GroupResponses;
import com.example.graduationthesis.group.service.GroupService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + GroupConstant.API_GROUP)
@RequiredArgsConstructor
@PreAuthorize("hasAnyAuthority('admin_master')")
public class GroupAdminController {

    private final GroupService groupService;

    @PostMapping
    public ResponseEntity<GroupResponse> addPermissionGroup(
            @Valid @RequestBody GroupAddRequest request
    ) {
        return new ResponseEntity<>(groupService.addGroup(request), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<GroupResponses> findAllPermissionGroup(
            @RequestParam(value = SystemConstant.CURRENT_PAGE, required = false) Optional<Integer> currentPage,
            @RequestParam(value = SystemConstant.LIMIT_PAGE, required = false) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(groupService.findAllGroup(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<GroupResponse> updatePermissionGroup(
            @Valid @RequestBody GroupRequest request
    ) {
        return new ResponseEntity<>(groupService.updateGroup(request), HttpStatus.OK);
    }

    @DeleteMapping(GroupConstant.API_GROUP_ID)
    public ResponseEntity<GroupResponse> deletePermissionGroup(
            @PathVariable(GroupConstant.PATH_GROUP_ID) UUID groupId
    ) {
        return new ResponseEntity<>(groupService.deleteGroup(groupId), HttpStatus.OK);
    }

    @PostMapping(GroupConstant.API_ADD_USER_FOR_GROUP)
    public ResponseEntity<GroupResponse> addUserForGroup(
            @Valid @RequestBody GroupAddUserForGroup request
    ) {
        return new ResponseEntity<>(groupService.addUserForGroup(request), HttpStatus.OK);
    }

    @GetMapping(GroupConstant.API_GROUP_ID)
    public ResponseEntity<GroupResponse> findPermissionGroupByName(
            @PathVariable(GroupConstant.PATH_GROUP_ID) UUID groupId
    ) {
        return new ResponseEntity<>(groupService.findGroupById(groupId), HttpStatus.OK);
    }

    @PutMapping(GroupConstant.API_UPDATE_PERMISSIONS_FOR_GROUP)
    public ResponseEntity<GroupResponse> updatePermissionsForGroup(
            @Valid @RequestBody GroupAddPermissionRequest request
    ) {
        return new ResponseEntity<>(groupService.updatePermissionsForGroup(request), HttpStatus.OK);
    }
}
