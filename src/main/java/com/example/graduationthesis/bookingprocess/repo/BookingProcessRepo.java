package com.example.graduationthesis.bookingprocess.repo;

import com.example.graduationthesis.bookingprocess.entity.BookingProcess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface BookingProcessRepo extends JpaRepository<BookingProcess, UUID> {

    Optional<BookingProcess> findBookingProcessByJwtToken(String jwtToken);
}
