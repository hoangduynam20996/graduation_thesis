package com.example.graduationthesis.billingaddress;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BillingAddressRepo extends JpaRepository<BillingAddress, UUID> {
}
