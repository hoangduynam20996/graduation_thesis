package com.example.graduationthesis.hoteltype.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hoteltype.dto.HotelTypeDTO;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeResponse;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeResponses;
import com.example.graduationthesis.hoteltype.entity.HotelType;
import com.example.graduationthesis.hoteltype.mapper.HotelTypeDTOMapper;
import com.example.graduationthesis.hoteltype.repo.HotelTypeRepo;
import com.example.graduationthesis.hoteltype.service.HotelTypeService;
import com.example.graduationthesis.image.ImageService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.BaseUrlServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class HotelTypeServiceImpl implements HotelTypeService {

    private final HotelTypeRepo hotelTypeRepo;

    private final HotelTypeDTOMapper hotelTypeDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final ImageService imageService;

    private final BaseUrlServiceUtil baseUrlServiceUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public HotelTypeResponse addHotelType(String name, String description, MultipartFile multipartFile) {
        if (hotelTypeRepo.existsByName(name))
            throw new ApiRequestException("add hotel type fail " + name + " already exist!.", "");
        checkFileImage(multipartFile);
        String urlImage = baseUrlServiceUtil.getBaseUrl() + SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + "/image/" + imageService.saveImage(multipartFile);

        try {
            HotelType save = hotelTypeRepo.save(
                    HotelType.builder()
                            .name(name.trim())
                            .description(description.trim())
                            .urlImage(urlImage)
                            .status(SystemConstant.STATUS_ACTIVE)
                            .isDeleted(SystemConstant.ACTIVE)
                            .build()
            );
            return HotelTypeResponse.builder()
                    .code(ResourceConstant.HT_T_009)
                    .status(SystemConstant.STATUS_SUCCESS)
                    .data(hotelTypeDTOMapper.apply(save))
                    .message(getMessageBundle(ResourceConstant.HT_T_009))
                    .responseTime(baseAmenityUtil.responseTime())
                    .build();

        } catch (Exception e) {
            throw new ApiRequestException("HTI_001", e.getMessage());
        }
    }

    @Override
    public HotelTypeResponse updateHotelType(String name, String description, MultipartFile multipartFile, UUID httid) {
        HotelType hotelType = extractedHotelType(httid);
        checkFileImage(multipartFile);

        if (hotelTypeRepo.existsByNameAndIdNot(name, httid))
            throw new ApiRequestException("update fail duplicate hotel type name!.", "");

        if (!hotelType.getName().equals(name))
            hotelType.setName(name);

        if (hotelType.getDescription() == null || !hotelType.getDescription().equals(description))
            hotelType.setDescription(description);

        String urlImage = baseUrlServiceUtil.getBaseUrl() + SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + "/image/" + imageService.saveImage(multipartFile);

        if (!hotelType.getUrlImage().equals(urlImage))
            hotelType.setUrlImage(urlImage);

        HotelType save = hotelTypeRepo.save(hotelType);
        return HotelTypeResponse.builder()
                .code(ResourceConstant.HT_T_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelTypeDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_T_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelTypeResponse deleteHotelType(UUID httid) {
        HotelType hotelType = extractedHotelType(httid);
        hotelType.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        hotelType.setIsDeleted(SystemConstant.NO_ACTIVE);
        HotelType save = hotelTypeRepo.save(hotelType);
        return HotelTypeResponse.builder()
                .code(ResourceConstant.HT_T_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelTypeDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_T_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelTypeResponses findAll(String status, int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<HotelType> hotelTypes = hotelTypeRepo.findAllByStatus(status, pageable);
        List<HotelTypeDTO> save = hotelTypes
                .stream()
                .map(hotelTypeDTOMapper)
                .toList();
        return HotelTypeResponses.builder()
                .code(ResourceConstant.HT_T_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(save)
                .message(getMessageBundle(ResourceConstant.HT_T_004))
                .responseTime(baseAmenityUtil.responseTime())
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, hotelTypes.getTotalPages()))
                .build();
    }

    private HotelType extractedHotelType(UUID httid) {
        return hotelTypeRepo.findById(httid)
                .orElseThrow(() -> new ApiRequestException("find hotel type by id " + httid + " not found!.", ""));
    }

    private void checkFileImage(MultipartFile multipartFile) {
        String fileName = multipartFile.getOriginalFilename();

        if (fileName != null && !(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png")))
            throw new ApiRequestException("file upload not image!.", "");
    }
}
