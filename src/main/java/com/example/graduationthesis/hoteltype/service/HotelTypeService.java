package com.example.graduationthesis.hoteltype.service;

import com.example.graduationthesis.hoteltype.dto.response.HotelTypeResponse;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeResponses;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface HotelTypeService {

    HotelTypeResponse addHotelType(String name, String description, MultipartFile multipartFile);

    HotelTypeResponse updateHotelType(String name, String description, MultipartFile multipartFile, UUID httid);

    HotelTypeResponse deleteHotelType(UUID httid);

    HotelTypeResponses findAll(String status, int currentPage, int limitPage);
}
