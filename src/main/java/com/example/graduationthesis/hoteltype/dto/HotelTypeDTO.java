package com.example.graduationthesis.hoteltype.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class HotelTypeDTO extends BaseDTOUtil {

    private String name;

    private String status;

    private String urlImage;

    private String description;
}
