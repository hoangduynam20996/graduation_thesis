package com.example.graduationthesis.hoteltype.entity;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_hotel_type")
public class HotelType extends BaseEntityUtil {

    @Column
    private String name;
    @Column
    @Nationalized
    private String description;
    @Column
    private String urlImage;
    @OneToMany(mappedBy = "hotelType", fetch = FetchType.LAZY)
    private List<Hotel> hotels;
}
