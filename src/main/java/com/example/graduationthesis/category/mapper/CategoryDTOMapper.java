package com.example.graduationthesis.category.mapper;

import com.example.graduationthesis.category.dto.CategoryDTO;
import com.example.graduationthesis.category.entity.Category;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CategoryDTOMapper implements Function<Category, CategoryDTO> {

    @Override
    public CategoryDTO apply(Category category) {
        return new CategoryDTO(
                category.getId(),
                category.getCategoryName(),
                category.getStatus()
        );
    }
}
