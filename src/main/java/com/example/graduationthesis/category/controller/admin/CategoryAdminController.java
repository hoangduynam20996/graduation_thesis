package com.example.graduationthesis.category.controller.admin;

import com.example.graduationthesis.category.dto.request.CategoryAddRequest;
import com.example.graduationthesis.category.dto.request.CategoryUpdateRequest;
import com.example.graduationthesis.category.dto.response.CategoryResponse;
import com.example.graduationthesis.category.dto.response.CategoryResponses;
import com.example.graduationthesis.category.service.CategoryService;
import com.example.graduationthesis.constant.CategoryConstant;
import com.example.graduationthesis.constant.SystemConstant;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + CategoryConstant.API_CATEGORY)
@RequiredArgsConstructor
public class CategoryAdminController {

    private final CategoryService categoryService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<CategoryResponse> addCategory(
            @Valid @RequestBody CategoryAddRequest request
    ) {
        categoryService.addCategory(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping
    public ResponseEntity<CategoryResponse> updateCategory(
            @Valid @RequestBody CategoryUpdateRequest request,
            @RequestParam("cid") UUID cid
    ) {
        categoryService.updateCategory(request, cid);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @DeleteMapping
    public ResponseEntity<CategoryResponse> deleteCategory(@RequestParam("cid") UUID cid) {
        categoryService.deleteCategory(cid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<CategoryResponses> findCategoriesByStatus(
            @RequestParam(value = "page", required = false) Optional<Integer> page,
            @RequestParam(value = "limit", required = false) Optional<Integer> limit,
            @RequestParam(value = "status", required = false) Optional<String> status
    ) {
        return new ResponseEntity<>(categoryService.findAllCategoryByStatus(
                page.orElse(1),
                limit.orElse(8),
                status.orElse(SystemConstant.STATUS_ACTIVE)),
                HttpStatus.OK
        );
    }
}
