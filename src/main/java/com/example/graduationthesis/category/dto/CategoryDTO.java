package com.example.graduationthesis.category.dto;

import java.util.UUID;

public record CategoryDTO(
        UUID categoryId,
        String categoryName,
        String status
) {
}
