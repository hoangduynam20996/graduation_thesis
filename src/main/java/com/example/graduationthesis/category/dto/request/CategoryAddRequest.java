package com.example.graduationthesis.category.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryAddRequest {

    @NotBlank
    private String categoryName;
}
