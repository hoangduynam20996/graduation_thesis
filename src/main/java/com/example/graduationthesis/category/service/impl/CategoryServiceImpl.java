package com.example.graduationthesis.category.service.impl;

import com.example.graduationthesis.category.dto.CategoryDTO;
import com.example.graduationthesis.category.dto.request.CategoryAddRequest;
import com.example.graduationthesis.category.dto.request.CategoryUpdateRequest;
import com.example.graduationthesis.category.dto.response.CategoryResponse;
import com.example.graduationthesis.category.dto.response.CategoryResponses;
import com.example.graduationthesis.category.entity.Category;
import com.example.graduationthesis.category.mapper.CategoryDTOMapper;
import com.example.graduationthesis.category.repo.CategoryRepo;
import com.example.graduationthesis.category.service.CategoryService;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepo categoryRepo;

    private final CategoryDTOMapper categoryDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public CategoryResponse addCategory(CategoryAddRequest request) {
        if (categoryRepo.existsByCategoryName(request.getCategoryName()))
            throw new ApiRequestException("add category fail " + request.getCategoryName() + " duplicate!.", "");

        Category save = categoryRepo.save(
                Category.builder()
                        .categoryName(request.getCategoryName())
                        .status(SystemConstant.STATUS_ACTIVE)
                        .isDeleted(SystemConstant.ACTIVE)
                        .build()
        );
        return CategoryResponse.builder()
                .code(ResourceConstant.CT_G_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(categoryDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.CT_G_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public CategoryResponse updateCategory(CategoryUpdateRequest request, UUID cid) {
        Category category = getCategory(cid);

        if (categoryRepo.existsByCategoryNameAndIdNot(request.getCategoryName(), cid))
            throw new ApiRequestException("update fail " + request.getCategoryName() + " already exists!.", "");

        if (!category.getCategoryName().equals(request.getCategoryName()))
            category.setCategoryName(request.getCategoryName());

        if (!Objects.equals(category.getStatus(), request.getStatus()))
            category.setStatus(request.getStatus());

        Category save = categoryRepo.save(category);
        return CategoryResponse.builder()
                .code(ResourceConstant.CT_G_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(categoryDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.CT_G_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public CategoryResponse deleteCategory(UUID cid) {
        Category category = getCategory(cid);
        category.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        category.setIsDeleted(SystemConstant.NO_ACTIVE);
        Category save = categoryRepo.save(category);
        return CategoryResponse.builder()
                .code(ResourceConstant.CT_G_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(categoryDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.CT_G_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public CategoryResponses findAllCategoryByStatus(Integer page, Integer limit, String status) {
        Pageable pageable = baseAmenityUtil.pageable(page, limit);
        Page<Category> categories = categoryRepo.findCategoriesByStatus(status, pageable)
                .orElseThrow(() -> new ApiRequestException("find categories by status" + status + " not found!.", ""));
        List<CategoryDTO> list = categories.stream()
                .map(categoryDTOMapper)
                .toList();
        return CategoryResponses.builder()
                .code(ResourceConstant.CT_G_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message(getMessageBundle(ResourceConstant.CT_G_004))
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, categories.getTotalPages()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private Category getCategory(UUID cid) {
        return categoryRepo.findById(cid)
                .orElseThrow(() -> new ApiRequestException("find category by id " + cid + " not found!.", ""));
    }
}
