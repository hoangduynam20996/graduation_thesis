package com.example.graduationthesis.category.entity;

import com.example.graduationthesis.reviewcategory.entity.ReviewCategory;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_category")
public class Category extends BaseIDUtil {

    @Column
    private String categoryName;
    @OneToMany(mappedBy = "category")
    private List<ReviewCategory> reviewCategories;
}
