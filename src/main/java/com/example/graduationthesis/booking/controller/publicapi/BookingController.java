package com.example.graduationthesis.booking.controller.publicapi;

import com.example.graduationthesis.booking.dto.request.BookingConfirmSessionInfoRequest;
import com.example.graduationthesis.booking.dto.request.BookingStartSessionInfoRequest;
import com.example.graduationthesis.booking.dto.request.BookingStartSessionRequest;
import com.example.graduationthesis.booking.dto.response.BookingResponse;
import com.example.graduationthesis.booking.service.BookingService;
import com.example.graduationthesis.constant.BookingConstant;
import com.example.graduationthesis.constant.SystemConstant;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + BookingConstant.API_BOOKING)
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;

    @PostMapping(BookingConstant.SESSION_BOOKING)
    public ResponseEntity<BookingResponse> startSessionBooking(
            @Valid @RequestBody BookingStartSessionRequest request
    ) {
        return new ResponseEntity<>(bookingService.starSessionBooking(request), HttpStatus.CREATED);
    }

    @PostMapping(BookingConstant.SESSION_BOOKING + "/info")
    public ResponseEntity<BookingResponse> startSessionBooking(
            @RequestHeader("jwtToken") String jwtToken,
            @Valid @RequestBody BookingStartSessionInfoRequest request
    ) {
        return new ResponseEntity<>(bookingService.starSessionBooking(jwtToken, request), HttpStatus.CREATED);
    }

    @PostMapping(BookingConstant.SESSION_BOOKING + "/confirm")
    public ResponseEntity<BookingResponse> confirmSessionBooking(
            @RequestHeader("jwtToken") String jwtToken,
            @Valid @RequestBody BookingConfirmSessionInfoRequest request
    ) {
        return new ResponseEntity<>(bookingService.confirmSessionBooking(jwtToken, request), HttpStatus.OK);
    }

    @GetMapping("/{jwtToken}/{status}")
    public ResponseEntity<?> getBookingProcessSession(
            @PathVariable("jwtToken") String jwtToken,
            @PathVariable("status") Optional<String> status
    ) {
        return new ResponseEntity<>(bookingService.getDataSessionBooking(jwtToken, status.orElse(BookingConstant.STATUS_BOOKING_SET_PLACE)), HttpStatus.OK);
    }
}
