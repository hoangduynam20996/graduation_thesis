package com.example.graduationthesis.booking.controller.admin;

import com.example.graduationthesis.booking.dto.request.BookingFindCondition;
import com.example.graduationthesis.booking.dto.response.BookingResponses;
import com.example.graduationthesis.booking.service.BookingService;
import com.example.graduationthesis.constant.BookingConstant;
import com.example.graduationthesis.constant.SystemConstant;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + BookingConstant.API_BOOKING)
@RequiredArgsConstructor
public class BookingAdminController {

    private final BookingService bookingService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<BookingResponses> findBookingsByHotelId(
            @Valid BookingFindCondition request,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(bookingService.findBookingsByCondition(
                request,
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK
        );
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping("/{bookingId}/{status}")
    public ResponseEntity<?> changeStatusBooking(
            @PathVariable("bookingId") UUID bookingId,
            @PathVariable(SystemConstant.PARAM_STATUS) Optional<String> status) {
        return new ResponseEntity<>(bookingService.changeStatusBooking(
                bookingId,
                status.orElse(BookingConstant.STATUS_BOOKING_PENDING)),
                HttpStatus.OK
        );
    }
}
