package com.example.graduationthesis.booking.mapper;

import com.example.graduationthesis.booking.dto.BookingDTO;
import com.example.graduationthesis.booking.dto.BookingUserDTO;
import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.hotel.dto.response.HotelBookingResponse;
import com.example.graduationthesis.payment.dto.PaymentDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
public class BookingDTOMapper implements Function<Booking, BookingDTO> {

    @Override
    public BookingDTO apply(Booking booking) {
        return null;
    }

    public BookingUserDTO applyBookingUserDTO(Booking booking, HotelBookingResponse hotelBookingResponse) {
        List<PaymentDTO> paymentDTOS = booking.getPayments().stream()
                .map(item -> new PaymentDTO(
                        item.getPaymentDate(),
                        item.getPaymentAmount(),
                        item.getPaymentMethod() != null ? item.getPaymentMethod() : "No method payment!..",
                        item.getStatus()
                ))
                .toList();
        return new BookingUserDTO(
                booking.getId(),
                booking.getFullName(),
                booking.getPhoneNumber(),
                booking.getEmail(),
                booking.getBookingCode(),
                booking.getPinCode(),
                booking.getAddress() != null ? booking.getAddress() : "No address!..",
                booking.getStatus(),
                booking.getBookingDate(),
                booking.getTotalPrice(),
                paymentDTOS.get(0),
                hotelBookingResponse
        );
    }

}
