package com.example.graduationthesis.booking.repo;

import com.example.graduationthesis.booking.dto.request.BookingFindCondition;
import com.example.graduationthesis.booking.entity.Booking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BookingRepo extends JpaRepository<Booking, UUID> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("""
                UPDATE
                    Booking b
                SET
                    b.status = 'ACTIVE'
                WHERE
                    b.id = ?1
            """)
    void updateStatusPaidByBookingId(UUID bookingId);

    @Query(value = """
            SELECT
                b
            FROM
                Booking b
            LEFT JOIN
                Payment p ON p.booking.id = b.id
            LEFT JOIN
                b.bookingDetails bd ON bd.booking.id = b.id
            WHERE
                (:#{#condition.bookingCode} IS NULL OR LOWER(b.bookingCode) ILIKE CONCAT('%', LOWER(:#{#condition.bookingCode}), '%') )
                AND
                   (:#{#condition.pinCode} IS NULL OR LOWER(b.pinCode) ILIKE CONCAT('%', LOWER(:#{#condition.pinCode}), '%') )
              AND
               (:#{#condition.phoneNumber} IS NULL OR LOWER(b.phoneNumber) ILIKE CONCAT('%', LOWER(:#{#condition.phoneNumber}), '%') )
                AND
                    (:#{#condition.checkInDate} IS NULL AND :#{#condition.checkOutDate} IS NULL
                        OR :#{#condition.checkInDate} IS NOT NULL AND b.checkInDate = CAST(:#{#condition.checkInDate} AS DATE)
                        OR :#{#condition.checkOutDate} IS NOT NULL AND b.checkOutDate = CAST(:#{#condition.checkOutDate} AS DATE)
                        OR :#{#condition.checkInDate} IS NOT NULL AND :#{#condition.checkOutDate} IS NOT NULL
                        AND
                            CAST(b.checkInDate AS DATE) >= CAST(:#{#condition.checkInDate} AS DATE)
                            AND CAST(b.checkOutDate AS DATE) <= CAST(:#{#condition.checkOutDate} AS DATE)
                        )
                AND
                    (:#{#condition.statusBooking} IS NULL OR b.status = :#{#condition.statusBooking})
                AND
                    (:#{#condition.nameContact} IS NULL OR LOWER(b.fullName) LIKE CONCAT('%', LOWER(:#{#condition.nameContact}), '%'))
                AND
                    (:#{#condition.hotelName} IS NULL OR LOWER(bd.room.hotel.name) LIKE CONCAT('%', LOWER(:#{#condition.hotelName}), '%'))
                AND
                    (:#{#condition.roomName} IS NULL OR LOWER(bd.room.roomName) LIKE CONCAT('%', LOWER(:#{#condition.roomName}), '%'))
                AND
                    (:#{#condition.statusPayment} IS NULL OR p.status = :#{#condition.statusPayment})
                AND
                    (:#{#condition.bookingDate} IS NULL AND :#{#condition.bookingDateTo} IS NULL
                        OR :#{#condition.bookingDate} IS NOT NULL AND CAST(b.bookingDate AS DATE) = CAST(:#{#condition.bookingDate} AS DATE)
                        OR :#{#condition.bookingDateTo} IS NOT NULL AND CAST(b.bookingDate AS DATE) = CAST(:#{#condition.bookingDateTo} AS DATE)
                        OR :#{#condition.bookingDate} IS NOT NULL AND :#{#condition.bookingDateTo} IS NOT NULL
                        AND CAST(b.bookingDate AS DATE) BETWEEN CAST(:#{#condition.bookingDate} AS DATE) AND CAST(:#{#condition.bookingDateTo} AS DATE))
                ORDER BY
                    b.bookingDate DESC
                """)
    Page<Booking> findBookingsByCondition(BookingFindCondition condition, Pageable pageable);

    @Query("""
            SELECT b FROM Booking b WHERE b.user.id = ?1 ORDER BY b.bookingDate DESC
            """)
    List<Booking> findBookingsByUser(UUID userId);

    @Query("""
                select b from Booking b where b.hotel.id= ?1 and b.user.id=?2
            """)
    Booking findBookingByHotelAndUser(UUID hotelId, UUID userId);

    Optional<Booking> findBookingByHotel_IdAndUser_Id(UUID hotelId, UUID userId);
}
