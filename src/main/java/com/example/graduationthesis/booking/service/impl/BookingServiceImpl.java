package com.example.graduationthesis.booking.service.impl;

import com.example.graduationthesis.booking.dto.BookingUserDTO;
import com.example.graduationthesis.booking.dto.request.BookingConfirmSessionInfoRequest;
import com.example.graduationthesis.booking.dto.request.BookingFindCondition;
import com.example.graduationthesis.booking.dto.request.BookingStartSessionInfoRequest;
import com.example.graduationthesis.booking.dto.request.BookingStartSessionRequest;
import com.example.graduationthesis.booking.dto.response.BookingResponse;
import com.example.graduationthesis.booking.dto.response.BookingResponses;
import com.example.graduationthesis.booking.dto.response.BookingRoomResponse;
import com.example.graduationthesis.booking.dto.response.BookingSessionInfoResponse;
import com.example.graduationthesis.booking.dto.response.BookingSessionResponse;
import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.booking.mapper.BookingDTOMapper;
import com.example.graduationthesis.booking.repo.BookingRepo;
import com.example.graduationthesis.booking.service.BookingService;
import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.bookingdetail.repo.BookingDetailRepo;
import com.example.graduationthesis.bookingprocess.entity.BookingProcess;
import com.example.graduationthesis.bookingprocess.repo.BookingProcessRepo;
import com.example.graduationthesis.constant.BookingConstant;
import com.example.graduationthesis.constant.PaymentConstant;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.emai.EmailService;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.historybooking.entity.HistoryBooking;
import com.example.graduationthesis.historybooking.repo.HistoryBookingRepo;
import com.example.graduationthesis.hotel.dto.response.HotelBookingResponse;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.hotelimage.repo.HotelImageRepo;
import com.example.graduationthesis.jwt.JwtService;
import com.example.graduationthesis.payment.entity.Payment;
import com.example.graduationthesis.payment.repo.PaymentRepo;
import com.example.graduationthesis.promotion.entity.Promotion;
import com.example.graduationthesis.promotion.repo.PromotionRepo;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.room.repo.RoomRepo;
import com.example.graduationthesis.roominfobookingprocess.dto.request.RoomInfoRequest;
import com.example.graduationthesis.roominfobookingprocess.entity.RoomInfo;
import com.example.graduationthesis.roominfobookingprocess.mapper.RoomInfoDtoMapper;
import com.example.graduationthesis.roominfobookingprocess.repo.RoomInfoRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.repo.UserRepo;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class BookingServiceImpl extends AbsUserServiceUtil implements BookingService {

    private static final Logger log = LoggerFactory.getLogger(BookingServiceImpl.class);
    private final BookingRepo bookingRepo;

    private final BookingDetailRepo bookingDetailRepo;

    private final BookingDTOMapper bookingDTOMapper;

    private final RoomRepo roomRepo;

    private final PromotionRepo promotionRepo;

    private final EmailService emailService;

    private final HistoryBookingRepo historyBookingRepo;

    private final BookingProcessRepo bookingProcessRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private final JwtService jwtService;

    private final RoomInfoRepo roomInfoRepo;

    private final HotelRepo hotelRepo;

    private final RoomInfoDtoMapper roomInfoDtoMapper;

    private final PaymentRepo paymentRepo;

    private final UserRepo userRepo;

    private final HttpServletRequest httpServletRequest;

    private final HotelImageRepo hotelImageRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public BookingResponse starSessionBooking(BookingStartSessionRequest request) {
        String jwt = jwtService.generateToken();

        Hotel hotel = getHotel(request.getHotelId());

        long totalDay = ChronoUnit.DAYS.between(request.getCheckInDate(), request.getCheckOutDate());

        Promotion promotion = getPromotion(request.getHotelId());

        List<RoomInfo> roomInfos = getRoomInfos(jwt, request.getRoomInfoRequests());
        roomInfoRepo.saveAll(roomInfos);

        List<BookingRoomResponse> bookingRoomResponses = new ArrayList<>();
        roomInfos.forEach(item -> {
            Room room = getRoom(item.getRoomId());
            if (room.getQuantityRoom() < item.getQuantityRoom()) {
                throw new ApiRequestException("ROOM_", "Number of rooms is not enough!..");
            }
            BigDecimal priceRoom = room.getPricePerNight().multiply(BigDecimal.valueOf(totalDay));
            bookingRoomResponses.add(
                    BookingRoomResponse.builder()
                            .quantityRoom(item.getQuantityRoom())
                            .totalPriceOrigin(priceRoom)
                            .totalPricePromotion(getTotalPriceBookingPromotion(promotion, priceRoom))
                            .build()
            );
        });

        double totalPrice = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPricePromotion)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        double totalPriceOrigin = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPriceOrigin)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        int totalRoom = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getQuantityRoom)
                .mapToInt(Integer::intValue)
                .sum();

        bookingProcessRepo.save(
                BookingProcess.builder()
                        .hotelId(hotel.getId())
                        .jwtToken(jwt)
                        .checkInDate(request.getCheckInDate())
                        .checkOutDate(request.getCheckOutDate())
                        .quantityRoom(totalRoom)
                        .quantityAdult(request.getQuantityAdult())
                        .quantityChildren(request.getQuantityChildren())
                        .location(request.getLocation())
                        .build()
        );

        BookingSessionResponse build = BookingSessionResponse.builder()
                .hotelName(hotel.getName())
                .address(hotel.getStreetAddress())
                .totalReview(9.0)
                .checkInDate(request.getCheckInDate())
                .checkOutDate(request.getCheckOutDate())
                .totalDay(totalDay)
                .quantityRoom(totalRoom)
                .quantityAdult(request.getQuantityAdult())
                .quantityChildren(request.getQuantityChildren())
                .priceOrigin(totalPriceOrigin)
                .pricePromotion(totalPrice)
                .jwtToken(jwt)
                .build();

        return BookingResponse.builder()
                .code("BK_SS_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(build)
                .message("Start session booking success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private Hotel getHotel(UUID hotelId) {
        return hotelRepo.findById(hotelId)
                .orElseThrow(() -> new ApiRequestException("HT_01", "Find hotel by id not found!.."));
    }

    @Override
    public BookingResponse starSessionBooking(String jwtToken, BookingStartSessionInfoRequest request) {
        if (jwtService.isTokenExpired(jwtToken)) {
            throw new ApiRequestException("Jwt Ex", "Jwt exception!..");
        }

        BookingProcess bookingProcess = getBookingProcess(jwtToken);

        Hotel hotel = getHotel(bookingProcess.getHotelId());

        List<RoomInfo> roomInfos = roomInfoRepo.findRoomInfosByJwtToken(jwtToken);

        Promotion promotion = getPromotion(bookingProcess.getHotelId());

        long totalDay = ChronoUnit.DAYS.between(bookingProcess.getCheckInDate(), bookingProcess.getCheckOutDate());

        List<BookingRoomResponse> bookingRoomResponses = new ArrayList<>();
        roomInfos.forEach(item -> {
            Room room = getRoom(item.getRoomId());
            if (room.getQuantityRoom() < item.getQuantityRoom()) {
                throw new ApiRequestException("ROOM_", "Number of rooms is not enough!..");
            }
            BigDecimal priceRoom = room.getPricePerNight().multiply(BigDecimal.valueOf(totalDay));
            bookingRoomResponses.add(
                    BookingRoomResponse.builder()
                            .quantityRoom(item.getQuantityRoom())
                            .totalPriceOrigin(priceRoom)
                            .totalPricePromotion(getTotalPriceBookingPromotion(promotion, priceRoom))
                            .build()
            );
        });

        double totalPrice = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPricePromotion)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        double totalPriceOrigin = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPriceOrigin)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        int totalRoom = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getQuantityRoom)
                .mapToInt(Integer::intValue)
                .sum();

        BookingProcess build = bookingProcess.toBuilder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .fullName(request.getFirstName() + " " + request.getLastName())
                .bookingForMe(request.getBookingForMe())
                .pickUpService(request.getPickUpService())
                .orderTaxi(request.getOrderTaxi())
                .orderCar(request.getOrderCar())
                .quantityAdult(bookingProcess.getQuantityAdult())
                .quantityChildren(bookingProcess.getQuantityChildren())
                .electronicConfirm(request.getElectronicConfirm())
                .country(request.getCountry())
                .estimatedCheckInTime(request.getEstimatedCheckInTime())
                .businessTravel(request.getBusinessTravel())
                .phoneNumber(request.getPhoneNumber())
                .specialRequirements(request.getSpecialRequirements())
                .email(request.getEmail())
                .build();
        BookingProcess save = bookingProcessRepo.save(build);

        BookingSessionInfoResponse bookingSessionResponse = BookingSessionInfoResponse.builder()
                .hotelName(hotel.getName())
                .address(hotel.getStreetAddress())
                .totalReview(9.0)
                .checkInDate(save.getCheckInDate())
                .checkOutDate(save.getCheckOutDate())
                .totalDay(totalDay)
                .quantityRoom(totalRoom)
                .quantityAdult(save.getQuantityAdult())
                .quantityChildren(save.getQuantityChildren())
                .priceOrigin(totalPriceOrigin)
                .pricePromotion(totalPrice)
                .jwtToken(save.getJwtToken())
                .firstName(save.getFirstName())
                .lastName(save.getLastName())
                .email(save.getEmail())
                .country(save.getCountry())
                .phoneNumber(save.getPhoneNumber())
                .address(hotel.getStreetAddress())
                .bookingForMe(save.getBookingForMe())
                .businessTravel(save.getBusinessTravel())
                .electronicConfirm(save.getElectronicConfirm())
                .orderCar(save.getOrderCar())
                .orderTaxi(save.getOrderTaxi())
                .pickUpService(save.getPickUpService())
                .specialRequirements(save.getSpecialRequirements())
                .estimatedCheckInTime(save.getEstimatedCheckInTime())
                .location(save.getLocation())
                .build();

        return BookingResponse.builder()
                .code(ResourceConstant.START_SESSION_BOOKING)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bookingSessionResponse)
                .message(getMessageBundle(ResourceConstant.START_SESSION_BOOKING))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BookingResponse confirmSessionBooking(String jwtToken, BookingConfirmSessionInfoRequest request) {
        if (jwtService.isTokenExpired(jwtToken)) {
            throw new ApiRequestException("Jwt Ex", "Jwt exception!..");
        }

        BookingProcess bookingProcess = getBookingProcess(jwtToken);
        bookingProcess.setReceiveMarketingEmail(request.getReceiveMarketingEmail());
        List<RoomInfo> roomInfos = roomInfoRepo.findRoomInfosByJwtToken(jwtToken);
        getRoomInfos(jwtToken, roomInfos.stream().map(roomInfoDtoMapper::applyRoomInfoRequest).toList());
        UUID uuid = confirmBooking(bookingProcess, roomInfos);

        return BookingResponse.builder()
                .code("BK_003")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(uuid)
                .message("Booking success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private BookingProcess getBookingProcess(String jwtToken) {
        return bookingProcessRepo.findBookingProcessByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.JWT_TOKEN_EXPIRATION, getMessageBundle(ResourceConstant.JWT_TOKEN_EXPIRATION)));
    }

    private List<RoomInfo> getRoomInfos(String jwtToken, List<RoomInfoRequest> request) {
        return request.stream()
                .map(roomInfoRequest -> {
                    Room room = getRoom(roomInfoRequest.getRoomId());
                    if (room.getQuantityRoom() < roomInfoRequest.getQuantityRoom()) {
                        throw new ApiRequestException("ROOM_", "Number of rooms is not enough!..");
                    }
                    return RoomInfo.builder()
                            .jwtToken(jwtToken)
                            .roomId(room.getId())
                            .quantityRoom(roomInfoRequest.getQuantityRoom())
                            .build();
                })
                .toList();
    }

    private BigDecimal getTotalPriceBookingPromotion(Promotion promotion, BigDecimal priceRoom) {
        return promotion != null
                ? priceRoom.subtract(priceRoom.multiply(BigDecimal.valueOf(promotion.getDiscountPercent() / 100)))
                : priceRoom;
    }

    private UUID confirmBooking(BookingProcess request, List<RoomInfo> roomInfos) {
        UUID hotelId = request.getHotelId();
        Promotion promotion = getPromotion(hotelId);
        Hotel hotel = getHotel(hotelId);
        long totalDay = ChronoUnit.DAYS.between(request.getCheckInDate(), request.getCheckOutDate());
        List<BookingRoomResponse> bookingRoomResponses = new ArrayList<>();

        roomInfos.forEach(item -> {
            Room room = getRoom(item.getRoomId());
            if (room.getQuantityRoom() < item.getQuantityRoom()) {
                throw new ApiRequestException("ROOM_002", "Number of rooms is not enough!..");
            }
            BigDecimal priceRoom = room.getPricePerNight().multiply(BigDecimal.valueOf(totalDay));
            bookingRoomResponses.add(
                    BookingRoomResponse.builder()
                            .room(room)
                            .roomName(room.getRoomName())
                            .quantityRoom(item.getQuantityRoom())
                            .totalDay(totalDay)
                            .totalPriceOrigin(priceRoom)
                            .promotion(promotion != null ? promotion.getDiscountPercent() : null)
                            .totalPricePromotion(getTotalPriceBookingPromotion(promotion, priceRoom))
                            .build()
            );
        });

        double totalPrice = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPricePromotion)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        User user = null;
        String authorization = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (authorization != null) {
            user = getUserByEmailAndIsDeleted(authorization);
        }
        String bkCode = randomCode();
        String pCode = randomCode();

        String fullName = request.getFirstName() + " " + request.getLastName();
        String address = hotel.getStreetAddress() + " " + hotel.getDistrictAddress() + " " + hotel.getCountry();
        Booking booking = bookingRepo.save(
                Booking.builder()
                        .user(user)
                        .hotel(hotel)
                        .bookingCode(bkCode)
                        .pinCode(pCode)
                        .firstName(request.getFirstName())
                        .lastName(request.getLastName())
                        .fullName(fullName)
                        .phoneNumber(request.getPhoneNumber())
                        .email(request.getEmail())
                        .bookingDate(LocalDate.now())
                        .totalPrice(BigDecimal.valueOf(totalPrice))
                        .status(BookingConstant.STATUS_BOOKING_SUCCESS)
                        .isDeleted(SystemConstant.ACTIVE)
                        .checkInDate(request.getCheckInDate())
                        .checkOutDate(request.getCheckOutDate())
                        .quantityAdult(request.getQuantityAdult())
                        .quantityChildren(request.getQuantityChildren())
                        .bookingForMe(request.getBookingForMe())
                        .businessTravel(request.getBusinessTravel())
                        .electronicConfirm(request.getElectronicConfirm())
                        .orderCar(request.getOrderCar())
                        .orderTaxi(request.getOrderTaxi())
                        .pickUpService(request.getPickUpService())
                        .specialRequirements(request.getSpecialRequirements())
                        .estimatedCheckInTime(request.getEstimatedCheckInTime())
                        .location(request.getLocation())
                        .country(request.getCountry())
                        .receiveMarketingEmail(request.getReceiveMarketingEmail())
                        .address(address)
                        .build()
        );

        List<BookingDetail> bookingDetails = bookingRoomResponses.stream()
                .map(item -> {
                    Room room = item.getRoom();
                    room.setQuantityRoom(room.getQuantityRoom() - item.getQuantityRoom());
                    roomRepo.save(room);

                    HistoryBooking historyBooking = historyBookingRepo.save(
                            HistoryBooking.builder()
                                    .bookingCode("stay" + randomCode())
                                    .pinCode(randomCode())
                                    .totalDay(totalDay)
                                    .fullName(fullName)
                                    .bookingDate(LocalDate.now())
                                    .bedName(room.getBedName())
                                    .roomName(room.getRoomName())
                                    .roomTypeName(room.getRoomType().getName())
                                    .hotelId(hotelId)
                                    .build()
                    );

                    paymentRepo.save(
                            Payment.builder()
                                    .paymentAmount(booking.getTotalPrice())
                                    .booking(booking)
                                    .status(PaymentConstant.STATUS_PAYMENT_PENDING)
                                    .isDeleted(SystemConstant.ACTIVE)
                                    .build()
                    );

                    return BookingDetail.builder()
                            .room(item.getRoom())
                            .booking(booking)
                            .quantityRoomBooking(item.getQuantityRoom())
                            .checkInDate(request.getCheckInDate())
                            .checkOutDate(request.getCheckOutDate())
                            .totalDay(item.getTotalDay())
                            .totalPriceRoom(item.getTotalPricePromotion())
                            .totalPriceRoomOrigin(item.getTotalPriceOrigin())
                            .discount(item.getPromotion())
                            .bookingCode(historyBooking.getBookingCode())
                            .pinCode(historyBooking.getPinCode())
                            .isDeleted(SystemConstant.ACTIVE)
                            .status(SystemConstant.STATUS_ACTIVE)
                            .build();
                })
                .collect(Collectors.toList());
        List<BookingDetail> bookingDetailsSave = bookingDetailRepo.saveAll(bookingDetails);
        booking.setBookingDetails(bookingDetailsSave);
        bookingRepo.save(booking);

        // Call sendMail Async
        emailService.sendMailConfirmBooking(fullName, request.getEmail(), request.getPhoneNumber(), bkCode, pCode, hotel.getUser().getEmail(), totalPrice, booking.getBookingDate(), bookingDetailsSave);

        return booking.getId();
    }

    private Promotion getPromotion(UUID hotelId) {
        return promotionRepo.findPromotionByStatusAndHotelId(hotelId).get(0);
    }

    @Override
    public BookingResponses findBookingsByCondition(BookingFindCondition request, int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Booking> bookings = bookingRepo.findBookingsByCondition(request, pageable);

        List<BookingUserDTO> list = bookings.stream()
                .map(item -> bookingDTOMapper.applyBookingUserDTO(item, null))
                .toList();

        return BookingResponses.builder()
                .code(ResourceConstant.BK_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message(getMessageBundle(ResourceConstant.BK_004))
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, bookings.getTotalPages()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BookingResponse changeStatusBooking(UUID bookingId, String status) {
        Booking booking = bookingRepo.findById(bookingId)
                .orElseThrow(() -> new ApiRequestException("BK_001", "Find booking by id not found!"));

        switch (status) {
            case BookingConstant.STATUS_BOOKING_PENDING:
                booking.setStatus(BookingConstant.STATUS_BOOKING_PENDING);
                break;
            case BookingConstant.STATUS_BOOKING_SUCCESS:
                booking.setStatus(BookingConstant.STATUS_BOOKING_SUCCESS);
                break;
            case BookingConstant.STATUS_BOOKING_CANCEL:
                booking.getBookingDetails().forEach(item -> {
                    Room room = roomRepo.findById(item.getRoom().getId())
                            .orElseThrow(() -> new ApiRequestException("ROOM_001", "Find room not found!.."));
                    room.setQuantityRoom(room.getQuantityRoom() + item.getQuantityRoomBooking());
                    roomRepo.save(room);
                });

                booking.setStatus(BookingConstant.STATUS_BOOKING_CANCEL);
                break;
            default:
                throw new ApiRequestException("BK_002", "Status booking not map!..");
        }

        Booking save = bookingRepo.save(booking);
        return BookingResponse.builder()
                .code("BK_005")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bookingDTOMapper.applyBookingUserDTO(save, null))
                .message("Change status booking success!.")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    public BookingResponse getDataSessionBooking(String jwtToken, String status) {
        if (jwtService.isTokenExpired(jwtToken)) {
            throw new ApiRequestException("Jwt Ex", "Jwt exception!..");
        }
        BookingProcess bookingProcess = getBookingProcess(jwtToken);
        Hotel hotel = getHotel(bookingProcess.getHotelId());
        Object data;
        long totalDay = ChronoUnit.DAYS.between(bookingProcess.getCheckInDate(), bookingProcess.getCheckOutDate());
        List<RoomInfo> roomInfos = roomInfoRepo.findRoomInfosByJwtToken(jwtToken);
        Promotion promotion = getPromotion(bookingProcess.getHotelId());
        List<BookingRoomResponse> bookingRoomResponses = new ArrayList<>();
        roomInfos.forEach(item -> {
            Room room = getRoom(item.getRoomId());
            if (room.getQuantityRoom() < item.getQuantityRoom()) {
                throw new ApiRequestException("ROOM_", "Number of rooms is not enough!..");
            }
            BigDecimal priceRoom = room.getPricePerNight().multiply(BigDecimal.valueOf(totalDay));
            bookingRoomResponses.add(
                    BookingRoomResponse.builder()
                            .quantityRoom(item.getQuantityRoom())
                            .totalPriceOrigin(priceRoom)
                            .totalPricePromotion(getTotalPriceBookingPromotion(promotion, priceRoom))
                            .build()
            );
        });

        double totalPrice = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPricePromotion)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        double totalPriceOrigin = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getTotalPriceOrigin)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        int totalRoom = bookingRoomResponses.stream()
                .map(BookingRoomResponse::getQuantityRoom)
                .mapToInt(Integer::intValue)
                .sum();
        switch (status) {
            case BookingConstant.STATUS_BOOKING_SET_PLACE:
                data = BookingSessionResponse.builder()
                        .hotelName(hotel.getName())
                        .address(hotel.getStreetAddress())
                        .totalReview(9.0)
                        .checkInDate(bookingProcess.getCheckInDate())
                        .checkOutDate(bookingProcess.getCheckOutDate())
                        .totalDay(totalDay)
                        .quantityRoom(totalRoom)
                        .quantityAdult(bookingProcess.getQuantityAdult())
                        .quantityChildren(bookingProcess.getQuantityChildren())
                        .priceOrigin(totalPriceOrigin)
                        .pricePromotion(totalPrice)
                        .build();
                break;
            case BookingConstant.STATUS_BOOKING_PENDING:
                data = BookingSessionInfoResponse.builder()
                        .hotelName(hotel.getName())
                        .address(hotel.getStreetAddress())
                        .totalReview(9.0)
                        .checkInDate(bookingProcess.getCheckInDate())
                        .checkOutDate(bookingProcess.getCheckOutDate())
                        .totalDay(totalDay)
                        .quantityRoom(totalRoom)
                        .quantityAdult(bookingProcess.getQuantityAdult())
                        .quantityChildren(bookingProcess.getQuantityChildren())
                        .priceOrigin(totalPriceOrigin)
                        .pricePromotion(totalPrice)
                        .jwtToken(bookingProcess.getJwtToken())
                        .firstName(bookingProcess.getFirstName())
                        .lastName(bookingProcess.getLastName())
                        .email(bookingProcess.getEmail())
                        .country(bookingProcess.getCountry())
                        .phoneNumber(bookingProcess.getPhoneNumber())
                        .address(hotel.getStreetAddress())
                        .bookingForMe(bookingProcess.getBookingForMe())
                        .businessTravel(bookingProcess.getBusinessTravel())
                        .electronicConfirm(bookingProcess.getElectronicConfirm())
                        .orderCar(bookingProcess.getOrderCar())
                        .orderTaxi(bookingProcess.getOrderTaxi())
                        .pickUpService(bookingProcess.getPickUpService())
                        .specialRequirements(bookingProcess.getSpecialRequirements())
                        .estimatedCheckInTime(bookingProcess.getEstimatedCheckInTime())
                        .location(bookingProcess.getLocation())
                        .build();
                break;
            default:
                throw new ApiRequestException("BKP_003", "Get booking process not found!..");
        }

        return BookingResponse.builder()
                .code(ResourceConstant.BK_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(data)
                .message(getMessageBundle(ResourceConstant.BK_004))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BookingResponses getBookingsByUser() {
        User user = getUser();
        List<Booking> bookings = bookingRepo.findBookingsByUser(user.getId());

        if (bookings.isEmpty()) {
            throw new ApiRequestException("BKP_008", "Bookings not found!..");
        }

        Hotel hotel = bookings.stream()
                .map(Booking::getHotel)
                .toList().get(0);

        UUID hotelId = hotel.getId();

        String hotelImage = hotelImageRepo.findHotelImageByHotelId(hotelId).get(0);

        String address = hotel.getStreetAddress() + " " + hotel.getDistrictAddress() + " " + hotel.getCountry();

        HotelBookingResponse build = HotelBookingResponse.builder()
                .hotelId(hotelId)
                .hotelImage(hotelImage)
                .hotelAddress(address)
                .hotelName(hotel.getName())
                .city(hotel.getCity())
                .build();

        List<BookingUserDTO> bookingDTOS = bookings.stream()
                .map(item -> bookingDTOMapper.applyBookingUserDTO(item, build))
                .toList();

        return BookingResponses.builder()
                .code("BK_008")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bookingDTOS)
                .message("Get bookings by user success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BookingResponse cancelBookingByUser(UUID bookingId) {
        changeStatusBooking(bookingId, BookingConstant.STATUS_BOOKING_CANCEL);

        return BookingResponse.builder()
                .code("BK_010")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bookingId)
                .message("Cancel booking success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }


    private Room getRoom(UUID roomId) {
        return roomRepo.findById(roomId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.RO_001, getMessageBundle(ResourceConstant.RO_001)));
    }

    private String randomCode() {
        Random rd = new Random();
        return String.valueOf(rd.nextInt(10)) +
               rd.nextInt(10) +
               rd.nextInt(10) +
               rd.nextInt(10) +
               rd.nextInt(10) +
               rd.nextInt(10);
    }

    private User getUser(String email) {
        return userRepo.findUserByEmailAndStatus(email, SystemConstant.STATUS_ACTIVE)
                .orElseThrow(() -> new UsernameNotFoundException("find user by " + email + " not found!."));
    }

    private User getUserByEmailAndIsDeleted(String token) {
        final String jwt = token.substring(7);
        final String email = jwtService.extractUsername(jwt);
        return getUser(email);
    }
}
