package com.example.graduationthesis.booking.service;

import com.example.graduationthesis.booking.dto.request.BookingConfirmSessionInfoRequest;
import com.example.graduationthesis.booking.dto.request.BookingFindCondition;
import com.example.graduationthesis.booking.dto.request.BookingStartSessionInfoRequest;
import com.example.graduationthesis.booking.dto.request.BookingStartSessionRequest;
import com.example.graduationthesis.booking.dto.response.BookingResponse;
import com.example.graduationthesis.booking.dto.response.BookingResponses;

import java.io.IOException;
import java.util.UUID;

public interface BookingService {

    BookingResponse starSessionBooking(BookingStartSessionRequest request);

    BookingResponse starSessionBooking(String jwtToken, BookingStartSessionInfoRequest request);

    BookingResponse confirmSessionBooking(String jwtToken, BookingConfirmSessionInfoRequest request);

    BookingResponses findBookingsByCondition(BookingFindCondition request, int currentPage, int limitPage);

    BookingResponse changeStatusBooking(UUID bookingId, String status);

    BookingResponse getDataSessionBooking(String jwtToken, String status);

    BookingResponses getBookingsByUser();

    BookingResponse cancelBookingByUser(UUID bookingId);

}
