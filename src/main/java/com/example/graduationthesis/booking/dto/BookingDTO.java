package com.example.graduationthesis.booking.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

public record BookingDTO(
        UUID userId,
        UUID roomId,
        BigDecimal totalPrice
) {
}
