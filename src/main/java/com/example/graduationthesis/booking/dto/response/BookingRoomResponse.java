package com.example.graduationthesis.booking.dto.response;

import com.example.graduationthesis.room.entity.Room;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class BookingRoomResponse {

    private Room room;

    private String roomName;

    private Integer quantityRoom;

    private long totalDay;

    private Double promotion;

    private BigDecimal totalPriceOrigin;

    private BigDecimal totalPricePromotion;
}
