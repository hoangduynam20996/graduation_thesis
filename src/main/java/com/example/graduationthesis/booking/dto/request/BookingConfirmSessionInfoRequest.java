package com.example.graduationthesis.booking.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BookingConfirmSessionInfoRequest {

    private Boolean receiveMarketingEmail;
}
