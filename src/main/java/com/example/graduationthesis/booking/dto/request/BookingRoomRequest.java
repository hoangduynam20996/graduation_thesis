package com.example.graduationthesis.booking.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class BookingRoomRequest {

    private UUID roomId;

    private Integer quantityRoom;
}
