package com.example.graduationthesis.booking.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class BookingFindCondition {

    private String hotelName;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private LocalDate bookingDate;

    private LocalDate bookingDateTo;

    private String nameContact;

    private String phoneNumber;

    private String statusBooking;

    private String statusPayment;

    private String bookingCode;

    private String pinCode;

    private String roomName;
}
