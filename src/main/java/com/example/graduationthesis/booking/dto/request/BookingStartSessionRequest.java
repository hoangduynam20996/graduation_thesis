package com.example.graduationthesis.booking.dto.request;

import com.example.graduationthesis.roominfobookingprocess.dto.request.RoomInfoRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class BookingStartSessionRequest {

    private UUID hotelId;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private Integer quantityRoom;

    private Integer quantityAdult;

    private Integer quantityChildren;

    private String location;

    private List<RoomInfoRequest> roomInfoRequests;
}
