package com.example.graduationthesis.booking.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
public abstract class AbsBookingSessionResp {

    private String hotelName;

    private String address;

    private Double totalReview;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private long totalDay;

    private Integer quantityRoom;

    private Integer quantityAdult;

    private Integer quantityChildren;

    private Double priceOrigin;

    private Double pricePromotion;

    private String jwtToken;

}
