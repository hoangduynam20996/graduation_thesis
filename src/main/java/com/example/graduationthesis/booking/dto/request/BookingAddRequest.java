package com.example.graduationthesis.booking.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class BookingAddRequest {

    private UUID hotelId;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private List<BookingRoomRequest> roomIds;
}
