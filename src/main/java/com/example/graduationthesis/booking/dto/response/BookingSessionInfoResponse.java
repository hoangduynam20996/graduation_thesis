package com.example.graduationthesis.booking.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class BookingSessionInfoResponse extends AbsBookingSessionResp {

    private String email;

    private String phoneNumber;

    private String firstName;

    private String lastName;

    private Boolean bookingForMe;

    private Boolean businessTravel;

    private String location;

    private String country;

    private Boolean electronicConfirm;

    private Boolean orderCar;

    private Boolean orderTaxi;

    private Boolean pickUpService;

    private String specialRequirements;

    private String estimatedCheckInTime;

}
