package com.example.graduationthesis.booking.entity;

import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.payment.entity.Payment;
import com.example.graduationthesis.revenue.entity.Revenue;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_booking")
public class Booking extends BaseIDUtil {

    @Column
    private String fullName;
    @Column
    private String email;
    @Column
    private String address;
    @Column
    private String phoneNumber;
    @Column
    private String bookingCode;
    @Column
    private String pinCode;
    @Column
    private BigDecimal totalPrice;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @Column
    private LocalDate bookingDate;
    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY)
    private List<BookingDetail> bookingDetails;
    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY)
    private List<Revenue> revenues;
    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY)
    private List<Payment> payments;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private Integer quantityRoom;

    private Integer quantityAdult;

    private Integer quantityChildren;

    private String firstName;

    private String lastName;

    private Boolean bookingForMe;

    private Boolean businessTravel;

    private String location;

    private String country;

    private Boolean electronicConfirm;

    private Boolean orderCar;

    private Boolean orderTaxi;

    private Boolean pickUpService;

    private String specialRequirements;

    private String estimatedCheckInTime;

    private Boolean receiveMarketingEmail;
}
