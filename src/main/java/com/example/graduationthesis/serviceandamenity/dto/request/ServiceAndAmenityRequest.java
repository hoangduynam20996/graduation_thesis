package com.example.graduationthesis.serviceandamenity.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ServiceAndAmenityRequest {

    private String name;

    private String type;

    private BigDecimal price;

    private String description;
}
