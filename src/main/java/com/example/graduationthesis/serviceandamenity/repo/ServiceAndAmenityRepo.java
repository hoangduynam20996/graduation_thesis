package com.example.graduationthesis.serviceandamenity.repo;

import com.example.graduationthesis.serviceandamenity.entity.ServiceAndAmenity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ServiceAndAmenityRepo extends JpaRepository<ServiceAndAmenity, UUID> {

    boolean existsByName(String name);
}
