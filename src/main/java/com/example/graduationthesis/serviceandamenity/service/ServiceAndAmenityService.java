package com.example.graduationthesis.serviceandamenity.service;

import com.example.graduationthesis.serviceandamenity.dto.request.ServiceAndAmenityRequest;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityResponse;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityResponses;

import java.util.UUID;

public interface ServiceAndAmenityService {

    ServiceAndAmenityResponse addServiceAndAmenity(ServiceAndAmenityRequest request);

    ServiceAndAmenityResponse updateServiceAndAmenity(ServiceAndAmenityRequest request, UUID said);

    ServiceAndAmenityResponse deleteServiceAndAmenity(UUID said);

    ServiceAndAmenityResponse findById(UUID said);

    ServiceAndAmenityResponses findAll(int currentPage, int limitPage);

}
