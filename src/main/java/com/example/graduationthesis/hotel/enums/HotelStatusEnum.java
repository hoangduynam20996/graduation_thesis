package com.example.graduationthesis.hotel.enums;

public enum HotelStatusEnum {

    ACTIVE, NO_ACTIVE, INACTIVE

}
