package com.example.graduationthesis.hotel.enums;

public enum HotelTypeEnum {

    LUX,
    NORMAL,
    VIP;
}
