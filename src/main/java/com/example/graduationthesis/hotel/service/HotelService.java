package com.example.graduationthesis.hotel.service;

import com.example.graduationthesis.hotel.dto.request.HotelAddTypeRequest;
import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.request.HotelStarSessionOne;
import com.example.graduationthesis.hotel.dto.request.HotelUpdateByOwnerRequest;
import com.example.graduationthesis.hotel.dto.response.HotelIndexResponses;
import com.example.graduationthesis.hotel.dto.response.HotelResponse;
import com.example.graduationthesis.hotel.dto.response.HotelResponses;
import com.example.graduationthesis.hotel.dto.response.HotelSearchResultResponse;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface HotelService {

    HotelResponse addHotelSessionOne(HotelStarSessionOne request);

    HotelResponse addHotelSessionTwo(String jwtToken, HotelAddTypeRequest request);

    HotelResponse updateHotelWithRoleOwner(HotelUpdateByOwnerRequest request);

    HotelResponse updateStatusHotelWithRoleAdminMaster(UUID hid, String status);

    HotelIndexResponses findHotelsIndex();

    HotelResponses findAll(String status, Integer currentPage, Integer limitPage);

    HotelResponse findHotelByConditionAndId(HotelSearchRequest request);

    HotelSearchResultResponse findAllHotelByCondition(HotelSearchRequest request, int currentPage, int limitPage);

    HotelResponse getProfileHotelRegister(String jwtToken, String status);

    HotelResponse getHotelByHotelFavouriteByUser();

    HotelResponse getHotelAndCategoryByHotelId(UUID hotelId);

    // Learning Specification and Criteria
    HotelResponse findHotelsByCondition(int currentPage, int limitPage, String sortBy);

    HotelResponse findHotelsByCondition(int currentPage, int limitPage, String... sortBys);

    HotelResponse findHotelsByCondition(int currentPage, int limitPage, String sortBy, String roomName, String... search);

    HotelResponses findHotelsByCondition(Pageable pageable, String[] hotel, String[] room);

}
