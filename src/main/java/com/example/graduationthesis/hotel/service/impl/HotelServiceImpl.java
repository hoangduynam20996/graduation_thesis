package com.example.graduationthesis.hotel.service.impl;

import com.example.graduationthesis.category.dto.response.CategoryResponses;
import com.example.graduationthesis.category.service.CategoryService;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.dto.HotelDTO;
import com.example.graduationthesis.hotel.dto.request.HotelAddTypeRequest;
import com.example.graduationthesis.hotel.dto.request.HotelSearchImageResponse;
import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.request.HotelStarSessionOne;
import com.example.graduationthesis.hotel.dto.request.HotelUpdateByOwnerRequest;
import com.example.graduationthesis.hotel.dto.response.HotelDTOFavouriteResponse;
import com.example.graduationthesis.hotel.dto.response.HotelDTOResponse;
import com.example.graduationthesis.hotel.dto.response.HotelGroupResp;
import com.example.graduationthesis.hotel.dto.response.HotelIndexResponse;
import com.example.graduationthesis.hotel.dto.response.HotelIndexResponses;
import com.example.graduationthesis.hotel.dto.response.HotelOneImageAndCategory;
import com.example.graduationthesis.hotel.dto.response.HotelResponse;
import com.example.graduationthesis.hotel.dto.response.HotelResponses;
import com.example.graduationthesis.hotel.dto.response.HotelSearchIdResultResponse;
import com.example.graduationthesis.hotel.dto.response.HotelSearchResult;
import com.example.graduationthesis.hotel.dto.response.HotelSearchResultResponse;
import com.example.graduationthesis.hotel.dto.response.HotelSearchRoomRecordResponse;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.mapper.HotelDTOMapper;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.hotel.repo.cretiria.HotelCriteriaRepo;
import com.example.graduationthesis.hotel.repo.specification.HotelSpecBuilder;
import com.example.graduationthesis.hotel.service.HotelService;
import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteDTOResp;
import com.example.graduationthesis.hotelfavourite.entity.HotelFavourite;
import com.example.graduationthesis.hotelfavourite.repo.HotelFavouriteRepo;
import com.example.graduationthesis.hotelimage.repo.HotelImageRepo;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeIndexResp;
import com.example.graduationthesis.hoteltype.entity.HotelType;
import com.example.graduationthesis.hoteltype.repo.HotelTypeRepo;
import com.example.graduationthesis.jwt.JwtService;
import com.example.graduationthesis.promotion.entity.Promotion;
import com.example.graduationthesis.promotion.repo.PromotionRepo;
import com.example.graduationthesis.review.dto.response.ReviewHotelIndexResponse;
import com.example.graduationthesis.review.repo.ReviewRepo;
import com.example.graduationthesis.review.service.ReviewService;
import com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName;
import com.example.graduationthesis.reviewcategory.repo.ReviewCategoryRepo;
import com.example.graduationthesis.room.repo.RoomRepo;
import com.example.graduationthesis.serviceandamenityroom.ServiceAndAmenitySqlNativeResult;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAndAmenityRoomNameResponse;
import com.example.graduationthesis.serviceandamenityroom.repo.ServiceAndAmenityRoomRepo;
import com.example.graduationthesis.session.common.SessionResponseUtil;
import com.example.graduationthesis.session.hotel.HotelSession;
import com.example.graduationthesis.session.hotel.HotelSessionRepo;
import com.example.graduationthesis.session.hotelimage.HotelImageSessionRepo;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSessionRepo;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySessionRepo;
import com.example.graduationthesis.session.room.RoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSession;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomId;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomIdRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class HotelServiceImpl extends AbsUserServiceUtil implements HotelService {

    private final HotelRepo hotelRepo;

    private final HotelFavouriteRepo hotelFavouriteRepo;

    private final HotelImageRepo hotelImageRepo;

    private final HotelDTOMapper hotelDTOMapper;

    private final HotelTypeRepo hotelTypeRepo;

    private final RoomRepo roomRepo;

    private final PromotionRepo promotionRepo;

    private final ReviewRepo reviewRepo;

    private final ReviewService reviewService;

    private final ReviewCategoryRepo reviewCategoryRepo;

    private final ServiceAndAmenityRoomRepo serviceAndAmenityRoomRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private final HotelSessionRepo hotelSessionRepo;

    private final JwtService jwtService;

    private final ServiceAmenityRoomSessionRepo serviceAmenityRoomSessionRepo;

    private final ServiceAmenityRoomIdRepo serviceAmenityRoomIdRepo;

    private final HotelPaymentMethodSessionRepo hotelPaymentMethodSessionRepo;

    private final HotelPolicySessionRepo hotelPolicySessionRepo;

    private final RoomSessionRepo roomSessionRepo;

    private final HotelImageSessionRepo hotelImageSessionRepo;

    private final CategoryService categoryService;

    private final HotelCriteriaRepo hotelCriteriaRepo;

    public String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public HotelResponse addHotelSessionOne(HotelStarSessionOne request) {
        HotelType hotelType = hotelTypeRepo.findById(request.getHotelTypeId())
                .orElseThrow(() -> new ApiRequestException("HT_001", "Find hotel type by id not found!.."));

        String jwtToken = jwtService.generateToken();

        HotelSession hotelSession = hotelSessionRepo.save(
                HotelSession.builder()
                        .hotelTypeId(hotelType.getId())
                        .jwtToken(jwtToken)
                        .build()
        );

        return HotelResponse.builder()
                .code("SSH_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("HOTEL", hotelSession))
                .message("Start session add hotel success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponse addHotelSessionTwo(String jwtToken, HotelAddTypeRequest request) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        HotelSession hotelSession = hotelSessionRepo.findHotelSessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("HTS_001", "Find hotel session by jwtToken not found!."));

        HotelSession build = hotelSessionRepo.save(
                hotelSession.toBuilder()
                        .name(request.getName())
                        .rating(request.getRating())
                        .contactPerson(request.getContactPerson())
                        .phoneNumber(request.getPhoneNumber())
                        .phoneNumberTwo(request.getPhoneNumberTwo())
                        .streetAddress(request.getStreetAddress())
                        .districtAddress(request.getDistrictAddress())
                        .country(request.getCountry())
                        .city(request.getCity())
                        .postalCode(request.getPostalCode())
                        .build()
        );

        return HotelResponse.builder()
                .code("SSH_002")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("HOTEL", build))
                .message("Add hotel session two success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponse updateHotelWithRoleOwner(HotelUpdateByOwnerRequest request) {
        Hotel hotel = getHotel(request.getHotelId());

        HotelType hotelType = getHotelType(request.getHotelTypeId());

        if (!hotel.getHotelType().equals(hotelType))
            hotel.setHotelType(hotelType);

        if (!hotel.getName().equals(request.getName()))
            hotel.setName(request.getName());

        if (!Objects.equals(hotel.getRating(), request.getRating()))
            hotel.setRating(request.getRating());

        if (!hotel.getContactPerson().equals(request.getContactPersonName()))
            hotel.setContactPerson(request.getContactPersonName());

        if (!hotel.getPhoneNumber().equals(request.getPhoneNumber()))
            hotel.setPhoneNumber(request.getPhoneNumber());

        if (!hotel.getPhoneNumberTwo().equals(request.getPhoneNumberTwo()))
            hotel.setPhoneNumberTwo(request.getPhoneNumberTwo());

        if (!hotel.getStreetAddress().equals(request.getStreetAddress()))
            hotel.setStreetAddress(request.getStreetAddress());

        if (!hotel.getDistrictAddress().equals(request.getDistrictAddress()))
            hotel.setDistrictAddress(request.getDistrictAddress());

        if (!hotel.getCity().equals(request.getCity()))
            hotel.setCity(request.getCity());

        if (!hotel.getCountry().equals(request.getCountry()))
            hotel.setCountry(request.getCountry());

        if (!hotel.getPostalCode().equals(request.getPostalCode()))
            hotel.setPostalCode(request.getPostalCode());

        if (hotel.getDescription() == null || !hotel.getDescription().equals(request.getDescription()))
            hotel.setDescription(request.getDescription());

        Hotel save = hotelRepo.save(hotel);
        return HotelResponse.builder()
                .code(ResourceConstant.HT_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }

    @Override
    public HotelResponse updateStatusHotelWithRoleAdminMaster(UUID hid, String status) {
        Hotel hotel = getHotel(hid);
        hotel.setStatus(status);
        hotel.setActivationDate(new Date(System.currentTimeMillis()));
        Hotel save = hotelRepo.save(hotel);
        return HotelResponse.builder()
                .code(ResourceConstant.HT_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelIndexResponses findHotelsIndex() {
        List<HotelTypeIndexResp> hotelTypeIndexResp = hotelTypeRepo.selectHotelTypeIndex()
                .orElseThrow(() -> new ApiRequestException("find hotel type index not found!.", ""));

        List<ReviewHotelIndexResponse> reviewHotelIndexResponses = reviewRepo.findHotelsGroupByReviewTop()
                .orElseThrow(() -> new ApiRequestException("find hotel by group review not found!.", ""));

        reviewHotelIndexResponses.forEach(item -> {
            item.setUrlImage(hotelImageRepo.findHotelImageByHotelId(item.getHotelId()).get(0));
            item.setReviewRating(getTotalRatingReview(item.getHotelId()));
        });

        List<HotelFavouriteDTOResp> hotelFavouriteResponses = hotelFavouriteRepo.findHotelByFavourite()
                .orElseThrow(() -> new ApiRequestException("find hotel by favourite not found!.", ""));

        hotelFavouriteResponses.forEach(item -> {
            item.setUrlImage(hotelImageRepo.findHotelImageByHotelId(item.getHotelId()).get(0));
            item.setReviewRating(getTotalRatingReview(item.getHotelId()));
            item.setCountReview(reviewService.countReviewByHotelId(item.getHotelId()));
        });

        List<HotelGroupResp> hotelGroupPostal = hotelRepo.findRoomsGroupByPostalCode()
                .orElseThrow(() -> new ApiRequestException("find room group by postal code not found!", ""));

        List<HotelGroupResp> hotelGroupCities = hotelRepo.findRoomsGroupByCity()
                .orElseThrow(() -> new ApiRequestException("find room group by city not found!", ""));

        HotelIndexResponse build = HotelIndexResponse.builder()
                .hotelTypeIndexResp(hotelTypeIndexResp)
                .reviewHotelIndexResp(reviewHotelIndexResponses)
                .hotelFavouriteResp(hotelFavouriteResponses)
                .hotelGroupPostalCodeResp(hotelGroupPostal)
                .hotelGroupCityResp(hotelGroupCities)
                .build();

        return HotelIndexResponses.builder()
                .code("HT_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(build)
                .message("Get hotel index success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponses findAll(String status, Integer page, Integer limit) {
        Pageable pageable = baseAmenityUtil.pageable(page, limit);
        Page<Hotel> hotels = hotelRepo.findHotelsByStatusAndOrderByCreatedDateDesc(status, pageable);

        List<HotelDTO> hotelDTOS = hotels.stream()
                .map(hotelDTOMapper)
                .toList();

        return HotelResponses.builder()
                .code(ResourceConstant.HT_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelDTOS)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, hotels.getTotalPages()))
                .message(getMessageBundle(ResourceConstant.HT_004))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponse findHotelByConditionAndId(HotelSearchRequest request) {
        Page<HotelDTOResponse> hotels = hotelRepo.findHotelsByCondition(request, 1, 8);
        HotelDTOResponse hotelDTOResponse = hotels.stream().findFirst().orElse(null);

        assert hotelDTOResponse != null;
        return HotelResponse.builder()
                .code("HT_003")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(toHotelSearchIdResultResponse(hotelDTOResponse, request))
                .message("Get hotel detail success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelSearchResultResponse findAllHotelByCondition(HotelSearchRequest request, int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<HotelDTOResponse> results = hotelRepo.findHotelsByCondition(request, currentPage, limitPage);

        List<HotelSearchResult> hotelSearchResults = apply(results, request);
        List<HotelSearchResult> hotelSearchResultList = hotelSearchResults.stream()
                .filter(item -> request.getPriceFindStart().compareTo(item.getTotalMoneyPromotion()) <= 0
                                && request.getPriceFindEnd().compareTo(item.getTotalMoneyPromotion()) >= 0)
                .toList();

        return HotelSearchResultResponse.builder()
                .code("HT_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelSearchResultList)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, results.getTotalPages()))
                .message("Get hotels by condition success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override

    public HotelResponse getProfileHotelRegister(String jwtToken, String status) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        Object data = null;

        switch (status) {
            case "HOTEL":
                data = hotelSessionRepo.findHotelSessionByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find hotel by jwtToken not found!.."));
                break;
            case "ROOM":
                data = roomSessionRepo.findRoomSessionByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find room by jwtToken not found!.."));
                break;
            case "HOTEL_SERVICE":
                Map<String, Object> value = new HashMap<>();
                ServiceAmenityRoomSession serviceAmenityRoomSession = serviceAmenityRoomSessionRepo.findServiceAmenityRoomSessionByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find service and room by jwtToken not found!.."));

                List<ServiceAmenityRoomId> serviceAmenityRoomIds = serviceAmenityRoomIdRepo.findServiceAmenityRoomIdByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find hotel by jwtToken not found!.."));
                value.put("SERVICE", serviceAmenityRoomSession);
                value.put("SERVICE_ID", serviceAmenityRoomIds);
                data = value;
                break;
            case "HOTEL_IMAGE":
                data = hotelImageSessionRepo.findHotelImageSessionByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find hotel by jwtToken not found!.."));
                break;
            case "HOTEL_POLICY":
                data = hotelPolicySessionRepo.findHotelPolicySessionByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find hotel policy by jwtToken not found!.."));
                break;
            case "HOTEL_PAYMENT_METHOD":
                data = hotelPaymentMethodSessionRepo.findHotelPaymentMethodSessionByJwtToken(jwtToken)
                        .orElseThrow(() -> new ApiRequestException("HTS_008", "find hotel payment by jwtToken not found!.."));
                break;
            default:
                throw new ApiRequestException("HTS_009", "Get hotel register fail...");
        }

        return HotelResponse.builder()
                .code("HT_010")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(data)
                .message("Get hotel register success...")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponse getHotelByHotelFavouriteByUser() {
        User user = getUser();

        if (user.getHotelFavourites().isEmpty()) {
            throw new ApiRequestException("HTS_010", "get hotel by hotel favourites not found!");
        }

        List<Hotel> hotels = user.getHotelFavourites().stream()
                .filter(item -> item.getStatus().equals(SystemConstant.STATUS_ACTIVE))
                .map(HotelFavourite::getHotel)
                .toList();

        List<HotelDTOFavouriteResponse> hotelDTOFavouriteResponses = hotels.stream()
                .map(item -> new HotelDTOFavouriteResponse(
                        item.getId(),
                        item.getName(),
                        item.getRating(),
                        item.getStreetAddress(),
                        item.getDistrictAddress(),
                        item.getCity(),
                        item.getCountry(),
                        item.getCountView(),
                        hotelImageRepo.findHotelImageByHotelId(item.getId()).get(0),
                        item.getDescription()
                ))
                .toList();

        return HotelResponse.builder()
                .code("HT_008")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelDTOFavouriteResponses)
                .message("Get hotel by hotel favourite by user success...")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    //get hotel with one Image and category by hotel id
    @Override
    public HotelResponse getHotelAndCategoryByHotelId(UUID hotelId) {
        Hotel hotel = getHotel(hotelId);
        String hotelImage = hotelImageRepo.findHotelImageByHotelId(hotelId).get(0);
        CategoryResponses categoryDTOS = categoryService.findAllCategoryByStatus(1, 8, SystemConstant.STATUS_ACTIVE);

        HotelOneImageAndCategory hotelOneImageAndCategory = HotelOneImageAndCategory.builder()
                .id(hotel.getId())
                .name(hotel.getName())
                .description(hotel.getDescription())
                .rating(hotel.getRating())
                .contactPerson(hotel.getContactPerson())
                .phoneNumber(hotel.getPhoneNumber())
                .phoneNumberTwo(hotel.getPhoneNumberTwo())
                .streetAddress(hotel.getStreetAddress())
                .districtAddress(hotel.getDistrictAddress())
                .country(hotel.getCountry())
                .city(hotel.getCity())
                .status(hotel.getStatus())
                .postalCode(hotel.getPostalCode())
                .countView(hotel.getCountView())
                .image(hotelImage)
                .categories(categoryDTOS.getData())
                .build();

        return HotelResponse.builder()
                .code("HT_007")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelOneImageAndCategory)
                .message("Get hotel and category by hotel id success...")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    /* Learning
     * @author Duy Nam
     * */
    @Override
    public HotelResponse findHotelsByCondition(int currentPage, int limitPage, String sortBy) {
        List<Sort.Order> orders = new ArrayList<>();
        if (StringUtils.hasLength(sortBy)) {
            Pattern pattern = Pattern.compile("(\\w+?)(:)(.*)");
            Matcher matcher = pattern.matcher(sortBy);
            if (matcher.find()) {
                if (matcher.group(3).equalsIgnoreCase("asc")) {
                    orders.add(new Sort.Order(Sort.Direction.ASC, matcher.group(1)));
                }

                if (matcher.group(3).equalsIgnoreCase("desc")) {
                    orders.add(new Sort.Order(Sort.Direction.DESC, matcher.group(1)));
                }
            }
        }

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage, Sort.by(orders));
        Page<Hotel> all = hotelRepo.findAll(pageable);
        List<HotelDTO> hotels = all.stream()
                .map(hotelDTOMapper)
                .toList();

        return HotelResponse.builder()
                .code("HT_007")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotels)
                .message("Get hotels success...")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponse findHotelsByCondition(int currentPage, int limitPage, String... sortBys) {
        List<Sort.Order> orders = new ArrayList<>();

        for (String s : sortBys) {
            Pattern pattern = Pattern.compile("(\\w+?)(:)(.*)");
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                if (matcher.group(3).equalsIgnoreCase("asc")) {
                    orders.add(new Sort.Order(Sort.Direction.ASC, matcher.group(1)));
                }

                if (matcher.group(3).equalsIgnoreCase("desc")) {
                    orders.add(new Sort.Order(Sort.Direction.DESC, matcher.group(1)));
                }
            }
        }

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage, Sort.by(orders));
        Page<Hotel> all = hotelRepo.findAll(pageable);
        List<HotelDTO> hotels = all.stream()
                .map(hotelDTOMapper)
                .toList();

        return HotelResponse.builder()
                .code("HT_007")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotels)
                .message("Get hotels success...")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponse findHotelsByCondition(int currentPage, int limitPage, String sortBy, String roomName, String... search) {
        List<Hotel> hotels = hotelCriteriaRepo.searchHotels(currentPage, limitPage, sortBy, roomName, search);
        List<HotelDTO> hotelsResp = hotels.stream()
                .map(hotelDTOMapper)
                .toList();

        return HotelResponse.builder()
                .code("HT_007")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelsResp)
                .message("Get hotels success...")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelResponses findHotelsByCondition(Pageable pageable, String[] hotel, String[] rooms) {
        Pattern pattern = Pattern.compile("(\\w+?)([:<>~!])(.*)(\\p{Punct}?)(.*)(\\p{Punct}?)");
        HotelSpecBuilder hotelSpecBuilder = new HotelSpecBuilder();
        Page<Hotel> all = null;
        if (rooms != null && hotel != null) {
            all = hotelCriteriaRepo.getHotelsJoinRoom(pageable, hotel, rooms);
        }

        if (hotel != null && rooms == null) {
            for (String s : hotel) {
                Matcher matcher = pattern.matcher(s);
                if (matcher.find()) {
                    hotelSpecBuilder.with(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5));
                }
            }
            all = hotelRepo.findAll(hotelSpecBuilder.build(), pageable);
        }

        assert all != null;
        List<HotelDTO> hotels = all.stream()
                .map(hotelDTOMapper)
                .toList();

        return HotelResponses.builder()
                .code("HT_007")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotels)
                .message("Get hotels success...")
                .responseTime(baseAmenityUtil.responseTime())
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .build();
    }

    private Hotel getHotel(UUID hid) {
        return hotelRepo.findById(hid)
                .orElseThrow(() -> new ApiRequestException("find hotel by id " + hid + " not found!.", ""));
    }

    private HotelType getHotelType(UUID httid) {
        return hotelTypeRepo.findById(httid)
                .orElseThrow(() -> new ApiRequestException("find hotel type by id " + httid + " not found!.", ""));
    }

    private List<HotelSearchResult> apply(Page<HotelDTOResponse> results, HotelSearchRequest request) {
        long totalDay = ChronoUnit.DAYS.between(request.getCheckInDate(), request.getCheckOutDate());

        return results.stream()
                .map(item ->
                        {
                            Promotion promotion = getPromotion(item.hotelId());
                            BigDecimal priceRoom = item.priceRoom().multiply(BigDecimal.valueOf(totalDay));
                            return HotelSearchResult.builder()
                                    .adults(request.getAdults())
                                    .children(request.getChildren())
                                    .checkInDate(request.getCheckInDate())
                                    .checkOutDate(request.getCheckOutDate())
                                    .quantityRoom(request.getQuantityRoom())
                                    .hotelId(item.hotelId())
                                    .name(item.name())
                                    .reviewRating(getTotalRatingReview(item.hotelId()))
                                    .rating(item.rating())
                                    .streetAddress(item.streetAddress())
                                    .districtAddress(item.districtAddress())
                                    .city(item.city())
                                    .country(item.country())
                                    .totalDay(totalDay)
                                    .totalMoneyOriginal(priceRoom)
                                    .totalMoneyPromotion(getTotalMoneyOriginal(promotion, priceRoom))
                                    .countView(item.countView())
                                    .countReview(reviewService.countReviewByHotelId(item.hotelId()))
                                    .discountPercent(promotion == null ? 0 : promotion.getDiscountPercent())
                                    .urlImage(hotelImageRepo.findHotelImageByHotelId(item.hotelId()).get(0))
                                    .build();
                        }
                )
                .toList();
    }

    private Double getTotalRatingReview(UUID hotelId) {
        List<StatisticalRatingGroupByName> statisticalRatingGroupByNames =
                reviewCategoryRepo.statisticalRatingGroupNameByHotelId(hotelId)
                        .orElseThrow(() -> new ApiRequestException("statistic by hotel not found!.", ""));

        return statisticalRatingGroupByNames.stream()
                .mapToDouble(StatisticalRatingGroupByName::averageRating)
                .sum();
    }

    private HotelSearchIdResultResponse toHotelSearchIdResultResponse(
            HotelDTOResponse result,
            HotelSearchRequest request
    ) {
        long totalDay = ChronoUnit.DAYS.between(request.getCheckInDate(), request.getCheckOutDate());

        List<ServiceAndAmenitySqlNativeResult> serviceAndAmenitySqlNatives =
                serviceAndAmenityRoomRepo.findNameServiceAndAmenityByHotelId(result.hotelId())
                        .orElse(new ArrayList<>());

        Hotel hotel = getHotel(result.hotelId());
        hotel.setCountView(hotel.getCountView() == null ? 1 : hotel.getCountView() + 1);
        hotelRepo.save(hotel);

        Promotion promotion = getPromotion(result.hotelId());

        List<ServiceAndAmenityRoomNameResponse> serviceAndAmenityRoomNameResponses =
                serviceAndAmenitySqlNatives.stream()
                        .map(item -> new ServiceAndAmenityRoomNameResponse(item.getName()))
                        .toList();

        List<HotelSearchRoomRecordResponse> hotelSearchRoomRecordResponses =
                hotel.getRooms()
                        .stream()
                        .map(item -> {
                            BigDecimal priceRoom = item.getPricePerNight().multiply(BigDecimal.valueOf(totalDay));
                            return new HotelSearchRoomRecordResponse(
                                    item.getId(),
                                    item.getRoomName(),
                                    item.getRoomNameCustom(),
                                    item.getBedName(),
                                    item.getMaxOccupancy(),
                                    item.getQuantityRoom(),
                                    priceRoom,
                                    getTotalMoneyOriginal(promotion, priceRoom),
                                    roomRepo.findAllNameServiceAndAmenityByRoomId(item.getId()).orElse(null));
                        }).toList();

        List<HotelSearchImageResponse> imageResponses =
                hotel.getImages()
                        .stream()
                        .map(image -> new HotelSearchImageResponse(image.getUrlImage()))
                        .toList();

        return HotelSearchIdResultResponse.builder()
                .adults(request.getAdults())
                .children(request.getChildren())
                .checkInDate(request.getCheckInDate())
                .checkOutDate(request.getCheckOutDate())
                .quantityRoom(request.getQuantityRoom())
                .hotelId(result.hotelId())
                .name(result.name())
                .rating(result.rating())
                .streetAddress(result.streetAddress())
                .districtAddress(result.districtAddress())
                .description(result.description())
                .city(result.city())
                .country(result.country())
                .totalDay(totalDay)
                .countView(result.countView())
                .discountPercent(promotion == null ? 0 : promotion.getDiscountPercent())
                .serviceAndAmenityRoomNameResponses(serviceAndAmenityRoomNameResponses)
                .rooms(hotelSearchRoomRecordResponses)
                .reviewSyntheticResponse(reviewService.findReviewsByHotel(result.hotelId(), 1, 8))
                .images(imageResponses)
                .build();
    }

    private Promotion getPromotion(UUID hotelId) {
        List<Promotion> promotions = promotionRepo.findPromotionByStatusAndHotelId(hotelId);
        if (promotions.isEmpty()) {
            return null;
        }
        return promotions.get(0);
    }

    private BigDecimal getTotalMoneyOriginal(Promotion promotion, BigDecimal totalMoneyOriginal) {
        return promotion == null
                ? totalMoneyOriginal
                : promotion.getEndDate().isAfter(LocalDate.now())
                ? totalMoneyOriginal.subtract(totalMoneyOriginal.multiply(BigDecimal.valueOf(promotion.getDiscountPercent() / 100)))
                : totalMoneyOriginal;
    }
}
