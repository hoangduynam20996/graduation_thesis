package com.example.graduationthesis.hotel.controller.publicapi.specification;

import com.example.graduationthesis.constant.HotelConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotel.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + HotelConstant.API_HOTEL)
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Hotel Specification Learning")
public class HotelSpecController {

    private final HotelService hotelService;

    @Operation(summary = "Get hotels by condition", description = "Get hotels by sort")
    @GetMapping("/condition")
    public ResponseEntity<?> findHotelByCondition(@RequestParam("currentPage") Integer currentPage,
                                                  @RequestParam("limitPage") Integer limitPage,
                                                  @RequestParam("sortBy") String sortBy) {
        log.info("Get hotels by condition {}", sortBy);
        return new ResponseEntity<>(hotelService.findHotelsByCondition(currentPage, limitPage, sortBy), HttpStatus.OK);
    }

    @Operation(summary = "Get hotels by condition", description = "Get hotels by sorts")
    @GetMapping("/conditions")
    public ResponseEntity<?> findHotelByCondition(@RequestParam("currentPage") Integer currentPage,
                                                  @RequestParam("limitPage") Integer limitPage,
                                                  @RequestParam("sortBys") String... sortBys) {
        log.info("Get hotels by conditions {}", (Object) sortBys);
        return new ResponseEntity<>(hotelService.findHotelsByCondition(currentPage, limitPage, sortBys), HttpStatus.OK);
    }


    @Operation(summary = "Get hotels by condition", description = "Get hotels by sort and search")
    @GetMapping("/conditions-search")
    public ResponseEntity<?> findHotelByCondition(
            @RequestParam(value = "currentPage", defaultValue = "1", required = false) Integer currentPage,
            @RequestParam(value = "limitPage", defaultValue = "10", required = false) Integer limitPage,
            @RequestParam(value = "sortBy", required = false) String sortBy,
            @RequestParam(value = "roomName", required = false) String roomName,
            @RequestParam(value = "search", required = false) String... search
    ) {
        log.info("Get hotels by conditions {} {}", sortBy, search);
        return new ResponseEntity<>(hotelService.findHotelsByCondition(currentPage, limitPage, sortBy, roomName, search), HttpStatus.OK);
    }

    @Operation(summary = "Get hotels by condition hotel and room", description = "Get hotels by sort and search")
    @GetMapping("/conditions-search-hotel")
    public ResponseEntity<?> findHotelByCondition(
            Pageable pageable,
            @RequestParam(value = "hotels", required = false) String[] hotels,
            @RequestParam(value = "rooms", required = false) String[] rooms
    ) {
        log.info("Get hotels by condition {} {}", hotels, rooms);
        return new ResponseEntity<>(hotelService.findHotelsByCondition(pageable, hotels, rooms), HttpStatus.OK);
    }
}
