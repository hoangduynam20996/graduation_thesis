package com.example.graduationthesis.hotel.controller.publicapi;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotel.dto.request.HotelValidateEnumRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE)
public class HotelValidateEnumController {

    @PostMapping("/validate")
    public ResponseEntity<?> validateEnum(@RequestBody HotelValidateEnumRequest hotelValidateEnumRequest) {
        return new ResponseEntity<>("Add Success", HttpStatus.OK);
    }
}
