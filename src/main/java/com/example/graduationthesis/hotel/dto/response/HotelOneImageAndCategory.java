package com.example.graduationthesis.hotel.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class HotelOneImageAndCategory {
    private UUID id;

    private String name;

    private String description;

    private Double rating;

    private String contactPerson;

    private String phoneNumber;

    private String phoneNumberTwo;

    private String streetAddress;

    private String districtAddress;

    private String country;

    private String city;

    private String status;

    private String postalCode;

    private Integer countView;

    private String image;

    private Object categories;
}
