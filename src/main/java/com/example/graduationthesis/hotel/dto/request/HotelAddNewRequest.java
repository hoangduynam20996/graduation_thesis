package com.example.graduationthesis.hotel.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelAddNewRequest {

    @NotNull
    private UUID hotelTypeId;
    @NotBlank
    private String name;
    @NotNull
    private Double rating;
    @NotBlank
    private String contactPerson;
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String phoneNumberTwo;
    @NotBlank
    private String streetAddress;
    @NotBlank
    private String districtAddress;
    @NotBlank
    private String country;
    @NotBlank
    private String city;
    @NotBlank
    private String postalCode;
    @NotNull
    private Integer quantityHotel;
}
