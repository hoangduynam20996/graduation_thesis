package com.example.graduationthesis.hotel.dto.response;

import com.example.graduationthesis.hotel.dto.request.HotelSearchImageResponse;
import com.example.graduationthesis.review.dto.response.ReviewSyntheticResponse;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAndAmenityRoomNameResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class HotelSearchIdResultResponse extends HotelSearchResultBase {

    private List<ServiceAndAmenityRoomNameResponse> serviceAndAmenityRoomNameResponses;

    private List<HotelSearchRoomRecordResponse> rooms;

    private ReviewSyntheticResponse reviewSyntheticResponse;

    private List<HotelSearchImageResponse> images;
}
