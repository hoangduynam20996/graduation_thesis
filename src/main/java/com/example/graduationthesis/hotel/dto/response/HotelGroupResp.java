package com.example.graduationthesis.hotel.dto.response;

public record HotelGroupResp(
        String name,
        Long quantityRoom
) {
}
