package com.example.graduationthesis.hotel.dto.request;

import com.example.graduationthesis.hotel.enums.HotelStatusEnum;
import com.example.graduationthesis.hotel.enums.HotelTypeEnum;
import com.example.graduationthesis.utils.validation.EnumPattern;
import com.example.graduationthesis.utils.validation.EnumValue;
import com.example.graduationthesis.utils.validation.Gender;
import com.example.graduationthesis.utils.validation.GenderSubset;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelValidateEnumRequest {

    @EnumPattern(name = "status", regexp = "ACTIVE|NO_ACTIVE|INACTIVE")
    private HotelStatusEnum status;

    @NotNull
    @EnumValue(name = "hotelType", enumClass = HotelTypeEnum.class)
    private String hotelType;

    @GenderSubset(anyOf = {Gender.MALE, Gender.FEMALE})
    private Gender gender;

}
