package com.example.graduationthesis.hotel.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelUpdateByOwnerRequest {

    private UUID hotelId;

    private UUID hotelTypeId;

    private String name;

    private Double rating;

    private String contactPersonName;

    private String phoneNumber;

    private String phoneNumberTwo;

    private String streetAddress;

    private String districtAddress;

    private String city;

    private String country;

    private String postalCode;

    private String description;

}
