package com.example.graduationthesis.hotel.repo.specification;

import lombok.Getter;

@Getter
public class SpecSearchCriteria {

    private String key;

    private SearchOperation operation;

    private Object value;

    private Boolean orPredicate;

    public SpecSearchCriteria(String key, SearchOperation operation, Object value) {
        super();
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public SpecSearchCriteria(String orPredicate, String key, SearchOperation operation, Object value) {
        super();
        this.orPredicate = orPredicate != null && orPredicate.equals(SearchOperation.OR_PREDICATE_FLAG);
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public SpecSearchCriteria(String key, String operation, String value, String prefix, String suffix) {
        SearchOperation op = SearchOperation.getSimpleOperation(operation.charAt(0));
        if (op == SearchOperation.EQUALITY) {
            boolean startWithAsterisk = prefix != null && prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX);
            boolean endWithAsterisk = prefix != null && suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX);

            if (startWithAsterisk && endWithAsterisk) {
                op = SearchOperation.CONTAINS;
            }

            if (startWithAsterisk) {
                op = SearchOperation.ENDS_WITH;
            }

            if (endWithAsterisk) {
                op = SearchOperation.STARTS_WITH;
            }
        }
        this.key = key;
        this.operation = op;
        this.value = value;
    }
}
