package com.example.graduationthesis.hotel.repo.specification;

import com.example.graduationthesis.hotel.entity.Hotel;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class HotelSpecBuilder {

    private final List<SpecSearchCriteria> params;

    public HotelSpecBuilder() {
        this.params = new ArrayList<>();
    }

    public HotelSpecBuilder with(String key, String operation, Object value, String prefix, String suffix) {
        return with(null, key, operation, value, prefix, suffix);
    }

    public HotelSpecBuilder with(String orPredicate, String key, String operation, Object value, String prefix, String suffix) {
        SearchOperation op = SearchOperation.getSimpleOperation(operation.charAt(0));

        if (op == SearchOperation.EQUALITY) {
            boolean startWithAsterisk = prefix != null && prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX);
            boolean endWithAsterisk = suffix != null && suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX);

            if (startWithAsterisk && endWithAsterisk) {
                op = SearchOperation.CONTAINS;
            }

            if (startWithAsterisk) {
                op = SearchOperation.ENDS_WITH;
            }

            if (endWithAsterisk) {
                op = SearchOperation.STARTS_WITH;
            }
            params.add(new SpecSearchCriteria(orPredicate, key, op, value));
        }
        return this;
    }

    public Specification<Hotel> build() {
        if (params.isEmpty()) return null;

        Specification<Hotel> specification = new HotelSpec(params.get(0));

        for (int i = 1; i < params.size(); i++) {
            specification = params.get(i).getOrPredicate()
                    ? Specification.where(specification).or(new HotelSpec(params.get(i)))
                    : Specification.where(specification).and(new HotelSpec(params.get(i)));
        }

        return specification;
    }
}
