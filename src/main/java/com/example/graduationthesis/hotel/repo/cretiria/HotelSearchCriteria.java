package com.example.graduationthesis.hotel.repo.cretiria;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HotelSearchCriteria {

    private String key;

    private String operation; // :, >, <

    private Object value;
}
