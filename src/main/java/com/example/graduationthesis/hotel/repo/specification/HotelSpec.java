package com.example.graduationthesis.hotel.repo.specification;

import com.example.graduationthesis.hotel.entity.Hotel;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@AllArgsConstructor
public class HotelSpec implements Specification<Hotel> {

    private SpecSearchCriteria specSearchCriteria;

    @Override
    public Predicate toPredicate(Root<Hotel> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return switch (specSearchCriteria.getOperation()) {
            case EQUALITY ->
                    criteriaBuilder.equal(root.get(specSearchCriteria.getKey()), specSearchCriteria.getValue());
            case NEGATION ->
                    criteriaBuilder.notEqual(root.get(specSearchCriteria.getKey()), specSearchCriteria.getValue());
            case GREATER_THAN ->
                    criteriaBuilder.greaterThan(root.get(specSearchCriteria.getKey()), specSearchCriteria.getValue().toString());
            case LESS_THAN ->
                    criteriaBuilder.lessThan(root.get(specSearchCriteria.getKey()), specSearchCriteria.getValue().toString());
            case GREATER_THAN_EQUAL -> null;
            case LESS_THAN_EQUAL -> null;
            case NOT_EQUAL -> null;
            case STARTS_WITH ->
                    criteriaBuilder.like(root.get(specSearchCriteria.getKey()), specSearchCriteria.getValue() + "%");
            case ENDS_WITH ->
                    criteriaBuilder.like(root.get(specSearchCriteria.getKey()), "%" + specSearchCriteria.getValue());
            case CONTAINS -> null;
            case NOT_CONTAINS -> null;
            case LIKE ->
                    criteriaBuilder.like(root.get(specSearchCriteria.getKey()), String.format("%%%s%%", specSearchCriteria.getValue()));
        };
    }
}
