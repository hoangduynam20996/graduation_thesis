package com.example.graduationthesis.hotel.repo.cretiria;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.specification.SpecSearchCriteria;
import com.example.graduationthesis.room.entity.Room;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
@RequiredArgsConstructor
public class HotelCriteriaRepo {

    private final EntityManager entityManager;

    public List<Hotel> searchHotels(int currentPage, int limitPage, String sortBy, String roomName, String... search) {
        List<HotelSearchCriteria> searchCriteriaList = new ArrayList<>();

        // Xử lí các điều kiện search
        if (search != null) {
            for (String s : search) {
                Pattern pattern = Pattern.compile("(\\w+?)([:><])(.*)");
                Matcher matcher = pattern.matcher(s);
                if (matcher.find()) {
                    searchCriteriaList.add(new HotelSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
                }
            }
        }

        return getHotels(currentPage, limitPage, sortBy, roomName, searchCriteriaList);
    }

    private List<Hotel> getHotels(int currentPage, int limitPage, String sortBy, String roomName, List<HotelSearchCriteria> searchCriteriaList) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Hotel> criteriaQuery = criteriaBuilder.createQuery(Hotel.class);
        Root<Hotel> root = criteriaQuery.from(Hotel.class);

        // Xử lí các điều kiện search
        Predicate predicate = criteriaBuilder.conjunction();
        HotelSearchCriteriaConsumer hotelSearchCriteriaConsumer = new HotelSearchCriteriaConsumer(criteriaBuilder, predicate, root);
        searchCriteriaList.forEach(hotelSearchCriteriaConsumer);
        predicate = hotelSearchCriteriaConsumer.getPredicate();

        if (StringUtils.hasLength(roomName)) {
            Join<Hotel, Room> roomJoin = root.join("rooms");
            Predicate roomPredicate = criteriaBuilder.like(roomJoin.get("roomName"), String.format("%%%s%%", roomName));
            criteriaQuery.where(predicate, roomPredicate);
        } else {
            criteriaQuery.where(predicate);
        }

        if (StringUtils.hasLength(sortBy)) {
            Pattern pattern = Pattern.compile("(\\w+?)(:)(asc|desc)");
            Matcher matcher = pattern.matcher(sortBy);
            if (matcher.find()) {
                String columnName = matcher.group(1);
                if (matcher.group(3).equalsIgnoreCase("asc")) {
                    criteriaQuery.orderBy(criteriaBuilder.asc(root.get(columnName)));
                } else {
                    criteriaQuery.orderBy(criteriaBuilder.desc(root.get(columnName)));
                }
            }
        }

        return entityManager.createQuery(criteriaQuery).setFirstResult((currentPage - 1) * limitPage).setMaxResults(limitPage).getResultList();
    }

    private Long getTotalElement(List<HotelSearchCriteria> searchCriteriaList, String roomName) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Hotel> root = criteriaQuery.from(Hotel.class);

        Predicate predicate = criteriaBuilder.conjunction();
        HotelSearchCriteriaConsumer hotelSearchCriteriaConsumer = new HotelSearchCriteriaConsumer(criteriaBuilder, predicate, root);
        searchCriteriaList.forEach(hotelSearchCriteriaConsumer);

        if (StringUtils.hasLength(roomName)) {
            Join<Hotel, Room> roomJoin = root.join("rooms");
            Predicate roomPredicate = criteriaBuilder.like(roomJoin.get("roomName"), String.format("%%%s%%", roomName));
            criteriaQuery.select(criteriaBuilder.count(root));
            criteriaQuery.where(predicate, roomPredicate);
        } else {
            predicate = hotelSearchCriteriaConsumer.getPredicate();
            criteriaQuery.select(criteriaBuilder.count(root));
            criteriaQuery.where(predicate);
        }

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public Page<Hotel> getHotelsJoinRoom(Pageable pageable, String[] searchHotels, String[] rooms) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Hotel> criteriaQuery = criteriaBuilder.createQuery(Hotel.class);
        Root<Hotel> root = criteriaQuery.from(Hotel.class);

        Join<Hotel, Room> roomJoin = root.join("rooms", JoinType.INNER);
        List<Predicate> hotelPredicates = new ArrayList<>();
        List<Predicate> roomPredicates = new ArrayList<>();

        Pattern pattern = Pattern.compile("(\\w+?)([:<>~!])(.*)(\\p{Punct}?)(.*)(\\p{Punct}?)");
        for (String room : searchHotels) {
            Matcher matcher = pattern.matcher(room);
            if (matcher.find()) {
                SpecSearchCriteria criteria = new SpecSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5));
                Predicate predicate = toPredicate(root, criteriaBuilder, criteria);
                hotelPredicates.add(predicate);
            }
        }

        for (String room : rooms) {
            Matcher matcher = pattern.matcher(room);
            if (matcher.find()) {
                SpecSearchCriteria criteria = new SpecSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5));
                Predicate predicate = toPredicate(roomJoin, criteriaBuilder, criteria);
                roomPredicates.add(predicate);
            }
        }

        Predicate hotelPredicate = criteriaBuilder.or(hotelPredicates.toArray(new Predicate[0]));
        Predicate roomPredicate = criteriaBuilder.or(roomPredicates.toArray(new Predicate[0]));
        Predicate filterPredicate = criteriaBuilder.and(hotelPredicate, roomPredicate);
        criteriaQuery.where(filterPredicate);

        List<Hotel> resultList = entityManager.createQuery(criteriaQuery).setFirstResult(pageable.getPageNumber()).setMaxResults(pageable.getPageSize()).getResultList();
        long totalElement = getTotalElement(searchHotels, rooms);
        System.out.println(totalElement);

        return new PageImpl<>(resultList, pageable, totalElement);
    }

    public long getTotalElement(String[] searchHotels, String[] rooms) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Hotel> root = criteriaQuery.from(Hotel.class);
        Join<Hotel, Room> roomJoin = root.join("rooms", JoinType.INNER);

        List<Predicate> hotelPredicates = new ArrayList<>();
        List<Predicate> roomPredicates = new ArrayList<>();

        Pattern pattern = Pattern.compile("(\\w+?)([:<>~!])(.*)(\\p{Punct}?)(.*)(\\p{Punct}?)");
        for (String room : searchHotels) {
            Matcher matcher = pattern.matcher(room);
            if (matcher.find()) {
                SpecSearchCriteria criteria = new SpecSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5));
                Predicate predicate = toPredicate(root, criteriaBuilder, criteria);
                hotelPredicates.add(predicate);
            }
        }

        for (String room : rooms) {
            Matcher matcher = pattern.matcher(room);
            if (matcher.find()) {
                SpecSearchCriteria criteria = new SpecSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5));
                Predicate predicate = toPredicate(roomJoin, criteriaBuilder, criteria);
                roomPredicates.add(predicate);
            }
        }

        Predicate hotelPredicate = criteriaBuilder.or(hotelPredicates.toArray(new Predicate[0]));
        Predicate roomPredicate = criteriaBuilder.or(roomPredicates.toArray(new Predicate[0]));
        Predicate filterPredicate = criteriaBuilder.and(hotelPredicate, roomPredicate);

        criteriaQuery.select(criteriaBuilder.count(root));
        criteriaQuery.where(filterPredicate);

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public Predicate toPredicate(Root<Hotel> root, CriteriaBuilder criteriaBuilder, SpecSearchCriteria criteria) {
        return switch (criteria.getOperation()) {
            case EQUALITY -> criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
            case NEGATION -> criteriaBuilder.notEqual(root.get(criteria.getKey()), criteria.getValue());
            case GREATER_THAN ->
                    criteriaBuilder.greaterThan(root.get(criteria.getKey()), criteria.getValue().toString());
            case LESS_THAN -> criteriaBuilder.lessThan(root.get(criteria.getKey()), criteria.getValue().toString());
            case GREATER_THAN_EQUAL -> null;
            case LESS_THAN_EQUAL -> null;
            case NOT_EQUAL -> null;
            case STARTS_WITH -> criteriaBuilder.like(root.get(criteria.getKey()), criteria.getValue() + "%");
            case ENDS_WITH -> criteriaBuilder.like(root.get(criteria.getKey()), "%" + criteria.getValue());
            case CONTAINS -> null;
            case NOT_CONTAINS -> null;
            case LIKE ->
                    criteriaBuilder.like(root.get(criteria.getKey()), String.format("%%%s%%", criteria.getValue()));
        };
    }

    public Predicate toPredicate(Join root, CriteriaBuilder criteriaBuilder, SpecSearchCriteria criteria) {
        return switch (criteria.getOperation()) {
            case EQUALITY -> criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
            case NEGATION -> criteriaBuilder.notEqual(root.get(criteria.getKey()), criteria.getValue());
            case GREATER_THAN ->
                    criteriaBuilder.greaterThan(root.get(criteria.getKey()), criteria.getValue().toString());
            case LESS_THAN -> criteriaBuilder.lessThan(root.get(criteria.getKey()), criteria.getValue().toString());
            case GREATER_THAN_EQUAL -> null;
            case LESS_THAN_EQUAL -> null;
            case NOT_EQUAL -> null;
            case STARTS_WITH -> criteriaBuilder.like(root.get(criteria.getKey()), criteria.getValue() + "%");
            case ENDS_WITH -> criteriaBuilder.like(root.get(criteria.getKey()), "%" + criteria.getValue());
            case CONTAINS -> null;
            case NOT_CONTAINS -> null;
            case LIKE ->
                    criteriaBuilder.like(root.get(criteria.getKey()), String.format("%%%s%%", criteria.getValue()));
        };
    }
}
