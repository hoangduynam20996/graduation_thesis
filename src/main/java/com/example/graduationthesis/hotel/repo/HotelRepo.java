package com.example.graduationthesis.hotel.repo;

import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.response.HotelDTOResponse;
import com.example.graduationthesis.hotel.dto.response.HotelGroupResp;
import com.example.graduationthesis.hotel.entity.Hotel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HotelRepo extends JpaRepository<Hotel, UUID>, HotelRepoCustom, JpaSpecificationExecutor<Hotel> {

    Optional<Hotel> findHotelByStatusAndId(String status, UUID hotelId);

    @Query("""
                SELECT
                    new com.example.graduationthesis.hotel.dto.response.HotelGroupResp(
                        h.postalCode,
                        SUM(r.quantityRoom)
                    )
                FROM
                    Hotel h
                INNER JOIN
                    Room r ON r.hotel.id = h.id
                WHERE
                    h.status = 'ACTIVE'
                AND
                    r.status = 'ACTIVE'
                GROUP BY
                   h.postalCode
            """)
    Optional<List<HotelGroupResp>> findRoomsGroupByPostalCode();

    @Query("""
                SELECT
                    new com.example.graduationthesis.hotel.dto.response.HotelGroupResp(
                        h.city,
                        SUM(r.quantityRoom)
                    )
                FROM
                    Hotel h
                INNER JOIN
                    Room r ON r.hotel.id = h.id
                WHERE
                    h.status = 'ACTIVE'
                AND
                    r.status = 'ACTIVE'
                GROUP BY
                   h.city
            """)
    Optional<List<HotelGroupResp>> findRoomsGroupByCity();

    @Query(value = """
            SELECT
                DISTINCT
                new com.example.graduationthesis.hotel.dto.response.HotelDTOResponse(
                    h.id,
                    h.name,
                    h.rating,
                    h.streetAddress,
                    h.districtAddress,
                    h.city,
                    h.country,
                    h.countView,
                    r.pricePerNight,
                    h.description
                )
            FROM
                Hotel h
            LEFT JOIN
                Room r ON r.hotel.id = h.id
            LEFT JOIN
                ServiceAndAmenityRoom sar ON sar.room.id = r.id
            LEFT JOIN
                BookingDetail bkd ON bkd.room.id = r.id
            WHERE
                h.status = 'ACTIVE' AND r.status = 'ACTIVE'
            AND
                (:#{#request.city} IS NULL OR h.city ILIKE CONCAT('%', :#{#request.city}, '%') )
            AND
                (:#{#request.country} IS NULL OR h.country ILIKE CONCAT('%', :#{#request.country}, '%') )
            AND
                r.quantityRoom >= :#{#request.quantityRoom}
            AND
                r.maxOccupancy >= :#{#request.adults + #request.children}
            AND
                (bkd.checkOutDate <= :#{#request.checkInDate} OR r.saleDate <= :#{#request.checkInDate})
            """)
    Page<HotelDTOResponse> findHotelsByCondition(HotelSearchRequest request, Pageable pageable);

    @Query(value = """
             SELECT
                DISTINCT
                new com.example.graduationthesis.hotel.dto.response.HotelDTOResponse(
                    h.id,
                    h.name,
                    h.rating,
                    h.streetAddress,
                    h.districtAddress,
                    h.city,
                    h.country,
                    h.countView,
                    r.pricePerNight,
                    h.description
                )
            FROM
                Hotel h
            LEFT JOIN
                Room r ON r.hotel.id = h.id
            LEFT JOIN
                ServiceAndAmenityRoom sar ON sar.room.id = r.id
            LEFT JOIN
                BookingDetail bkd ON bkd.room.id = r.id
            WHERE
                h.status = 'ACTIVE' AND r.status = 'ACTIVE'
            AND
                (:#{#request.city} IS NULL OR h.city ILIKE CONCAT('%', :#{#request.city}, '%') )
            AND
                (:#{#request.country} IS NULL OR h.country ILIKE CONCAT('%', :#{#request.country}, '%') )
            AND
                r.quantityRoom >= :#{#request.quantityRoom}
            AND
                r.maxOccupancy >= :#{#request.adults + #request.children}
            AND
                (bkd.checkOutDate <= :#{#request.checkInDate} OR r.saleDate <= :#{#request.checkInDate})
             AND
                (:#{#request.hotelId} IS NULL OR h.id = :#{#request.hotelId})
            """)
    HotelDTOResponse findHotelByConditionAndId(@Param("request") HotelSearchRequest request);

    @Query("""
            SELECT
                o
            FROM
                Hotel  o
            WHERE
                o.status = ?1
            ORDER BY
                o.createdDate DESC
            """)
    Page<Hotel> findHotelsByStatusAndOrderByCreatedDateDesc(String status, Pageable pageable);

}




