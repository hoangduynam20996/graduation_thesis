package com.example.graduationthesis.hotel.repo.specification;

import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.response.HotelDTOResponse;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.room.entity.Room;
import com.ibm.icu.text.Normalizer;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.metamodel.SingularAttribute;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HotelSpecification {

    public static volatile SingularAttribute<Hotel, Long> id;

    public static Specification<HotelDTOResponse> hotelSpecification(HotelSearchRequest request) {
        return (root, query, criteriaBuilder) -> {
            Join<Hotel, Room> joinRoom = root.join("rooms", JoinType.LEFT);
            Join<Room, BookingDetail> joinRoomDetail = joinRoom.join("bookingDetails", JoinType.LEFT);

            List<Predicate> predicates = new ArrayList<>();
            // Adding predicates based on the number of adults and children
            Integer adults = request.getAdults();
            Integer children = request.getChildren();

            predicates.add(criteriaBuilder.equal(root.get("status"), "ACTIVE"));
            predicates.add(criteriaBuilder.equal(joinRoom.get("status"), "ACTIVE"));
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(joinRoom.get("quantity"), request.getQuantityRoom()));
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(joinRoom.get("maxOccupancy"), adults + children));
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.lessThanOrEqualTo(joinRoomDetail.get("checkOutDate"), request.getCheckInDate()),
                    criteriaBuilder.lessThanOrEqualTo(joinRoom.get("saleDate"), request.getCheckOutDate())
            ));

            if (request.getCountry() != null && !request.getCountry().trim().isEmpty()) {
                predicates.add(criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("country")),
                        "%" + request.getCountry().toLowerCase() + "%")
                );
            }

            if (request.getHotelId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("hotelId"), request.getHotelId()));
            }

            // Add condition for city
            if (request.getCity() != null && !request.getCity().trim().isEmpty()) {
                String city = removeAccents(request.getCity()).toLowerCase();
                String[] parts = city.split("\\s+");

                List<Predicate> cityPredicates = Arrays.stream(parts)
                        .map(part -> criteriaBuilder.like(criteriaBuilder.lower(root.get("city")), "%" + part + "%"))
                        .toList();

                if (!cityPredicates.isEmpty()) {
                    predicates.add(criteriaBuilder.or(cityPredicates.toArray(new Predicate[0])));
                }
            }

            query.multiselect(
                    root.get("id"),
                    root.get("name"),
                    root.get("rating"),
                    root.get("streetAddress"),
                    root.get("districtAddress"),
                    root.get("city"),
                    root.get("country"),
                    root.get("countView"),
                    joinRoom.get("priceRoom"),
                    root.get("description")
            );

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    public static String removeAccents(String source) {
        // Chuẩn hóa chuỗi với NFD
        String normalized = Normalizer.normalize(source, Normalizer.NFD);
        // Xóa các ký tự điều khiển và dấu, chỉ giữ lại các ký tự đơn
        return normalized.replaceAll("\\p{M}", "");
    }
}
