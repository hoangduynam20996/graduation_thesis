package com.example.graduationthesis.hotel.repo.specification;

public enum SearchOperation {

    EQUALITY,
    NEGATION,
    GREATER_THAN,
    LESS_THAN,
    GREATER_THAN_EQUAL,
    LESS_THAN_EQUAL,
    NOT_EQUAL,
    STARTS_WITH,
    ENDS_WITH,
    CONTAINS,
    NOT_CONTAINS,
    LIKE;

    public static final String[] SIMPLE_OPERATIONS = {":", "!", ">", "<", ">=", "<=", "!=", "~"};

    public static final String OR_PREDICATE_FLAG = "'";

    public static final String ZERO_OR_MORE_REGEX = "*";

    public static final String OR_OPERATOR = "OR";

    public static final String AND_OPERATOR = "AND";

    public static final String LEFT_PARENTHESIS = "(";

    public static final String RIGHT_PARENTHESIS = ")";

    public static SearchOperation getSimpleOperation(final char input) {
        return switch (input) {
            case ':' -> SearchOperation.EQUALITY;
            case '!' -> SearchOperation.NEGATION;
            case '>' -> SearchOperation.GREATER_THAN;
            case '<' -> SearchOperation.LESS_THAN;
            case '~' -> SearchOperation.LIKE;
            default -> null;
        };
    }

}
