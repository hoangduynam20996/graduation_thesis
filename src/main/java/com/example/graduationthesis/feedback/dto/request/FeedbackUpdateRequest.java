package com.example.graduationthesis.feedback.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class FeedbackUpdateRequest {

    @NotNull
    private UUID feedbackId;
    @NotBlank
    private String feedbackContent;
}
