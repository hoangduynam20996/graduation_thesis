package com.example.graduationthesis.feedback.dto;

import java.util.Date;
import java.util.UUID;

public record FeedbackDTO(
        UUID feedbackId,
        UUID reviewId,
        Date feedbackDate,
        String feedbackContent
) {
}
