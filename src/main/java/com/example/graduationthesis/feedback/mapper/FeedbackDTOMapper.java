package com.example.graduationthesis.feedback.mapper;

import com.example.graduationthesis.feedback.dto.FeedbackDTO;
import com.example.graduationthesis.feedback.entity.Feedback;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class FeedbackDTOMapper implements Function<Feedback, FeedbackDTO> {
    @Override
    public FeedbackDTO apply(Feedback feedback) {
        return new FeedbackDTO(
                feedback.getId(),
                feedback.getId(),
                feedback.getFeedbackDate(),
                feedback.getFeedbackContent()
        );
    }
}
