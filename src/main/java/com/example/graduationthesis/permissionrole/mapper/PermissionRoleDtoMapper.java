package com.example.graduationthesis.permissionrole.mapper;

import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleDto;
import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class PermissionRoleDtoMapper implements Function<PermissionRole, PermissionRoleDto> {

    @Override
    public PermissionRoleDto apply(PermissionRole permissionRole) {
        return PermissionRoleDto.builder()
                .permissionRoleId(permissionRole.getId())
                .permissionId(permissionRole.getPermission().getId())
                .permissionCode(permissionRole.getPermission().getPermissionCode())
                .roleId(permissionRole.getRole().getId())
                .roleName(permissionRole.getRole().getRoleName())
                .isDeleted(permissionRole.getIsDeleted())
                .build();
    }
}
