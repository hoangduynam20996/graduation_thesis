package com.example.graduationthesis.permissionrole.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class PermissionRoleAddRequest {

    private UUID permissionId;

    private UUID roleId;
}
