package com.example.graduationthesis.permissionrole.service.impl;


import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.permission.repo.PermissionRepo;
import com.example.graduationthesis.permissionrole.dto.request.PermissionRoleAddRequest;
import com.example.graduationthesis.permissionrole.dto.request.PermissionRoleUpdateRequest;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleDto;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleResponse;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleResponses;
import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import com.example.graduationthesis.permissionrole.mapper.PermissionRoleDtoMapper;
import com.example.graduationthesis.permissionrole.repo.PermissionRoleRepo;
import com.example.graduationthesis.permissionrole.service.PermissionRoleService;
import com.example.graduationthesis.role.entity.Role;
import com.example.graduationthesis.role.repo.RoleRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class PermissionRoleServiceImpl implements PermissionRoleService {

    private final PermissionRoleRepo permissionRoleRepo;

    private final PermissionRepo permissionRepo;

    private final RoleRepo roleRepo;

    private final PermissionRoleDtoMapper permissionRoleDtoMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    private Permission getPermission(UUID permissionId) {
        return permissionRepo.findById(permissionId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.PMS_002, getMessageBundle(ResourceConstant.PMS_002)));
    }

    private Role getRole(UUID roleId) {
        return roleRepo.findById(roleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.RL_002, getMessageBundle(ResourceConstant.RL_002)));
    }

    @Override
    public PermissionRoleResponse addPermissionRole(PermissionRoleAddRequest request) {
        Role role = getRole(request.getRoleId());

        Permission permission = getPermission(request.getPermissionId());

        PermissionRole permissionRole = permissionRoleRepo.save(
                PermissionRole.builder()
                        .permission(permission)
                        .role(role)
                        .build()
        );

        return PermissionRoleResponse.builder()
                .code(ResourceConstant.PMS_R_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionRoleDtoMapper.apply(permissionRole))
                .message(getMessageBundle(ResourceConstant.PMS_R_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PermissionRoleResponse updatePermissionRole(PermissionRoleUpdateRequest request) {
        PermissionRole permissionRole = permissionRoleRepo.findById(request.getPermissionRoleId())
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.PMS_R_001, getMessageBundle(ResourceConstant.PMS_R_001)));
        Role role = getRole(request.getRoleId());
        Permission permission = getPermission(request.getPermissionId());

        permissionRole.setPermission(permission);
        permissionRole.setRole(role);

        PermissionRole permissionRoleSave = permissionRoleRepo.save(permissionRole);
        return PermissionRoleResponse.builder()
                .code(ResourceConstant.PMS_R_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionRoleDtoMapper.apply(permissionRoleSave))
                .message(getMessageBundle(ResourceConstant.PMS_R_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PermissionRoleResponse deletePermissionRole(UUID permissionRoleId) {
        PermissionRole permissionRole = permissionRoleRepo.findById(permissionRoleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.PMS_R_001, getMessageBundle(ResourceConstant.PMS_R_001)));
        permissionRole.setIsDeleted(SystemConstant.NO_ACTIVE);
        PermissionRole permissionRoleSave = permissionRoleRepo.save(permissionRole);

        return PermissionRoleResponse.builder()
                .code(ResourceConstant.PMS_R_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionRoleDtoMapper.apply(permissionRoleSave))
                .message(getMessageBundle(ResourceConstant.PMS_R_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PermissionRoleResponses findAllPermissionRole(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<PermissionRole> all = permissionRoleRepo.findAll(pageable);
        List<PermissionRoleDto> permissionRoleDtos = all.stream()
                .map(permissionRoleDtoMapper)
                .toList();

        return PermissionRoleResponses.builder()
                .code(ResourceConstant.PMS_R_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionRoleDtos)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message(getMessageBundle(ResourceConstant.PMS_R_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }
}
