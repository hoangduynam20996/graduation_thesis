package com.example.graduationthesis.permissionrole.repo;

import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface PermissionRoleRepo extends JpaRepository<PermissionRole, UUID> {

    @Query("""
               SELECT
                o
              FROM
                PermissionRole o
              INNER JOIN
                Role r ON r.id = o.role.id
              WHERE
                r.id = ?1
            """)
    List<PermissionRole> findPermissionRolesByRoleId(UUID roleId);

    @Modifying
    @Query("""
            DELETE FROM PermissionRole o WHERE o.permission.id = ?1
                """)
    void deletePermissionRoleByPermissionId(UUID permissionId);

    @Modifying(clearAutomatically = true)
    @Query("""
            DELETE FROM PermissionRole o WHERE o.role.id = ?1
                """)
    void deletePermissionRoleByRoleId(UUID roleId);

    @Query("""
            SELECT
                pmr
            FROM
                PermissionRole pmr
            INNER JOIN
                Role rl ON rl.id = pmr.role.id
            INNER JOIN
                UserRole ur ON ur.role.id = rl.id
            INNER JOIN
                User u ON u.id = ur.user.id
            WHERE
                u.id = ?1
                 """)
    List<PermissionRole> findPermissionRolesByUserId(UUID userId);

    @Query("""
            SELECT
                o
            FROM
                PermissionRole o
            INNER JOIN
                Permission pm ON pm.id = o.permission.id
            INNER JOIN
                PermissionGroup pmg ON pmg.permission.id = pm.id
            INNER JOIN
                Group g ON g.id = pmg.group.id
            WHERE
                g.id = ?1
            """)
    List<PermissionRole> findPermissionRolesByGroupId(UUID groupId);
}
