package com.example.graduationthesis.review.repo;

import com.example.graduationthesis.review.dto.response.ReviewHotelIndexResponse;
import com.example.graduationthesis.review.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ReviewRepo extends JpaRepository<Review, UUID> {

    long countReviewByHotelId(UUID hotelId);

    Optional<List<Review>> findReviewsByHotelId(UUID hotelId, Pageable pageable);

    Optional<Review> findReviewByUser_IdAndStatusAndId(UUID userId, String status,UUID reviewId);
    @Query("""
                SELECT
                    new com.example.graduationthesis.review.dto.response.ReviewHotelIndexResponse(
                        h.id,
                        h.name,
                        h.country,
                        h.streetAddress,
                        h.districtAddress,
                        h.city,
                        COUNT(r.hotel)
                    )
                FROM
                    Review r
                INNER JOIN
                    Hotel h ON h.id = r.hotel.id
                WHERE
                    h.status = 'ACTIVE'
                GROUP BY
                    h.id,
                    h.name,
                    h.country,
                    h.streetAddress,
                    h.districtAddress,
                    h.city
                ORDER BY
                    COUNT(r.hotel) DESC
                LIMIT
                    18
            """)
    Optional<List<ReviewHotelIndexResponse>> findHotelsGroupByReviewTop();

    Optional<Page<Review>> findReviewsByUser_IdAndStatus(UUID userId, String status, Pageable pageable);
}
