package com.example.graduationthesis.review.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class ReviewHotelIndexResponse {
    private UUID hotelId;
    private String nameHotel;
    private String country;
    private String streetAddress;
    private String districtAddress;
    private String city;
    private Double reviewRating;
    private Long countReview;
    private String urlImage;

    public ReviewHotelIndexResponse(
            UUID hotelId,
            String nameHotel,
            String country,
            String streetAddress,
            String districtAddress,
            String city,
            Long countReview
    ) {
        this.hotelId = hotelId;
        this.nameHotel = nameHotel;
        this.country = country;
        this.streetAddress = streetAddress;
        this.districtAddress = districtAddress;
        this.city = city;
        this.countReview = countReview;
    }
}
