package com.example.graduationthesis.review.dto.response;

import com.example.graduationthesis.hotel.dto.HotelDTO;
import com.example.graduationthesis.review.dto.ReviewDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDTOAndHotel {
    private HotelDTO hotel;
    private ReviewDTO review;
}
