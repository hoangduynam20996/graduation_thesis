package com.example.graduationthesis.review.dto.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ReviewAddRequest {

    @NotNull
    private UUID hotelId;
    @NotNull
    private UUID categoryId;
    @NotNull
    @Min(value = 0)
    @Max(value = 10)
    private Double rating;
    @NotBlank
    private String reviewComment;
    @NotBlank
    private String roomName;
    @NotBlank
    private String bedName;
    @NotBlank
    private String roomTypeName;
    @NotNull
    private Integer totalDay;
    @NotBlank
    private String fullName;
    @NotNull
    private LocalDate bookingDate;
}
