package com.example.graduationthesis.review.dto.response;

import com.example.graduationthesis.category.dto.CategoryDTO;
import com.example.graduationthesis.review.dto.ReviewDTO;
import com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class ReviewSyntheticResponse {

    private Double totalRatingReview;

    private Integer totalNumberOfReviews;

    private List<StatisticalRatingGroupByName> statisticalRatingGroupByNames;

    private List<CategoryDTO> categories;

    private List<ReviewDTO> reviewResponse;
}
