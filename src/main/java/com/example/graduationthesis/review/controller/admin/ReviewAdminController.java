package com.example.graduationthesis.review.controller.admin;

import com.example.graduationthesis.constant.ReviewConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.review.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + ReviewConstant.API_REVIEW)
public class ReviewAdminController {
//    private final ReviewService reviewService;

//    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
//    @GetMapping("/{rid}")
//    public ResponseEntity<?> deleteReview(
//            @PathVariable("rid") UUID rid
//    ) {
//        return new ResponseEntity<>(reviewService.deleteReview(rid), HttpStatus.OK);
//    }
}
