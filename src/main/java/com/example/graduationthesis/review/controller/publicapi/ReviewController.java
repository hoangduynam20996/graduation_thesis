package com.example.graduationthesis.review.controller.publicapi;

import com.example.graduationthesis.constant.ReviewConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.review.dto.response.ReviewSyntheticResponse;
import com.example.graduationthesis.review.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + ReviewConstant.API_REVIEW)
@RequiredArgsConstructor
public class ReviewController {

    private final ReviewService reviewService;

    @GetMapping("/{hid}")
    public ResponseEntity<ReviewSyntheticResponse> getReviewsByHotel(
            @PathVariable("hid") UUID hid,
            @RequestParam(value = SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(value = SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(reviewService.findReviewsByHotel(hid, currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

}
