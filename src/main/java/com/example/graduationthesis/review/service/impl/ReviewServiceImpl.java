package com.example.graduationthesis.review.service.impl;

import com.example.graduationthesis.booking.repo.BookingRepo;
import com.example.graduationthesis.category.entity.Category;
import com.example.graduationthesis.category.mapper.CategoryDTOMapper;
import com.example.graduationthesis.category.repo.CategoryRepo;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.mapper.HotelDTOMapper;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.jwt.JwtService;
import com.example.graduationthesis.review.dto.ReviewDTO;
import com.example.graduationthesis.review.dto.request.ReviewAddRequest;
import com.example.graduationthesis.review.dto.request.ReviewUpdateRequest;
import com.example.graduationthesis.review.dto.response.ReviewDTOAndHotel;
import com.example.graduationthesis.review.dto.response.ReviewResponse;
import com.example.graduationthesis.review.dto.response.ReviewResponses;
import com.example.graduationthesis.review.dto.response.ReviewSyntheticResponse;
import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.review.mapper.ReviewDTOMapper;
import com.example.graduationthesis.review.repo.ReviewRepo;
import com.example.graduationthesis.review.service.ReviewService;
import com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName;
import com.example.graduationthesis.reviewcategory.entity.ReviewCategory;
import com.example.graduationthesis.reviewcategory.repo.ReviewCategoryRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.repo.UserRepo;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ReviewServiceImpl extends AbsUserServiceUtil implements ReviewService {

    private final ReviewRepo reviewRepo;

    private final ReviewDTOMapper reviewDTOMapper;

    private final ReviewCategoryRepo reviewCategoryRepo;

    private final HotelRepo hotelRepo;



    private final CategoryRepo categoryRepo;

    private final CategoryDTOMapper categoryDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final BookingRepo bookingRepo;

    private final HttpServletRequest httpServletRequest;

    private final JwtService jwtService;

    private final UserRepo userRepo;
    private final HotelDTOMapper hotelDTOMapper;
    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public long countReviewByHotelId(UUID hid) {
        return reviewRepo.countReviewByHotelId(hid);
    }

    @Override
    public ReviewResponse addReview(ReviewAddRequest request) {
        Category category = categoryRepo.findById(request.getCategoryId())
                .orElseThrow(() -> new ApiRequestException("", "find category by id " + request.getCategoryId() + " not found!."));

        Hotel hotel = hotelRepo.findById(request.getHotelId())
                .orElseThrow(() -> new ApiRequestException("", "find hotel by id " + request.getHotelId() + " not found!"));

        User user = getUser();
        if (bookingRepo.findBookingByHotelAndUser(hotel.getId(), user.getId()) == null) {
            throw new ApiRequestException("", "Booking by hotel and user not exist");
        }
        Review review = reviewRepo.save(
                Review.builder()
                        .hotel(hotel)
                        .user(user)
                        .rating(request.getRating())
                        .reviewComment(request.getReviewComment())
                        .roomName(request.getRoomName())
                        .bedName(request.getBedName())
                        .roomTypeName(request.getRoomTypeName())
                        .totalDay(request.getTotalDay())
                        .fullName(request.getFullName())
                        .bookingDate(request.getBookingDate())
                        .reviewDate(new Date(System.currentTimeMillis()))
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        reviewCategoryRepo.save(
                ReviewCategory.builder()
                        .review(review)
                        .category(category)
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        return ReviewResponse.builder()
                .code(ResourceConstant.RV_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(reviewDTOMapper.apply(review))
                .message(getMessageBundle(ResourceConstant.RV_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }

    @Override
    public ReviewResponse updateReview(ReviewUpdateRequest request) {
        User user = getUser();

        Review review = reviewRepo.findReviewByUser_IdAndStatusAndId(user.getId(), SystemConstant.STATUS_ACTIVE, request.getReviewId())
                .orElseThrow(() -> new ApiRequestException("", "find review by id " + request.getReviewId() + " not found!."));

        if (!Objects.equals(review.getRating(), request.getRating()))
            review.setRating(request.getRating());

        if (!review.getReviewComment().equals(request.getReviewComment()))
            review.setReviewComment(request.getReviewComment());

        Review save = reviewRepo.save(review);
        return ReviewResponse.builder()
                .code(ResourceConstant.RV_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(reviewDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.RV_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();


    }

    @Override
    public ReviewSyntheticResponse findReviewsByHotel(UUID hid, Integer page, Integer limit) {
        Pageable pageable = baseAmenityUtil.pageable(page, limit);
        List<Review> reviews = reviewRepo.findReviewsByHotelId(hid, pageable)
                .orElseThrow(() -> new ApiRequestException("", "find reviews by hotel id " + hid + " not found!."));
        List<ReviewDTO> reviewDTOS = reviews.stream()
                .map(reviewDTOMapper)
                .toList();

        List<StatisticalRatingGroupByName> statisticalRatingGroupByNames =
                reviewCategoryRepo.statisticalRatingGroupNameByHotelId(hid)
                        .orElseThrow(() -> new ApiRequestException("", "statistic by hotel not found!."));

        double totalRatingReview = statisticalRatingGroupByNames.stream()
                .mapToDouble(StatisticalRatingGroupByName::averageRating)
                .sum();

        List<Category> categories = categoryRepo.findCategoriesByStatus(SystemConstant.STATUS_ACTIVE)
                .orElseThrow(() -> new ApiRequestException("", "find category by status active not found!"));

        return ReviewSyntheticResponse.builder()
                .totalRatingReview(totalRatingReview / statisticalRatingGroupByNames.size())
                .totalNumberOfReviews(reviews.size())
                .statisticalRatingGroupByNames(statisticalRatingGroupByNames)
                .categories(
                        categories.stream()
                                .map(categoryDTOMapper)
                                .toList()
                )
                .reviewResponse(reviewDTOS)
                .build();
    }

    @Override
    public ReviewResponse deleteReview(UUID rid) {
        Review review = reviewRepo.findById(rid)
                .orElseThrow(() -> new ApiRequestException("", "find review by id " + rid + " not found!."));
        review.setIsDeleted(SystemConstant.NO_ACTIVE);
        review.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        reviewRepo.save(review);
        return ReviewResponse.builder()
                .code(ResourceConstant.RV_008)
                .status(SystemConstant.STATUS_SUCCESS)
                .data("Review deleted is id : " + review.getId())
                .message("Delete Success")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ReviewResponses getAllReviewByUser(String status, Integer page, Integer limit) {
        Pageable pageable= baseAmenityUtil.pageable(page, limit);

        User user =null;

        List<ReviewDTOAndHotel> list = null;
        String authorization = httpServletRequest.getHeader("Authorization");
        if (authorization != null && authorization.startsWith("Bearer ")) {
            String token = authorization.substring(7);
            String email = jwtService.extractUsername(token);
            user=userRepo.findUserByEmailAndStatus(email, SystemConstant.STATUS_ACTIVE)
                    .orElseThrow(() -> new UsernameNotFoundException("find user by " + email + " not found!."));
            Page<Review> reviews=reviewRepo.findReviewsByUser_IdAndStatus(user.getId(),status,pageable)
                    .orElseThrow(()->new ApiRequestException("", "find review by user id  not found!."));
            list = reviews.stream().map(review ->
                    {
                        Hotel hotel= hotelRepo.findById(review.getHotel().getId())
                                .orElseThrow(()->new ApiRequestException("", "find hotel by id "+review.getHotel().getId()+" not found!."));
                        return
                                ReviewDTOAndHotel.builder()
                                        .hotel(hotelDTOMapper.apply(hotel))
                                        .review(reviewDTOMapper.apply(review))
                                        .build();
                    }
            ).collect(Collectors.toList());
        }
        return ReviewResponses.builder()
                .code(ResourceConstant.RV_010)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message("Get all review by user success")
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, list.size()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }


}
