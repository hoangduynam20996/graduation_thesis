package com.example.graduationthesis.review.service;

import com.example.graduationthesis.review.dto.request.ReviewAddRequest;
import com.example.graduationthesis.review.dto.request.ReviewUpdateRequest;
import com.example.graduationthesis.review.dto.response.ReviewResponse;
import com.example.graduationthesis.review.dto.response.ReviewResponses;
import com.example.graduationthesis.review.dto.response.ReviewSyntheticResponse;

import java.util.UUID;

public interface ReviewService {

    long countReviewByHotelId(UUID hid);

    ReviewResponse addReview(ReviewAddRequest request);

    ReviewResponse updateReview(ReviewUpdateRequest request);

    ReviewSyntheticResponse findReviewsByHotel(UUID hid, Integer page, Integer limit);

    ReviewResponse deleteReview(UUID rid);

    ReviewResponses getAllReviewByUser(String status, Integer page, Integer limit);
}
