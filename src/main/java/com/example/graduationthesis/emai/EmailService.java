package com.example.graduationthesis.emai;

import com.example.graduationthesis.booking.dto.response.BookingEmailCustomer;
import com.example.graduationthesis.bookingdetail.entity.BookingDetail;

import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.util.List;

public interface EmailService {

    void send(String to, String email, String subject, BufferedImage qrCodeImage);

    String buildEmailWelcome(String email);

    String buildEmail(String name, String link, String password, boolean isValidEmail);

    String buildEmailCongratulation(String email);

    String buildEmailVerify(String email);

    String buildEmailContract(String mail);

    String buildEmailWaitForConfirmation(String mail);

    String buildEmailBookingCustomer(BookingEmailCustomer bookingEmailCustomer);

    String buildEmailBookingCancel(BookingEmailCustomer bookingEmailCustomer);

    String buildEmailBookingOwner(BookingEmailCustomer bookingEmailCustomer);

    void sendMailConfirmBooking(String fullName, String email, String phoneNumber, String bkCode, String pCode, String emailHotelOwner,
                                double totalPrice, LocalDate bookingDate, List<BookingDetail> bookingDetails);
}
