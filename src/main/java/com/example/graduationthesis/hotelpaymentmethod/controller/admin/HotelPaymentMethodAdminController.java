package com.example.graduationthesis.hotelpaymentmethod.controller.admin;

import com.example.graduationthesis.constant.HotelPaymentMethodConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotelpaymentmethod.dto.request.HotelPaymentMethodRequest;
import com.example.graduationthesis.hotelpaymentmethod.dto.response.HotelPaymentMethodResponse;
import com.example.graduationthesis.hotelpaymentmethod.dto.response.HotelPaymentMethodResponses;
import com.example.graduationthesis.hotelpaymentmethod.service.HotelPaymentMethodService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + HotelPaymentMethodConstant.API_HOTEL_PAYMENT_METHOD)
@RequiredArgsConstructor
public class HotelPaymentMethodAdminController {

    private final HotelPaymentMethodService hotelPaymentMethodService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<HotelPaymentMethodResponse> addHotelPaymentMethod(
            @RequestHeader("jwtToken") String jwtToken,
            @Valid @RequestBody HotelPaymentMethodRequest request
    ) {
        return new ResponseEntity<>(hotelPaymentMethodService.addHotelPaymentMethod(jwtToken, request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<HotelPaymentMethodResponses> findAllHotelPaymentMethod(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage) {
        return new ResponseEntity<>(hotelPaymentMethodService.findAllHotelPaymentMethod(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }
}
