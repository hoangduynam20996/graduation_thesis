package com.example.graduationthesis.hotelpaymentmethod.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotelpaymentmethod.dto.HotelPaymentMethodDTO;
import com.example.graduationthesis.hotelpaymentmethod.dto.request.HotelPaymentMethodRequest;
import com.example.graduationthesis.hotelpaymentmethod.dto.response.HotelPaymentMethodResponse;
import com.example.graduationthesis.hotelpaymentmethod.dto.response.HotelPaymentMethodResponses;
import com.example.graduationthesis.hotelpaymentmethod.entity.HotelPaymentMethod;
import com.example.graduationthesis.hotelpaymentmethod.mapper.HotelPaymentMethodDTOMapper;
import com.example.graduationthesis.hotelpaymentmethod.repo.HotelPaymentMethodRepo;
import com.example.graduationthesis.hotelpaymentmethod.service.HotelPaymentMethodService;
import com.example.graduationthesis.session.common.SessionResponseUtil;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSession;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSessionRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class HotelPaymentMethodServiceImpl implements HotelPaymentMethodService {

    private final HotelPaymentMethodRepo hotelPaymentMethodRepo;

    private final HotelPaymentMethodDTOMapper hotelPaymentMethodDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final HotelPaymentMethodSessionRepo hotelPaymentMethodSessionRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public HotelPaymentMethodResponse addHotelPaymentMethod(String jwtToken, HotelPaymentMethodRequest request) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        hotelPaymentMethodSessionRepo.findHotelPaymentMethodSessionByJwtToken(jwtToken).ifPresent(hotelPaymentMethodSessionRepo::delete);
        StringBuilder address = new StringBuilder();
        if (!request.getFlagReceivingAddress()) {
            address.append(request.getHotelPaymentMethodAddressRequest().getStreetAddress())
                    .append(" ")
                    .append(request.getHotelPaymentMethodAddressRequest().getDistrictAddress())
                    .append(" ")
                    .append(request.getHotelPaymentMethodAddressRequest().getCity())
                    .append(" ")
                    .append(request.getHotelPaymentMethodAddressRequest().getCountry());
        }
        HotelPaymentMethodSession hotelPaymentMethodSession = hotelPaymentMethodSessionRepo.save(
                HotelPaymentMethodSession.builder()
                        .creditOrDebitCard(request.getCreditOrDebitCard())
                        .nameOnInvoice(request.getNameOnInvoice())
                        .invoiceAddress(address.toString())
                        .flagReceivingAddress(request.getFlagReceivingAddress())
                        .jwtToken(jwtToken)
                        .build()
        );

        return HotelPaymentMethodResponse.builder()
                .code("HTP_S_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("HOTEL_PAYMENT_METHOD", hotelPaymentMethodSession))
                .message("Add hotel payment method session success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelPaymentMethodResponses findAllHotelPaymentMethod(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<HotelPaymentMethod> hotelPaymentMethods = hotelPaymentMethodRepo.findAll(pageable);
        List<HotelPaymentMethodDTO> list = hotelPaymentMethods
                .stream()
                .map(hotelPaymentMethodDTOMapper)
                .toList();
        return HotelPaymentMethodResponses.builder()
                .code(ResourceConstant.HT_PM_002)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message(getMessageBundle(ResourceConstant.HT_PM_002))
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, hotelPaymentMethods.getTotalPages()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

}
