package com.example.graduationthesis.hotelpaymentmethod.repo;

import com.example.graduationthesis.hotelpaymentmethod.entity.HotelPaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface HotelPaymentMethodRepo extends JpaRepository<HotelPaymentMethod, UUID> {
}
