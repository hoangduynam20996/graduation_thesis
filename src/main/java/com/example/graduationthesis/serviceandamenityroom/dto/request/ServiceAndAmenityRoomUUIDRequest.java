package com.example.graduationthesis.serviceandamenityroom.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class ServiceAndAmenityRoomUUIDRequest {

    private UUID id;

}
