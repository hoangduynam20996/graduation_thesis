package com.example.graduationthesis.serviceandamenityroom.dto.response;

public record ServiceAndAmenityRoomNameResponse(String nameServiceAndAmenity) {
}
