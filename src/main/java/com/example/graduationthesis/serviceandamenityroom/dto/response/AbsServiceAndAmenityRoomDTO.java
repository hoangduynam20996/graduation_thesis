package com.example.graduationthesis.serviceandamenityroom.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsServiceAndAmenityRoomDTO {

    private Integer extraBed;

    private Boolean childrenSleepInCribs;

    private Boolean typeOfGuestChildren;

    private String childrenOld;

    private BigDecimal priceGuestChildren;

    private Boolean typeOfGuestAdults;

    private BigDecimal priceGuestAdults;
}
