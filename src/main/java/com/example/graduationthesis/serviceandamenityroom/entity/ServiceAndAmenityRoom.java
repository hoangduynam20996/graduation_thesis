package com.example.graduationthesis.serviceandamenityroom.entity;

import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.serviceandamenity.entity.ServiceAndAmenity;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_service_and_amenity_room")
public class ServiceAndAmenityRoom extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;
    @Column
    private Integer extraBed;
    @Column
    private Boolean childrenSleepInCribs;
    @Column
    private Boolean typeOfGuestChildren;

    private String childrenOld;

    private BigDecimal priceGuestChildren;

    private Boolean typeOfGuestAdults;

    private BigDecimal priceGuestAdults;
    @ManyToOne
    @JoinColumn(name = "service_and_amenity_id")
    private ServiceAndAmenity serviceAndAmenity;

}
