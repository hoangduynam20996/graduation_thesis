package com.example.graduationthesis.serviceandamenityroom.repo;

import com.example.graduationthesis.serviceandamenityroom.ServiceAndAmenitySqlNativeResult;
import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ServiceAndAmenityRoomRepo extends JpaRepository<ServiceAndAmenityRoom, UUID> {

    @Query(value = """
                SELECT
                    saa.name
                FROM
                    _service_and_amenity_room saar
                INNER JOIN
                    _room r ON r.id = saar.room_id
                INNER JOIN
                    _hotel h ON h.id = r.hotel_id
                INNER JOIN
                    _service_and_amenity saa ON saa.id = saar.service_and_amenity_id
                WHERE
                    h.id = ?1
            """, nativeQuery = true)
    Optional<List<ServiceAndAmenitySqlNativeResult>> findNameServiceAndAmenityByHotelId(UUID hotelId);
}
