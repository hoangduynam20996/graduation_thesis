package com.example.graduationthesis.serviceandamenityroom.controller.admin;

import com.example.graduationthesis.constant.ServiceAndAmenityRoomConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.serviceandamenityroom.dto.request.ServiceAndAmenityRoomAddRequest;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAndAmenityRoomResp;
import com.example.graduationthesis.serviceandamenityroom.service.ServiceAndAmenityRoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + ServiceAndAmenityRoomConstant.API_SERVICE_AND_AMENITY_ROOM)
@RequiredArgsConstructor
public class ServiceAndAmenityRoomAdminController {

    private final ServiceAndAmenityRoomService serviceAndAmenityService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<ServiceAndAmenityRoomResp> addServiceAndAmenityRoom(
            @RequestHeader("jwtToken") String jwtToken,
            @Validated @RequestBody ServiceAndAmenityRoomAddRequest request
    ) {
        return new ResponseEntity<>(serviceAndAmenityService.addServiceAndAmenityRoom(jwtToken, request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping
    public ResponseEntity<ServiceAndAmenityRoomResp> updateServiceAndAmenity(
            @Validated @RequestBody ServiceAndAmenityRoomAddRequest request
    ) {
        return new ResponseEntity<>(serviceAndAmenityService.updateServiceAndAmenityRoom(request), HttpStatus.OK);
    }
}
