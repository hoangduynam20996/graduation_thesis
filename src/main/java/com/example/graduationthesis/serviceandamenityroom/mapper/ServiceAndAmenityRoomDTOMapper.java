package com.example.graduationthesis.serviceandamenityroom.mapper;

import com.example.graduationthesis.serviceandamenityroom.dto.ServiceAndAmenityRoomDTO;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAndAmenityRoomNameResponse;
import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class ServiceAndAmenityRoomDTOMapper implements
        Function<ServiceAndAmenityRoom, ServiceAndAmenityRoomDTO> {

    private final ModelMapper mapper;

    @Override
    public ServiceAndAmenityRoomDTO apply(ServiceAndAmenityRoom serviceAndAmenityRoom) {
        return mapper.map(serviceAndAmenityRoom, ServiceAndAmenityRoomDTO.class);
    }

    public List<ServiceAndAmenityRoomNameResponse> apply(List<Object[]> requests) {
        return requests.stream()
                .map(item -> new ServiceAndAmenityRoomNameResponse(String.valueOf(item[0])))
                .toList();
    }
}
