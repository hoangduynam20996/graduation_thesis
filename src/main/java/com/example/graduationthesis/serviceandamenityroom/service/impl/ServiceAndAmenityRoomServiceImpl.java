package com.example.graduationthesis.serviceandamenityroom.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.room.repo.RoomRepo;
import com.example.graduationthesis.serviceandamenity.entity.ServiceAndAmenity;
import com.example.graduationthesis.serviceandamenity.repo.ServiceAndAmenityRepo;
import com.example.graduationthesis.serviceandamenityroom.dto.request.ServiceAndAmenityRoomAddRequest;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAndAmenityRoomResp;
import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import com.example.graduationthesis.serviceandamenityroom.repo.ServiceAndAmenityRoomRepo;
import com.example.graduationthesis.serviceandamenityroom.service.ServiceAndAmenityRoomService;
import com.example.graduationthesis.session.common.SessionResponseUtil;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSession;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomId;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomIdRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ServiceAndAmenityRoomServiceImpl implements ServiceAndAmenityRoomService {

    private final ServiceAndAmenityRoomRepo serviceAndAmenityRoomRepo;

    private final RoomRepo roomRepo;

    private final ServiceAndAmenityRepo serviceAndAmenityRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private final ServiceAmenityRoomIdRepo serviceAmenityRoomIdRepo;

    private final ServiceAmenityRoomSessionRepo serviceAmenityRoomSessionRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public ServiceAndAmenityRoomResp addServiceAndAmenityRoom(String jwtToken, ServiceAndAmenityRoomAddRequest request) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        serviceAmenityRoomIdRepo.findServiceAmenityRoomIdByJwtToken(jwtToken).ifPresent(serviceAmenityRoomIdRepo::deleteAll);
        serviceAmenityRoomSessionRepo.findServiceAmenityRoomSessionByJwtToken(jwtToken).ifPresent(serviceAmenityRoomSessionRepo::delete);
        List<ServiceAmenityRoomId> serviceAmenityRoomIds = new ArrayList<>();
        request.getServiceAndAmenityId().forEach(item -> {
            serviceAndAmenityRepo.findById(item.getId())
                    .orElseThrow(() -> new ApiRequestException("SAR_001", "find service and amenity by id " + item.getId() + " not found!."));

            serviceAmenityRoomIds.add(ServiceAmenityRoomId.builder()
                    .serviceAmenityRoomId(item.getId())
                    .jwtToken(jwtToken)
                    .build());
        });
        serviceAmenityRoomIdRepo.saveAll(serviceAmenityRoomIds);
        ServiceAmenityRoomSession serviceAmenityRoomSession = serviceAmenityRoomSessionRepo.save(ServiceAmenityRoomSession.builder()
                .extraBed(request.getExtraBed())
                .childrenSleepInCribs(request.getChildrenSleepInCribs())
                .typeOfGuestChildren(request.getTypeOfGuestChildren())
                .childrenOld(request.getChildrenOld())
                .priceGuestChildren(request.getPriceGuestChildren())
                .typeOfGuestAdults(request.getTypeOfGuestAdults())
                .priceGuestAdults(request.getPriceGuestAdults())
                .jwtToken(jwtToken)
                .build());

        return ServiceAndAmenityRoomResp.builder()
                .code("SAAR_S_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("HOTEL_SERVICE", serviceAmenityRoomSession))
                .message("Add service and amenity room session success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ServiceAndAmenityRoomResp updateServiceAndAmenityRoom(ServiceAndAmenityRoomAddRequest request) {
        Room room = getRoom(request.getRoomId());

        List<ServiceAndAmenity> serviceAndAmenitiesRequest = request.getServiceAndAmenityId()
                .stream()
                .map(value -> serviceAndAmenityRepo.findById(value.getId())
                        .orElseThrow(() -> new ApiRequestException("", "find service and amenity by id " + value.getId() + " not found!.")))
                .toList();

        List<ServiceAndAmenity> serviceAndAmenities = room.getServiceAndAmenityRooms()
                .stream()
                .map(ServiceAndAmenityRoom::getServiceAndAmenity)
                .toList();

        List<ServiceAndAmenity> result = new ArrayList<>();

        result.addAll(serviceAndAmenitiesRequest.stream()
                .filter(item -> !serviceAndAmenities.contains(item))
                .toList());

        result.addAll(serviceAndAmenities.stream()
                .filter(item -> !serviceAndAmenitiesRequest.contains(item))
                .toList());

        List<ServiceAndAmenityRoom> serviceAndAmenityRooms = result.stream()
                .map(item ->
                        ServiceAndAmenityRoom.builder()
                                .room(room)
                                .extraBed(request.getExtraBed())
                                .childrenSleepInCribs(request.getChildrenSleepInCribs())
                                .typeOfGuestChildren(request.getTypeOfGuestChildren())
                                .childrenOld(request.getChildrenOld())
                                .priceGuestChildren(request.getPriceGuestChildren())
                                .typeOfGuestAdults(request.getTypeOfGuestAdults())
                                .priceGuestAdults(request.getPriceGuestAdults())
                                .serviceAndAmenity(item)
                                .build()
                )
                .collect(Collectors.toList());

        room.getServiceAndAmenityRooms()
                .forEach(item -> {
                    item.setExtraBed(request.getExtraBed());
                    item.setChildrenSleepInCribs(request.getChildrenSleepInCribs());
                    item.setTypeOfGuestChildren(request.getTypeOfGuestChildren());
                    item.setChildrenOld(request.getChildrenOld());
                    item.setPriceGuestChildren(request.getPriceGuestChildren());
                    item.setTypeOfGuestAdults(request.getTypeOfGuestAdults());
                    item.setPriceGuestAdults(request.getPriceGuestAdults());
                });

        serviceAndAmenityRoomRepo.saveAll(room.getServiceAndAmenityRooms());
        serviceAndAmenityRoomRepo.saveAll(serviceAndAmenityRooms);
        return ServiceAndAmenityRoomResp.builder()
                .code(ResourceConstant.SAA_R007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data("")
                .message(getMessageBundle(ResourceConstant.SAA_R007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ServiceAndAmenityRoomResp deleteServiceAndAmenityRoom(UUID uuid) {
        return null;
    }

    private Room getRoom(UUID uuid) {
        return roomRepo.findById(uuid)
                .orElseThrow(() -> new ApiRequestException("", "find room by id " + uuid + " not found!."));
    }

}
