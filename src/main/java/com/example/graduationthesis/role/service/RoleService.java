package com.example.graduationthesis.role.service;


import com.example.graduationthesis.role.dto.request.RoleAddRequest;
import com.example.graduationthesis.role.dto.request.RoleUpdateRequest;
import com.example.graduationthesis.role.dto.response.RoleResponse;
import com.example.graduationthesis.role.dto.response.RoleResponses;

import java.util.UUID;

public interface RoleService {

    RoleResponse addRole(RoleAddRequest request);

    RoleResponse updateRole(UUID userId, RoleUpdateRequest request);

    RoleResponse deleteRole(UUID roleId);

    RoleResponse findById(UUID roleId);

    RoleResponses findAllRole(Integer currentPage, Integer limitPage);

}
