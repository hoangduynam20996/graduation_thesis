package com.example.graduationthesis.role.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsRoleDTO {

    private UUID roleId;

    private String roleName;

    private String roleCode;

    private Boolean isDeleted;

    private String status;
}
