package com.example.graduationthesis.role.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class RoleDTO extends AbsRoleDTO {
}
