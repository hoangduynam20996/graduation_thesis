package com.example.graduationthesis.role.dto.request;

import com.example.graduationthesis.permission.dto.request.PermissionIdRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RoleUpdateRequest {
    private String roleName;
    private String roleCode;
    private List<PermissionIdRequest> permissionIdsRequest;
}
