package com.example.graduationthesis.role.dto.request;

import com.example.graduationthesis.constant.ResourceConstant;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateRoleRequest {

    @NotNull(message = ResourceConstant.MSG_ID_001)
    private List<RoleIdRequest> roleIds;

    public record RoleIdRequest(UUID roleId) {
    }
}
