package com.example.graduationthesis.role.mapper;

import com.example.graduationthesis.permission.dto.PermissionDto;
import com.example.graduationthesis.permission.mapper.PermissionDtoMapper;
import com.example.graduationthesis.role.dto.response.RoleDTO;
import com.example.graduationthesis.role.dto.response.RolesDTOResp;
import com.example.graduationthesis.role.entity.Role;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class RoleDTOMapper implements Function<Role, RoleDTO> {

    private final ModelMapper mapper;

    private final PermissionDtoMapper permissionDtoMapper;

    public RolesDTOResp applyRoleDetail(Role role) {
        List<PermissionDto> permissions = role.getPermissionRoles().stream()
                .map(item -> permissionDtoMapper.apply(item.getPermission()))
                .toList();
        return RolesDTOResp.builder()
                .roleId(role.getId())
                .roleName(role.getRoleName())
                .roleCode(role.getRoleCode())
                .isDeleted(role.getIsDeleted())
                .status(role.getStatus())
                .permissions(permissions)
                .build();
    }

    @Override
    public RoleDTO apply(Role role) {
        return mapper.map(role, RoleDTO.class);
    }
}
