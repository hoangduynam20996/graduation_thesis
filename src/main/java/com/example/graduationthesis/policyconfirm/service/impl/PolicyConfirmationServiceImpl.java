package com.example.graduationthesis.policyconfirm.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.emai.EmailService;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.hotelimage.entity.HotelImage;
import com.example.graduationthesis.hotelimage.repo.HotelImageRepo;
import com.example.graduationthesis.hotelpaymentmethod.entity.HotelPaymentMethod;
import com.example.graduationthesis.hotelpaymentmethod.repo.HotelPaymentMethodRepo;
import com.example.graduationthesis.hotelpolicy.entity.HotelPolicy;
import com.example.graduationthesis.hotelpolicy.repo.HotelPolicyRepo;
import com.example.graduationthesis.hoteltype.entity.HotelType;
import com.example.graduationthesis.hoteltype.repo.HotelTypeRepo;
import com.example.graduationthesis.policyconfirm.dto.request.PolicyConfirmationRequest;
import com.example.graduationthesis.policyconfirm.dto.response.PolicyConfirmResponse;
import com.example.graduationthesis.policyconfirm.entity.PolicyConfirmation;
import com.example.graduationthesis.policyconfirm.repo.PolicyConfirmationRepo;
import com.example.graduationthesis.policyconfirm.service.PolicyConfirmationService;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.room.repo.RoomRepo;
import com.example.graduationthesis.roomtype.entity.RoomType;
import com.example.graduationthesis.roomtype.repo.RoomTypeRepo;
import com.example.graduationthesis.serviceandamenity.entity.ServiceAndAmenity;
import com.example.graduationthesis.serviceandamenity.repo.ServiceAndAmenityRepo;
import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import com.example.graduationthesis.serviceandamenityroom.repo.ServiceAndAmenityRoomRepo;
import com.example.graduationthesis.session.hotel.HotelSession;
import com.example.graduationthesis.session.hotel.HotelSessionRepo;
import com.example.graduationthesis.session.hotelimage.HotelImageSession;
import com.example.graduationthesis.session.hotelimage.HotelImageSessionRepo;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSession;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSessionRepo;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySession;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySessionRepo;
import com.example.graduationthesis.session.room.RoomSession;
import com.example.graduationthesis.session.room.RoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSession;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomId;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomIdRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class PolicyConfirmationServiceImpl extends AbsUserServiceUtil implements PolicyConfirmationService {

    private final EmailService emailService;

    private final HotelRepo hotelRepo;

    private final HotelImageRepo hotelImageRepo;

    private final HotelPolicyRepo hotelPolicyRepo;

    private final RoomRepo roomRepo;

    private final ServiceAndAmenityRoomRepo serviceAndAmenityRoomRepo;

    private final PolicyConfirmationRepo policyConfirmationRepo;

    private final HotelPaymentMethodRepo hotelPaymentMethodRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private final ServiceAmenityRoomSessionRepo serviceAmenityRoomSessionRepo;

    private final ServiceAmenityRoomIdRepo serviceAmenityRoomIdRepo;

    private final HotelPaymentMethodSessionRepo hotelPaymentMethodSessionRepo;

    private final HotelPolicySessionRepo hotelPolicySessionRepo;

    private final RoomSessionRepo roomSessionRepo;

    private final HotelSessionRepo hotelSessionRepo;

    private final HotelImageSessionRepo hotelImageSessionRepo;

    private final HotelTypeRepo hotelTypeRepo;

    private final RoomTypeRepo roomTypeRepo;

    private final ServiceAndAmenityRepo serviceAndAmenityRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public PolicyConfirmResponse addPolicyConfirmation(String jwtToken, PolicyConfirmationRequest request) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        if (!(request.getAcceptGeneralTermsPrivacy() && request.getCertificationOfLegalBusinessOperations())) {
            throw new ApiRequestException("Completed registration failed due to not accepting the terms!.", "");
        }

        HotelSession hotelSession = hotelSessionRepo.findHotelSessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("HTS_001", "Find not found!..."));

        HotelType hotelType = hotelTypeRepo.findById(hotelSession.getHotelTypeId())
                .orElseThrow(() -> new ApiRequestException("HTT_001", "Find hotel type by id not found!"));

        User user = getUser();

        Hotel hotel = hotelRepo.save(
                Hotel.builder()
                        .user(user)
                        .hotelType(hotelType)
                        .name(hotelSession.getName())
                        .rating(hotelSession.getRating())
                        .contactPerson(hotelSession.getContactPerson())
                        .phoneNumber(hotelSession.getPhoneNumber())
                        .phoneNumberTwo(hotelSession.getPhoneNumberTwo())
                        .streetAddress(removeAccents(hotelSession.getStreetAddress()))
                        .districtAddress(removeAccents(hotelSession.getDistrictAddress()))
                        .country(removeAccents(hotelSession.getCountry()))
                        .city(removeAccents(hotelSession.getCity()))
                        .postalCode(hotelSession.getPostalCode())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_NO_ACTIVE)
                        .build()
        );

        RoomSession roomSession = roomSessionRepo.findRoomSessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("ROS_001", "Find not found!..."));

        RoomType roomType = roomTypeRepo.findById(roomSession.getRoomTypeId())
                .orElseThrow(() -> new ApiRequestException("SARS_001", "Find not found!.."));

        Room room = roomRepo.save(
                Room.builder()
                        .hotel(hotel)
                        .roomType(roomType)
                        .pricePerNight(roomSession.getPricePerNight())
                        .roomName(roomSession.getRoomName())
                        .roomNameCustom(roomSession.getRoomNameCustom())
                        .quantityRoom(roomSession.getQuantityRoom())
                        .roomArea(roomSession.getRoomArea())
                        .maxOccupancy(roomSession.getMaxOccupancy())
                        .bedName(roomSession.getBedName())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .saleDate(LocalDate.now())
                        .build()
        );

        ServiceAmenityRoomSession serviceAmenityRoomSession = serviceAmenityRoomSessionRepo.findServiceAmenityRoomSessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("SARS_001", "Find not found!.."));

        List<ServiceAmenityRoomId> serviceAmenityRoomIds = serviceAmenityRoomIdRepo.findServiceAmenityRoomIdByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("SARI_001", "Find not found!..."));

        List<ServiceAndAmenityRoom> serviceAndAmenityRooms = new ArrayList<>();
        serviceAmenityRoomIds
                .forEach(item -> {
                    ServiceAndAmenity serviceAndAmenity = serviceAndAmenityRepo.findById(item.getServiceAmenityRoomId())
                            .orElseThrow(() -> new ApiRequestException("HTPM_001", "Find not found!..."));
                    serviceAndAmenityRooms.add(
                            ServiceAndAmenityRoom.builder()
                                    .serviceAndAmenity(serviceAndAmenity)
                                    .extraBed(serviceAmenityRoomSession.getExtraBed())
                                    .childrenSleepInCribs(serviceAmenityRoomSession.getChildrenSleepInCribs())
                                    .typeOfGuestAdults(serviceAmenityRoomSession.getTypeOfGuestAdults())
                                    .childrenOld(serviceAmenityRoomSession.getChildrenOld())
                                    .priceGuestAdults(serviceAmenityRoomSession.getPriceGuestAdults())
                                    .priceGuestChildren(serviceAmenityRoomSession.getPriceGuestChildren())
                                    .room(room)
                                    .isDeleted(SystemConstant.ACTIVE)
                                    .status(SystemConstant.STATUS_ACTIVE)
                                    .build()
                    );
                });
        serviceAndAmenityRoomRepo.saveAll(serviceAndAmenityRooms);

        HotelPaymentMethodSession hotelPaymentMethodSession = hotelPaymentMethodSessionRepo.findHotelPaymentMethodSessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("HTPM_001", "Find not found!..."));

        HotelPaymentMethod hotelPaymentMethod = hotelPaymentMethodRepo.save(
                HotelPaymentMethod.builder()
                        .creditOrDebitCard(hotelPaymentMethodSession.getCreditOrDebitCard())
                        .commissionPercentage(SystemConstant.COMMISSION_PERCENTAGE)
                        .nameOnInvoice(hotelPaymentMethodSession.getNameOnInvoice())
                        .invoiceAddress(hotelPaymentMethodSession.getInvoiceAddress())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        hotel.setHotelPaymentMethod(hotelPaymentMethod);
        hotelRepo.save(hotel);

        HotelPolicySession hotelPolicySession = hotelPolicySessionRepo.findHotelPolicySessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("HPS_001", "Find not found!..."));

        hotelPolicyRepo.save(
                HotelPolicy.builder()
                        .hotel(hotel)
                        .bookingPolicy(hotelPolicySession.getBookingPolicy())
                        .cancellationPolicy(hotelPolicySession.getCancellationPolicy())
                        .petPolicy(hotelPolicySession.getPetPolicy())
                        .smokingPolicy(hotelPolicySession.getSmokingPolicy())
                        .additionalPolices(hotelPolicySession.getAdditionalPolices())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        List<HotelImageSession> imageSessions = hotelImageSessionRepo.findHotelImageSessionByJwtToken(jwtToken)
                .orElseThrow(() -> new ApiRequestException("ROS_001", "Find not found!..."));

        List<HotelImage> hotelImages = new ArrayList<>();
        imageSessions.forEach(item -> hotelImages.add(
                HotelImage.builder()
                        .hotel(hotel)
                        .urlImage(item.getUrlImage())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        ));
        hotelImageRepo.saveAll(hotelImages);

        PolicyConfirmation policyConfirmation = policyConfirmationRepo.save(PolicyConfirmation.builder()
                .hotels(List.of(hotel))
                .acceptGeneralTermsPrivacy(SystemConstant.STATUS_ACCEPT_GENERAL_TERMS_PRIVACY_ACTIVE)
                .certificationOfLegalBusinessOperations(SystemConstant.STATUS_CERTIFICATION_OF_LEGAL_BUSINESS_OPERATIONS_ACTIVE)
                .isDeleted(SystemConstant.ACTIVE)
                .status(SystemConstant.STATUS_ACTIVE)
                .build());

        hotel.setPolicyConfirmation(policyConfirmation);
        hotelRepo.save(hotel);
        deleteAllHotelSession(jwtToken);

        emailService.buildEmailCongratulation(user.getEmail());

        return PolicyConfirmResponse.builder()
                .code(ResourceConstant.PL_C_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data("Add Hotel success!..")
                .message(getMessageBundle(ResourceConstant.PL_C_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private void deleteAllHotelSession(String jwtToken) {
        hotelSessionRepo.deleteByJwtToken(jwtToken);
        hotelPolicySessionRepo.deleteByJwtToken(jwtToken);
        hotelImageSessionRepo.deleteByJwtToken(jwtToken);
        roomSessionRepo.deleteByJwtToken(jwtToken);
        serviceAmenityRoomIdRepo.deleteByJwtToken(jwtToken);
        serviceAmenityRoomSessionRepo.deleteByJwtToken(jwtToken);
        hotelPaymentMethodSessionRepo.deleteByJwtToken(jwtToken);
    }

}
