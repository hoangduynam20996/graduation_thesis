package com.example.graduationthesis.policyconfirm.repo;

import com.example.graduationthesis.policyconfirm.entity.PolicyConfirmation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PolicyConfirmationRepo extends JpaRepository<PolicyConfirmation, UUID> {
}
