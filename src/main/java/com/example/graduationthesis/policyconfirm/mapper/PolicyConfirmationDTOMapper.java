package com.example.graduationthesis.policyconfirm.mapper;

import com.example.graduationthesis.policyconfirm.dto.PolicyConfirmationDTO;
import com.example.graduationthesis.policyconfirm.entity.PolicyConfirmation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class PolicyConfirmationDTOMapper implements
        Function<PolicyConfirmation, PolicyConfirmationDTO> {

    private final ModelMapper mapper;

    @Override
    public PolicyConfirmationDTO apply(PolicyConfirmation policyConfirmation) {
        return mapper.map(policyConfirmation, PolicyConfirmationDTO.class);
    }
}
