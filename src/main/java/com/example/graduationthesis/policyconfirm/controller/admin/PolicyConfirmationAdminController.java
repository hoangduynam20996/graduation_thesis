package com.example.graduationthesis.policyconfirm.controller.admin;

import com.example.graduationthesis.constant.PolicyConfirmationConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.policyconfirm.dto.request.PolicyConfirmationRequest;
import com.example.graduationthesis.policyconfirm.dto.response.PolicyConfirmResponse;
import com.example.graduationthesis.policyconfirm.service.PolicyConfirmationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + PolicyConfirmationConstant.API_POLICY_CONFIRMATION)
@RequiredArgsConstructor
public class PolicyConfirmationAdminController {

    private final PolicyConfirmationService policyConfirmationService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<PolicyConfirmResponse> addPolicyConfirmation(
            @RequestHeader("jwtToken") String jwtToken,
            @Validated @RequestBody PolicyConfirmationRequest request
    ) {
        return new ResponseEntity<>(policyConfirmationService.addPolicyConfirmation(jwtToken, request), HttpStatus.CREATED);
    }

}
