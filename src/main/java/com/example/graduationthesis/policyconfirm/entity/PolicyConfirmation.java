package com.example.graduationthesis.policyconfirm.entity;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_policy_confirmation")
public class PolicyConfirmation extends BaseEntityUtil {

    @Column
    private Integer acceptGeneralTermsPrivacy;
    @Column
    private Integer certificationOfLegalBusinessOperations;
    @OneToMany(mappedBy = "policyConfirmation")
    private List<Hotel> hotels;
}
