package com.example.graduationthesis.policyconfirm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PolicyConfirmationRequest {

    private Boolean acceptGeneralTermsPrivacy;

    private Boolean certificationOfLegalBusinessOperations;
}
