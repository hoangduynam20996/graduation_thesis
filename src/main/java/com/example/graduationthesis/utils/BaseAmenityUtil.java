package com.example.graduationthesis.utils;

import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;


@Service
@RequiredArgsConstructor
public class BaseAmenityUtil {

    private final JwtService jwtService;

    private final ResourceBundle resourceBundle = ResourceBundle.getBundle("i18n/messages");

    public String getMessageBundle(String key) {
        return resourceBundle.getString(key);
    }

    public Long responseTime() {
        return TimeUnit.MICROSECONDS.toSeconds(System.currentTimeMillis());
    }

    public Pageable pageable(int currentPage, int limitPage) {
        return PageRequest.of(currentPage - 1, limitPage);
    }

    public PageableResponseUtil pageableResponseUtil(Pageable pageable, int totalPage) {
        return new PageableResponseUtil(totalPage, pageable);
    }

    public void checkJwtToken(String jwtToken) {
        if (jwtService.isTokenExpired(jwtToken)) {
            throw new ApiRequestException("JWT_S_001", "JwtToken session expired!..");
        }
    }
}
