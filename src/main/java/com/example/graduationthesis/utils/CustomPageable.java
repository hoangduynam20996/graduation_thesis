package com.example.graduationthesis.utils;

import io.micrometer.common.lang.NonNullApi;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@NonNullApi
public class CustomPageable implements Pageable {

    private final PageRequest pageRequest;

    @Getter
    private int totalPage;

    public CustomPageable(int currentPage, int limitPage, int totalPage) {
        this.pageRequest = PageRequest.of(currentPage - 1, limitPage);
        this.totalPage = totalPage;
    }

    @Override
    public int getPageNumber() {
        return pageRequest.getPageNumber();
    }

    @Override
    public int getPageSize() {
        return pageRequest.getPageSize();
    }

    @Override
    public long getOffset() {
        return pageRequest.getOffset();
    }

    @Override
    public Sort getSort() {
        return pageRequest.getSort();
    }

    @Override
    public Pageable next() {
        return pageRequest.next();
    }

    @Override
    public Pageable previousOrFirst() {
        return pageRequest.previousOrFirst();
    }

    @Override
    public Pageable first() {
        return pageRequest.first();
    }

    @Override
    public Pageable withPage(int pageNumber) {
        return pageRequest.withPage(pageNumber);
    }

    @Override
    public boolean hasPrevious() {
        return pageRequest.hasPrevious();
    }

}
