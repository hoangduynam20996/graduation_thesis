package com.example.graduationthesis.utils;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

public class ExcelUtil {

    public static final String FILE_FORMAT_XLS = "xls";

    public static final String FILE_FORMAT_XLSX = "xlsx";

    public static final String FONT_NAME = "Times New Roman";

    public static final String CONTENT_TYPE = "application/octet-stream"; // Define the content type as octet-stream, indicating binary data

    public static final String HEADER_KEY = "Content-Disposition"; // Define the header key for content disposition

    public static final String HEADER_VALUE = "attachment; filename=excel.xlsx"; // Define the header value, specifying that the file should be treated as an attachment with the filename excel.xlsx

    /**
     * Configures the HTTP servlet response to send an Excel file as an attachment.
     *
     * @param response HttpServletResponse object to configure
     * @author Duy Nam
     */
    public static void configExcelResponse(String fileName, String fileFormat, HttpServletResponse response) {
        response.setContentType(CONTENT_TYPE); // Set the content type header to application/octet-stream
        final String headerValue = "attachment; filename=" + fileName + "." + fileFormat;
        response.setHeader(HEADER_KEY, headerValue); // Set the content disposition header to attachment; filename=excel.xlsx
    }

    /**
     * Creates and returns a Workbook object based on the file extension of the provided Excel file path.
     * <p>
     * This method checks the file extension of the given Excel file path to determine whether
     * to create an XSSFWorkbook (for .xlsx files) or an HSSFWorkbook (for .xls files).
     * If the file extension is not recognized as either .xlsx or .xls, an IllegalArgumentException is thrown.
     *
     * @param fileFormat the file format.
     * @return A Workbook object appropriate for the specified file extension.
     * @throws IllegalArgumentException If the file extension is not .xlsx or .xls.
     * @author Duy Nam
     */
    public static Workbook getWorkbook(String fileFormat) {
        // Check if the file path ends with .xlsx and create an XSSFWorkbook if it does
        if (fileFormat.endsWith("xlsx")) {
            return new XSSFWorkbook();
            // Check if the file path ends with .xls and create an HSSFWorkbook if it does
        } else if (fileFormat.endsWith("xls")) {
            return new HSSFWorkbook();
            // Throw an exception if the file extension is not recognized
        } else {
            throw new IllegalArgumentException("The specified file is not an Excel file");
        }
    }

    /**
     * Writes the header row to the given sheet.
     * <p>
     * This method creates a header row at the specified row index in the sheet,
     * using the provided array of header names. Each header cell is styled
     * using a predefined header style.
     *
     * @param sheet    The Sheet object where the header row will be written.
     * @param headers  An array of strings representing the header names.
     * @param rowIndex The index of the row where the header will be written.
     * @author Duy Nam
     */
    public static void writeHeader(Sheet sheet, String[] headers, String fontName, int rowIndex) {
        // Create a cell style for the header using a predefined method
        CellStyle cellStyle = createStyleForHeader(sheet, fontName);

        // Create a new row at the specified index
        Row row = sheet.createRow(rowIndex);

        // Iterate through the header names and create cells
        for (int i = 0; i < headers.length; i++) {
            // Create a new cell in the current row at the specified column index
            Cell cell = row.createCell(i);
            // Apply the header cell style to the cell
            cell.setCellStyle(cellStyle);
            // Set the cell value to the corresponding header name
            cell.setCellValue(headers[i]);
        }
    }

    /**
     * Creates a cell style for the header row in the given sheet.
     * <p>
     * This method creates and returns a CellStyle object with specific formatting
     * for the header row. The style includes a bold "Times New Roman" font with
     * a font size of 10 points.
     *
     * @param sheet The Sheet object where the header style will be applied.
     * @return A CellStyle object configured for the header row.
     * @author Duy Nam
     */
    public static CellStyle createStyleForHeader(Sheet sheet, String fontName) {
        // Create a new font and set its properties
        Font font = sheet.getWorkbook().createFont();
        font.setFontName(fontName); // Set the font name
        font.setBold(true); // Make the font bold
        font.setFontHeightInPoints((short) 10); // Set the font size to 10 points
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create a new cell style and set its font
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font); // Apply the font to the cell style
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // Return the configured cell style
        return cellStyle;
    }

    /**
     * Automatically adjusts the width of columns to fit the content.
     * <p>
     * This method iterates through all columns in the specified row and adjusts the width
     * of each column to fit the content of the cells in that column. It ensures that the content
     * is visible and properly formatted by resizing each column based on its contents.
     * <p>
     * Note: This method should be called after all relevant data has been written to the row.
     *
     * @param sheet    The sheet that requires column width adjustment.
     * @param rowIndex The index of the row to base the column width adjustment on.
     * @author Duy Nam
     */
    public static void autosizeColumns(Sheet sheet, int rowIndex) {
        Row row = sheet.getRow(rowIndex);
        if (row != null) {
            int numberOfColumns = row.getPhysicalNumberOfCells();
            for (int columnIndex = 0; columnIndex < numberOfColumns; columnIndex++) {
                sheet.autoSizeColumn(columnIndex);
            }
        }
    }

    /**
     * Save and close a session, sending the workbook as HTTP response data.
     *
     * @param workbook            The workbook to send.
     * @param httpServletResponse The HttpServletResponse used to send the workbook.
     * @author Duy Nam
     */
    public static void exportWorkbookToHttpResponse(Workbook workbook, HttpServletResponse httpServletResponse) throws IOException {
        try (ServletOutputStream outputStream = httpServletResponse.getOutputStream()) {
            workbook.write(outputStream);
        }
    }

    /**
     * Writes headers and data to a specified sheet starting from a given row index.
     * <p>
     * This method first writes the headers to the specified row, then writes the data
     * to the next row in the sheet. The row index is incremented accordingly to keep track
     * of the current row position.
     * <p>
     * Note: Ensure that the `writeHeader` and `writeRowData` methods are correctly implemented
     * to handle the writing operations.
     *
     * @param sheet    The sheet where headers and data will be written.
     * @param headers  An array of strings representing the header titles.
     * @param data     A list of objects representing the data to be written.
     * @param rowIndex The starting row index for writing headers and data.
     * @return The updated row index after writing the headers and data.
     * @author Duy Nam
     */
    public static int writeData(Sheet sheet, String[] headers, List<Object> data, int rowIndex) {
        // Write headers
        // This method call writes the headers to the sheet at the specified row index
        writeHeader(sheet, headers, ExcelUtil.FONT_NAME, rowIndex);
        // Increment the row index to move to the next row after writing the headers
        rowIndex++;

        // Write data
        // Create a new row in the sheet at the current row index
        Row row = sheet.createRow(rowIndex);
        // This method call writes the data to the created row using the headers for column mapping
        writeRowData(row, headers, data);
        // Increment the row index to move to the next row after writing the data
        rowIndex++;

        // Return the updated row index
        return rowIndex;
    }


    /**
     * Writes data to a specified row in the sheet based on the provided headers and data.
     * <p>
     * This method iterates over the headers and writes corresponding data to the cells in the row.
     * It supports various data types and handles null values appropriately by setting an empty string.
     * <p>
     * Note: The data list should align with the headers in terms of the order and number of elements.
     *
     * @param row     The row to which the data will be written.
     * @param headers An array of strings representing the header titles.
     * @param data    A list of objects representing the data to be written.
     * @author Duy Nam
     */
    public static void writeRowData(Row row, String[] headers, List<Object> data) {
        // Iterate over the range of headers and create cells for each header position
        IntStream.range(0, headers.length).forEach(i -> {
            // Create a new cell in the row at the current index
            Cell cell = row.createCell(i);
            // Get the data value corresponding to the current index
            Object value = data.get(i);
            // Check if the value is not null
            if (value != null) {
                // Determine the type of the value and set the cell value accordingly
                switch (value.getClass().getSimpleName()) {
                    case "String":
                        cell.setCellValue((String) value);
                        break;
                    case "Integer":
                        cell.setCellValue((Integer) value);
                        break;
                    case "Boolean":
                        cell.setCellValue((Boolean) value);
                        break;
                    case "Double":
                        cell.setCellValue((Double) value);
                        break;
                    case "Date":
                        cell.setCellValue((Date) value);
                        break;
                    case "LocalDate":
                        cell.setCellValue(((LocalDate) value).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                        break;
                    case "LocalDateTime":
                        cell.setCellValue(((LocalDateTime) value).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                        break;
                    case "Long":
                        cell.setCellValue((Long) value);
                        break;
                    case "Float":
                        cell.setCellValue((Float) value);
                        break;
                    case "BigDecimal":
                        cell.setCellValue(((BigDecimal) value).doubleValue());
                        break;
                    case "UUID":
                        cell.setCellValue(value.toString());
                        break;
                    default:
                        cell.setCellValue(String.valueOf(value));
                        break;
                }
            } else {
                // Set an empty string if the value is null
                cell.setCellValue("");
            }
        });
    }

    /**
     * Creates an output Excel file from the given workbook.
     * <p>
     * This method writes the contents of the provided workbook to an Excel file at the specified file path.
     * It uses a try-with-resources statement to ensure that the output stream is properly closed after writing.
     *
     * @param workbook      The Workbook object containing the data to be written to the file.
     * @param excelFilePath The file path where the Excel file will be created.
     * @author Duy Nam
     */
    public static void createOutputFile(Workbook workbook, String excelFilePath) throws IOException {
        try (OutputStream os = new FileOutputStream(excelFilePath)) {
            workbook.write(os);
        }
    }
}
