package com.example.graduationthesis.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class BaseResponsesUtil extends BaseResponseUtil {

    private PageableResponseUtil meta;
}
