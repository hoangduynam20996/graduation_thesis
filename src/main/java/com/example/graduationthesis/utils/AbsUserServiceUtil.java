package com.example.graduationthesis.utils;

import com.example.graduationthesis.user.entity.User;
import com.ibm.icu.text.Normalizer;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbsUserServiceUtil {

    public User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public String removeAccents(String source) {
        // Chuẩn hóa chuỗi với NFD
        String normalized = Normalizer.normalize(source, Normalizer.NFD);
        // Xóa các ký tự điều khiển và dấu, chỉ giữ lại các ký tự đơn
        return normalized.replaceAll("\\p{M}", "");
    }
}
