package com.example.graduationthesis.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public abstract class BaseResponseUtil {

    private String code;
    private Integer status;
    private String message;
    private Long responseTime;
    private Object data;
}
