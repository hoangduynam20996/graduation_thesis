package com.example.graduationthesis.room.entity;

import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.roomtype.entity.RoomType;
import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_room")
public class Room extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @ManyToOne
    @JoinColumn(name = "room_type_id")
    private RoomType roomType;
    @Column
    private String roomNumber;
    @Column(precision = 10, scale = 2)
    private BigDecimal pricePerNight;
    @Column
    private String roomNameCustom;
    @Column
    private Integer quantityRoom;
    @Column
    private Double roomArea;
    @Column
    private Integer maxOccupancy;
    @Column
    private String roomName;
    @Column
    private String bedName;
    @Column
    private LocalDate saleDate;
    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private List<BookingDetail> bookingDetails;
    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private List<ServiceAndAmenityRoom> serviceAndAmenityRooms;
}
