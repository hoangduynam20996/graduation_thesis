package com.example.graduationthesis.room.dto;

import java.util.UUID;

public record RoomDTO(
        UUID id,
        String roomTypeName,
        String roomNumber,
        String status,
        String roomNameCustom,
        Integer quantityRoom,
        Double roomArea,
        Integer maxOccupancy,
        String roomName,
        String bedName
) {
}
