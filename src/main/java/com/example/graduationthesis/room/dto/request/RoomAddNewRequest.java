package com.example.graduationthesis.room.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class RoomAddNewRequest {

    @NotNull
    private UUID roomTypeId;
    @NotNull
    private BigDecimal pricePerNight;
    @NotBlank
    private String roomName;
    private String roomNameCustom;
    @NotNull
    private Integer quantityRoom;
    private Double roomArea;
    @NotNull
    private Integer maxOccupancy;
    @NotNull
    private String bedName;
}
