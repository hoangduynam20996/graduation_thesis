package com.example.graduationthesis.room.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class RoomUpdateRequest {

    private UUID roomTypeId;

    private BigDecimal pricePerNight;

    private String roomName;

    private String roomNameCustom;

    private Integer quantityRoom;

    private Double roomArea;

    private Integer maxOccupancy;

    private String bedName;
}
