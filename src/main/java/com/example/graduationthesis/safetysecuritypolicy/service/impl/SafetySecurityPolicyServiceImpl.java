package com.example.graduationthesis.safetysecuritypolicy.service.impl;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.safetysecuritypolicy.dto.SafetySecurityPolicyDTO;
import com.example.graduationthesis.safetysecuritypolicy.dto.request.SafetySecurityPolicyRequest;
import com.example.graduationthesis.safetysecuritypolicy.dto.response.SafetySecurityPolicyResponse;
import com.example.graduationthesis.safetysecuritypolicy.dto.response.SafetySecurityPolicyResponses;
import com.example.graduationthesis.safetysecuritypolicy.entity.SafetySecurityPolicy;
import com.example.graduationthesis.safetysecuritypolicy.mapper.SafetySecurityPolicyDTOMapper;
import com.example.graduationthesis.safetysecuritypolicy.repo.SafetySecurityPolicyRepo;
import com.example.graduationthesis.safetysecuritypolicy.service.SafetySecurityPolicyService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class SafetySecurityPolicyServiceImpl implements SafetySecurityPolicyService {

    private final SafetySecurityPolicyRepo safetySecurityPolicyRepo;

    private final SafetySecurityPolicyDTOMapper safetySecurityPolicyDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    @Override
    public SafetySecurityPolicyResponse addSafetySecurityPolicy(SafetySecurityPolicyRequest request) {
        if (safetySecurityPolicyRepo.existsByName(request.getName()))
            throw new ApiRequestException("add fail name " + request.getName() + " already!.", "");

        SafetySecurityPolicy safetySecurityPolicy = safetySecurityPolicyRepo.save(
                SafetySecurityPolicy.builder()
                        .name(request.getName())
                        .content(request.getContent())
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        return SafetySecurityPolicyResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(safetySecurityPolicyDTOMapper.apply(safetySecurityPolicy))
                .message("")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public SafetySecurityPolicyResponse updateSafetySecurityPolicy(SafetySecurityPolicyRequest request, UUID sspid) {
        SafetySecurityPolicy safetySecurityPolicy = getSafetySecurityPolicy(sspid);

        if (!safetySecurityPolicy.getName().equals(request.getName()))
            safetySecurityPolicy.setName(request.getName());

        if (!safetySecurityPolicy.getContent().equals(request.getContent()))
            safetySecurityPolicy.setContent(request.getContent());

        SafetySecurityPolicy save = safetySecurityPolicyRepo.save(safetySecurityPolicy);

        return SafetySecurityPolicyResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(safetySecurityPolicyDTOMapper.apply(save))
                .message("")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public SafetySecurityPolicyResponse deleteSafetySecurityPolicy(UUID sspid) {
        SafetySecurityPolicy safetySecurityPolicy = getSafetySecurityPolicy(sspid);
        safetySecurityPolicy.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        safetySecurityPolicy.setIsDeleted(SystemConstant.NO_ACTIVE);
        SafetySecurityPolicy save = safetySecurityPolicyRepo.save(safetySecurityPolicy);

        return SafetySecurityPolicyResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(safetySecurityPolicyDTOMapper.apply(save))
                .message("")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public SafetySecurityPolicyResponses findAll(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<SafetySecurityPolicy> all = safetySecurityPolicyRepo.findAll(pageable);

        List<SafetySecurityPolicyDTO> safetySecurityPolicys = all.stream()
                .map(safetySecurityPolicyDTOMapper)
                .toList();
        return SafetySecurityPolicyResponses.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(safetySecurityPolicys)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private SafetySecurityPolicy getSafetySecurityPolicy(UUID sspid) {
        return safetySecurityPolicyRepo.findById(sspid)
                .orElseThrow(() -> new ApiRequestException("find safety security policy by id " + sspid + " not found!.", ""));
    }
}
