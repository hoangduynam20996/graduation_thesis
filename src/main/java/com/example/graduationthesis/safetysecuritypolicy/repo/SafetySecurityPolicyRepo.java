package com.example.graduationthesis.safetysecuritypolicy.repo;

import com.example.graduationthesis.safetysecuritypolicy.entity.SafetySecurityPolicy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SafetySecurityPolicyRepo extends JpaRepository<SafetySecurityPolicy, UUID> {

    boolean existsByName(String name);
}
