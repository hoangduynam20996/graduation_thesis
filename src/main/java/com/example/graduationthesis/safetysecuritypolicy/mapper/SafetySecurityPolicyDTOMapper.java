package com.example.graduationthesis.safetysecuritypolicy.mapper;

import com.example.graduationthesis.safetysecuritypolicy.dto.SafetySecurityPolicyDTO;
import com.example.graduationthesis.safetysecuritypolicy.entity.SafetySecurityPolicy;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class SafetySecurityPolicyDTOMapper implements
        Function<SafetySecurityPolicy, SafetySecurityPolicyDTO> {

    private final ModelMapper mapper;

    @Override
    public SafetySecurityPolicyDTO apply(SafetySecurityPolicy safetySecurityPolicy) {
        return mapper.map(safetySecurityPolicy, SafetySecurityPolicyDTO.class);
    }
}
