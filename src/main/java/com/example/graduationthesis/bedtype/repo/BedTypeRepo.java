package com.example.graduationthesis.bedtype.repo;

import com.example.graduationthesis.bedtype.entity.BedType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BedTypeRepo extends JpaRepository<BedType, UUID> {

    boolean existsByName(String name);

    Page<BedType> findBedTypesByStatus(String status, Pageable pageable);
}
