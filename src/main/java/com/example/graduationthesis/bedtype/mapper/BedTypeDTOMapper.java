package com.example.graduationthesis.bedtype.mapper;

import com.example.graduationthesis.bedtype.dto.BedTypeDTO;
import com.example.graduationthesis.bedtype.entity.BedType;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class BedTypeDTOMapper implements Function<BedType, BedTypeDTO> {

    private final ModelMapper mapper;

    @Override
    public BedTypeDTO apply(BedType bedType) {
        return mapper.map(bedType, BedTypeDTO.class);
    }
}
