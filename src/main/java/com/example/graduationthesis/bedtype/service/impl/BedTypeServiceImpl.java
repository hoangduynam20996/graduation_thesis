package com.example.graduationthesis.bedtype.service.impl;

import com.example.graduationthesis.bedtype.dto.BedTypeDTO;
import com.example.graduationthesis.bedtype.dto.request.BedTypeAddRequest;
import com.example.graduationthesis.bedtype.dto.response.BedTypeResponse;
import com.example.graduationthesis.bedtype.dto.response.BedTypeResponses;
import com.example.graduationthesis.bedtype.entity.BedType;
import com.example.graduationthesis.bedtype.mapper.BedTypeDTOMapper;
import com.example.graduationthesis.bedtype.repo.BedTypeRepo;
import com.example.graduationthesis.bedtype.service.BedTypeService;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class BedTypeServiceImpl implements BedTypeService {

    private final BedTypeRepo bedTypeRepo;

    private final BedTypeDTOMapper bedTypeDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    @Override
    public BedTypeResponse addBedType(BedTypeAddRequest request) {
        if (bedTypeRepo.existsByName(request.getName()))
            throw new ApiRequestException("add fail bed type name " + request.getName() + " already exist!.", "");

        BedType save = bedTypeRepo.save(
                BedType.builder()
                        .name(request.getName().trim())
                        .description(request.getDescription().trim())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        return BedTypeResponse.builder()
                .code("BT_002")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bedTypeDTOMapper.apply(save))
                .message("Add bed type success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BedTypeResponse updateBedType(BedTypeAddRequest request, UUID btid) {
        BedType bedType = getBedType(btid);

        if (!bedType.getName().equals(request.getName()))
            bedType.setName(request.getName());

        if (bedType.getDescription() == null || !bedType.getDescription().equals(request.getDescription()))
            bedType.setDescription(request.getDescription());

        BedType save = bedTypeRepo.save(bedType);

        return BedTypeResponse.builder()
                .code("BT_003")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bedTypeDTOMapper.apply(save))
                .message("Update bed type success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BedTypeResponse deleteBedType(UUID btid) {
        BedType bedType = getBedType(btid);
        bedType.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        BedType save = bedTypeRepo.save(bedType);

        return BedTypeResponse.builder()
                .code("BT_004")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bedTypeDTOMapper.apply(save))
                .message("Delete bed type success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public BedTypeResponses findAll(int currentPage, int limitPage, String status) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<BedType> all = bedTypeRepo.findBedTypesByStatus(status, pageable);

        List<BedTypeDTO> bedTypeDTOS = all.stream()
                .map(bedTypeDTOMapper)
                .toList();

        return BedTypeResponses.builder()
                .code("BT_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(bedTypeDTOS)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("Get bed types success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private BedType getBedType(UUID btid) {
        return bedTypeRepo.findById(btid)
                .orElseThrow(() -> new ApiRequestException("find bed type by id " + btid + " not found!.", ""));
    }
}
