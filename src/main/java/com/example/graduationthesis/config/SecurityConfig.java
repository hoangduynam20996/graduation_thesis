package com.example.graduationthesis.config;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.constant.UserConstant;
import com.example.graduationthesis.jwt.CustomBasicAuthenticationEntryPoint;
import com.example.graduationthesis.jwt.JwtAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final LogoutHandler logoutHandler;

    private final AuthenticationProvider authenticationProvider;

    private final HandlerExceptionResolver handlerExceptionResolver;

    private final CustomBasicAuthenticationEntryPoint customBasicAuthenticationEntryPoint;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter(handlerExceptionResolver);
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .authenticationEntryPoint(customBasicAuthenticationEntryPoint)
                .and()
                .cors()
                .and()
                .authorizeHttpRequests(authConfig -> {
                    authConfig.requestMatchers(SystemConstant.API_PUBLIC + "/**").permitAll();
                    authConfig.requestMatchers(
                            "/api-docs/**",
                            "/v2/api-docs",
                            "/v3/api-docs",
                            "/v3/api-docs/**",
                            "/swagger-resources/**",
                            "/configuration/ui",
                            "/configuration/security",
                            "/swagger-ui.html",
                            "/webjars/**",
                            "/swagger-ui.html/**",
                            "/swagger-ui/**"
                    ).permitAll();
                    authConfig.anyRequest().authenticated();
                })
                .csrf(AbstractHttpConfigurer::disable)
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .logout()
                .logoutUrl(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + UserConstant.API_USER + "/logout")
                .addLogoutHandler(logoutHandler)
                .logoutSuccessHandler((request, response, authentication) -> SecurityContextHolder.clearContext())
                .and()
                .oauth2Login()
                .defaultSuccessUrl("/api/v1/auth/oauth2-success")
                .failureUrl("/api/v1/auth/oath2-fail")
                .authorizationEndpoint()
                .baseUri("/api/v1/auth/oauth2/authorization");

        return http.build();
    }
}
