package com.example.graduationthesis.hotelimage.dto.request;

import com.example.graduationthesis.hotelimage.dto.request.HotelImageIdRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelImageRequest {

    private UUID hotelId;

    private List<HotelImageIdRequest> hotelImageIds;

}
