package com.example.graduationthesis.hotelimage.repo;

import com.example.graduationthesis.hotelimage.entity.HotelImage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HotelImageRepo extends JpaRepository<HotelImage, UUID> {
    @Query(value = """
                SELECT o.urlImage FROM HotelImage o WHERE o.hotel.id = ?1
            """)
    List<String> findHotelImageByHotelId(UUID hotelId);

    Optional<List<HotelImage>> findHotelImagesByHotelId(UUID hotelId, Pageable pageable);

}
