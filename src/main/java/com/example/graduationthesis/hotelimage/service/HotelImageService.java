package com.example.graduationthesis.hotelimage.service;

import com.example.graduationthesis.hotelimage.dto.request.HotelImageRequest;
import com.example.graduationthesis.hotelimage.dto.response.HotelImageResponse;
import com.example.graduationthesis.hotelimage.dto.response.HotelImageResponses;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface HotelImageService {

    HotelImageResponse addHotelImage(String jwtToken, MultipartFile[] multipartFiles);

    void addHotelImageMore(UUID hid, MultipartFile[] multipartFiles);

    void updateHotelImage(HotelImageRequest request);

    void deleteHotelImage(UUID hiid);

    HotelImageResponses findHotelImagesByHotelId(UUID hotelId, Integer currentPage, Integer limitPage);
}
