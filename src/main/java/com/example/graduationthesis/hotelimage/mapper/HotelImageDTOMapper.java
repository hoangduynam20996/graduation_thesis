package com.example.graduationthesis.hotelimage.mapper;

import com.example.graduationthesis.hotelimage.dto.HotelImageDTO;
import com.example.graduationthesis.hotelimage.entity.HotelImage;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class HotelImageDTOMapper
        implements Function<HotelImage, HotelImageDTO> {

    private final ModelMapper mapper;

    @Override
    public HotelImageDTO apply(HotelImage hotelImage) {
        return mapper.map(hotelImage, HotelImageDTO.class);
    }
}
