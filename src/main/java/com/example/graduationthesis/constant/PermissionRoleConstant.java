package com.example.graduationthesis.constant;

public class PermissionRoleConstant {

    public static final String API_PERMISSION_ROLE = "/permission-role";
    public static final String API_PERMISSION_ROLE_ID = "/{permissionRoleId}";
    public static final String PATH_PERMISSION_ROLE_ID = "permissionRoleId";

}
