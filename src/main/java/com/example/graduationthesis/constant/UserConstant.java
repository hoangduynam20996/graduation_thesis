package com.example.graduationthesis.constant;

public class UserConstant {

    public static final String API_USER = "/user";
    public static final String API_OAUTH2_SUCCESS = "/oauth2-success";
    public static final String API_OAUTH2_FAIL = "/oauth2-fail";
    public static final String API_LOGIN = "/login";
    public static final String API_REGISTER = "/register";
    public static final String URL_CONFIRM = "/public/v1/user/confirm?token=";
    public static final String API_PROFILE = "/profile";
    public static final String API_CONFIRM = "/confirm";
    public static final String API_FORGOT_PASSWORD = "/forgot-password";
    public static final String API_LOGOUT = "/logout";
    public static final String API_PERMISSIONS = "/permissions";

    public static final String API_USER_ID = "/{userId}";
    public static final String PATH_USER_ID = "userId";
}
