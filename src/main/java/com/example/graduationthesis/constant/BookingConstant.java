package com.example.graduationthesis.constant;

public class BookingConstant {

    public static final String API_BOOKING = "/booking";
    public static final String API_GET_STATUS = "/get-status";
    public static final String STATUS_BOOKING_NOT_PAID = "NOT_PAID";
    public static final String SESSION_BOOKING = "/session";
    public static final String STATUS_BOOKING_SET_PLACE = "STATUS_BOOKING_SET_PLACE";
    public static final String STATUS_BOOKING_PENDING = "STATUS_BOOKING_PENDING";
    public static final String STATUS_BOOKING_SUCCESS = "STATUS_BOOKING_SUCCESS";
    public static final String STATUS_BOOKING_CANCEL = "STATUS_BOOKING_CANCEL";
}
