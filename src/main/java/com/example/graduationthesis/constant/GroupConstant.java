package com.example.graduationthesis.constant;

public class GroupConstant {
    public static final String API_GROUP = "/group";
    public static final String PATH_GROUP_ID = "groupId";
    public static final String API_GROUP_ID = "/{groupId}";
    public static final String API_ADD_USER_FOR_GROUP = "/user";
    public static final String API_UPDATE_PERMISSIONS_FOR_GROUP = "/permissions";

}
