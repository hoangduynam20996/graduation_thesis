package com.example.graduationthesis.constant;

public class PolicyConfirmationConstant {

    public static final String API_POLICY_CONFIRMATION = "/policy-confirmation";
}
