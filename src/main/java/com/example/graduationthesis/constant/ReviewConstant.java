package com.example.graduationthesis.constant;

public class ReviewConstant {

    public static final String API_REVIEW = "/review";
    public static final String API_GET_BY_HOTEL = "/get-by-hotel";
}
