package com.example.graduationthesis.constant;

public class PaymentConstant {

    public static final String API_PAYMENT = "/payment";
    public static final String API_CREATE = "/create";
    public static final String API_CHECK_VN_PAY = "/check-vn-pay";
    public static final String API_ADD_DIRECT_PAYMENT = "/add-direct-payment";
    public static final String API_STATISTIC = "/statistic";
    public static final String API_STATISTIC_DAY = "/statistic-day";
    public static final String API_STATISTIC_YEAR = "/statistic-year";
    public static final String STATUS_PAYMENT_PENDING = "STATUS_PAYMENT_PENDING";
    public static final String STATUS_PAYMENT_PAID = "STATUS_PAYMENT_PAID";
    public static final String STATUS_PAYMENT_UN_PAID = "STATUS_PAYMENT_UN_PAID";
}
