package com.example.graduationthesis.exception;

import lombok.Getter;

@Getter
public class ApiRequestException extends RuntimeException {

    private final String code;
    private final String message;

    public ApiRequestException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
