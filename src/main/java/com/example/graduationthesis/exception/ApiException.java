package com.example.graduationthesis.exception;

public record ApiException(
        String code,
        Integer status,
        String message,
        long responseTime
) {
}
