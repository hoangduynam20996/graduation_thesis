package com.example.graduationthesis.exception;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ApiExceptionHandle {

    private final BaseAmenityUtil baseAmenity;

    private String extractErrorCode(String fullErrorMessage) {
        int startBracketIndex = fullErrorMessage.indexOf("Detail:");
        int endBracketIndex = fullErrorMessage.indexOf(']');

        if (startBracketIndex != -1 && endBracketIndex != -1 && startBracketIndex < endBracketIndex) {
            return fullErrorMessage.substring(startBracketIndex + 1, endBracketIndex).trim();
        }

        return fullErrorMessage;
    }

    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleException(ApiRequestException e) {
        log.error(e.getMessage());
        ApiException exception = new ApiException(
                e.getCode(),
                SystemConstant.STATUS_BAD_REQUEST,
                e.getMessage(),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, BAD_REQUEST);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<Object> handleBindException(BindException e) {
        log.error(e.getMessage());
        List<String> listError = new ArrayList<>();

        if (e.getBindingResult().hasErrors()) {
            listError = e.getBindingResult().getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .map(baseAmenity::getMessageBundle)
                    .toList();
        }
        ApiException exception = new ApiException(
                ResourceConstant.ERROR_VALIDATION_FIELD,
                SystemConstant.STATUS_BAD_REQUEST,
                listError.toString(),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, BAD_REQUEST);
    }

    /**
     * Handle exception when validate data
     *
     * @param e
     * @param request
     * @return errorResponse
     */
    @ExceptionHandler({
            ConstraintViolationException.class,
            MissingServletRequestParameterException.class,
            MethodArgumentNotValidException.class,
            MethodArgumentTypeMismatchException.class
    })
    @ResponseStatus(BAD_REQUEST)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Bad Request",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            examples = @ExampleObject(
                                    name = "Handle exception when the data invalid. (@RequestBody, @RequestParam, @PathVariable)",
                                    summary = "Handle Bad Request",
                                    value = """
                                            {
                                                 "timestamp": "2024-04-07T11:38:56.368+00:00",
                                                 "status": 400,
                                                 "path": "/api/v1/...",
                                                 "error": "Invalid Payload",
                                                 "message": "{data} must be not blank"
                                             }
                                            """
                            ))})
    })
    public ErrorResponse handleValidationException(Exception e, WebRequest request) {
        log.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode("This wrong parameter!");
        errorResponse.setStatus(BAD_REQUEST.value());
        errorResponse.setResponseTime(baseAmenity.responseTime());
        errorResponse.setPath(request.getDescription(false).replace("uri=", ""));

        String message = e.getMessage();
        if (e instanceof MethodArgumentNotValidException) {
            int start = message.lastIndexOf("[") + 1;
            int end = message.lastIndexOf("]") - 1;
            message = message.substring(start, end);
            errorResponse.setError("Invalid Payload");
            errorResponse.setMessage(message);
        } else if (e instanceof MissingServletRequestParameterException) {
            errorResponse.setError("Invalid Parameter");
            errorResponse.setMessage(message);
        } else if (e instanceof ConstraintViolationException) {
            errorResponse.setError("Invalid Parameter");
            errorResponse.setMessage(message.substring(message.indexOf(" ") + 1));
        } else if (e instanceof MethodArgumentTypeMismatchException) {
            errorResponse.setError("Invalid Parameter");
            errorResponse.setMessage(message.substring(message.indexOf(" ") + 1));
        } else {
            errorResponse.setError("Invalid Data");
            errorResponse.setMessage(message);
        }

        return errorResponse;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleException(DataIntegrityViolationException e) {
        log.error(e.getMessage());
        HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiException exception = new ApiException(
                baseAmenity.getMessageBundle(ResourceConstant.ERROR_SERVER),
                SystemConstant.STATUS_INTERNAL,
                extractErrorCode(e.getMessage()),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, internalServerError);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleJwtException(Exception e, WebRequest request) {
        log.error(e.getMessage());
        ErrorResponse errorResponse;
        HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
        HttpStatus forbidden = HttpStatus.FORBIDDEN;
        HttpStatus unauthorized = HttpStatus.UNAUTHORIZED;

        if (e instanceof BadCredentialsException) {
            errorResponse = ErrorResponse.builder()
                    .code(unauthorized.getReasonPhrase())
                    .status(HttpStatus.valueOf(401).value())
                    .path(request.getDescription(false).replace("uri=", ""))
                    .message(e.getMessage())
                    .error("Authentication Failure")
                    .responseTime(baseAmenity.responseTime())
                    .build();
            return new ResponseEntity<>(errorResponse, unauthorized);
        }

        if (e instanceof AccessDeniedException) {
            errorResponse = ErrorResponse.builder()
                    .code(unauthorized.getReasonPhrase())
                    .status(HttpStatus.valueOf(401).value())
                    .path(request.getDescription(false).replace("uri=", ""))
                    .message(e.getMessage())
                    .error("Not Authorized!")
                    .responseTime(baseAmenity.responseTime())
                    .build();
            return new ResponseEntity<>(errorResponse, unauthorized);
        }

        if (e instanceof ExpiredJwtException) {
            errorResponse = ErrorResponse.builder()
                    .code(forbidden.getReasonPhrase())
                    .status(HttpStatus.valueOf(403).value())
                    .path(request.getDescription(false).replace("uri=", ""))
                    .message(e.getMessage())
                    .error("JWT Token already expired !")
                    .responseTime(baseAmenity.responseTime())
                    .build();
            return new ResponseEntity<>(errorResponse, forbidden);
        }

        if (e instanceof SignatureException) {
            errorResponse = ErrorResponse.builder()
                    .code(forbidden.getReasonPhrase())
                    .status(HttpStatus.valueOf(403).value())
                    .path(request.getDescription(false).replace("uri=", ""))
                    .message(e.getMessage())
                    .error("JWT Signature not valid")
                    .responseTime(baseAmenity.responseTime())
                    .build();
            return new ResponseEntity<>(errorResponse, forbidden);
        }

        ErrorServerResponse serverResponse = ErrorServerResponse.builder().code("SV_001").message(internalServerError.getReasonPhrase()).build();
        return new ResponseEntity<>(serverResponse, internalServerError);
    }

}