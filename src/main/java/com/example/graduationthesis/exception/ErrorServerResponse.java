package com.example.graduationthesis.exception;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorServerResponse {

    private String code;

    private String message;
}
