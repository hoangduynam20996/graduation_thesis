package com.example.graduationthesis.hotelfavourite.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteResp;
import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteResponse;
import com.example.graduationthesis.hotelfavourite.entity.HotelFavourite;
import com.example.graduationthesis.hotelfavourite.mapper.HotelFavouriteDTOMapper;
import com.example.graduationthesis.hotelfavourite.repo.HotelFavouriteRepo;
import com.example.graduationthesis.hotelfavourite.service.HotelFavouriteService;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class HotelFavouriteServiceImpl extends AbsUserServiceUtil implements HotelFavouriteService {

    private final HotelRepo hotelRepo;

    private final HotelFavouriteRepo hotelFavouriteRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private final HotelFavouriteDTOMapper hotelFavouriteDTOMapper;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public HotelFavouriteResp addHotelFavourite(UUID hid) {
        User user = getUser();

        Hotel hotel = hotelRepo.findHotelByStatusAndId(SystemConstant.STATUS_ACTIVE, hid)
                .orElseThrow(() -> new ApiRequestException("find hotel by id " + hid + " not found!.", ""));

        if (existsByUserIdAndHotelId(user.getId(), hotel.getId()))
            throw new ApiRequestException("add favourite fail duplicate!.", "");

        HotelFavourite save = hotelFavouriteRepo.save(
                HotelFavourite.builder()
                        .user(user)
                        .hotel(hotel)
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );
        return HotelFavouriteResp.builder()
                .code(ResourceConstant.HT_FA_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelFavouriteDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_FA_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelFavouriteResponse deleteFavourite(UUID hotelId) {
        User user = getUser();
        HotelFavourite hotelFavourite = hotelFavouriteRepo.findHotelFavouriteByHotel_IdAndUser_Id(hotelId, user.getId())
                .orElseThrow(() -> new ApiRequestException("HTF_001", "find hotel favourite by id not found!."));
        hotelFavourite.setIsDeleted(SystemConstant.NO_ACTIVE);
        hotelFavourite.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        HotelFavourite save = hotelFavouriteRepo.save(hotelFavourite);
        return HotelFavouriteResponse.builder()
                .code(ResourceConstant.HT_FA_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelFavouriteDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_FA_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public boolean existsByUserIdAndHotelId(UUID uid, UUID hid) {
        return hotelFavouriteRepo.existsByUserIdAndHotelIdAndStatus(uid, hid, SystemConstant.STATUS_NO_ACTIVE);
    }
}
