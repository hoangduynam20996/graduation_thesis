package com.example.graduationthesis.hotelfavourite.service;

import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteResp;
import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteResponse;

import java.util.UUID;

public interface HotelFavouriteService {

    HotelFavouriteResp addHotelFavourite(UUID hid);

    HotelFavouriteResponse deleteFavourite(UUID hfid);

    boolean existsByUserIdAndHotelId(UUID uid, UUID hid);
}
