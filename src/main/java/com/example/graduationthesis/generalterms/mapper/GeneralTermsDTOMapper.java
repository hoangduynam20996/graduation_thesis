package com.example.graduationthesis.generalterms.mapper;

import com.example.graduationthesis.generalterms.dto.GeneralTermsDTO;
import com.example.graduationthesis.generalterms.entity.GeneralTerms;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class GeneralTermsDTOMapper implements
        Function<GeneralTerms, GeneralTermsDTO> {

    private final ModelMapper mapper;

    @Override
    public GeneralTermsDTO apply(GeneralTerms generalTerms) {
        return mapper.map(generalTerms, GeneralTermsDTO.class);
    }
}
