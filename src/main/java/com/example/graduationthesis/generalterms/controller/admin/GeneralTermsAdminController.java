package com.example.graduationthesis.generalterms.controller.admin;

import com.example.graduationthesis.constant.GeneralTermsConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.generalterms.dto.request.GeneralTermsRequest;
import com.example.graduationthesis.generalterms.dto.response.GeneralTermsResponse;
import com.example.graduationthesis.generalterms.dto.response.GeneralTermsResponses;
import com.example.graduationthesis.generalterms.service.GeneralTermsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + GeneralTermsConstant.API_GENERAL_TERMS)
@RequiredArgsConstructor
public class GeneralTermsAdminController {

    private final GeneralTermsService generalTermsService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping
    public ResponseEntity<GeneralTermsResponse> addGeneralTerms(
            @Validated @RequestBody GeneralTermsRequest request
    ) {
        generalTermsService.addGeneralTerms(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping
    public ResponseEntity<GeneralTermsResponse> updateGeneralTerms(
            @Validated @RequestBody GeneralTermsRequest request,
            @RequestParam("gtid") UUID gtid
    ) {
        generalTermsService.updateGeneralTerms(request, gtid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping
    public ResponseEntity<GeneralTermsResponse> deleteGeneralTerms(@RequestParam("gtid") UUID gtid) {
        generalTermsService.deleteGeneralTerms(gtid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @GetMapping
    public ResponseEntity<GeneralTermsResponses> findAllGeneralTerms(
            @RequestParam (SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam (SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage) {
        return new ResponseEntity<>(generalTermsService.findAll(currentPage.orElse(1),limitPage.orElse(8)), HttpStatus.OK);
    }

}
