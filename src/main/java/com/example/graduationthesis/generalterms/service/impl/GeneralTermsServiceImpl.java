package com.example.graduationthesis.generalterms.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.generalterms.dto.GeneralTermsDTO;
import com.example.graduationthesis.generalterms.dto.request.GeneralTermsRequest;
import com.example.graduationthesis.generalterms.dto.response.GeneralTermsResponse;
import com.example.graduationthesis.generalterms.dto.response.GeneralTermsResponses;
import com.example.graduationthesis.generalterms.entity.GeneralTerms;
import com.example.graduationthesis.generalterms.mapper.GeneralTermsDTOMapper;
import com.example.graduationthesis.generalterms.repo.GeneralTermsRepo;
import com.example.graduationthesis.generalterms.service.GeneralTermsService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class GeneralTermsServiceImpl implements GeneralTermsService {

    private final GeneralTermsRepo generalTermsRepo;

    private final GeneralTermsDTOMapper generalTermsDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public GeneralTermsResponse addGeneralTerms(GeneralTermsRequest request) {
        if (generalTermsRepo.existsByName(request.getName()))
            throw new ApiRequestException("add fail name general already!.", "");

        GeneralTerms save = generalTermsRepo.save(
                GeneralTerms.builder()
                        .name(request.getName().trim())
                        .content(request.getContent().trim())
                        .status(SystemConstant.STATUS_ACTIVE)
                        .isDeleted(SystemConstant.ACTIVE)
                        .build()
        );
        return GeneralTermsResponse.builder()
                .code(ResourceConstant.GT_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(generalTermsDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.GT_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }

    @Override
    public GeneralTermsResponse updateGeneralTerms(GeneralTermsRequest request, UUID gtid) {
        GeneralTerms generalTerms = getGeneralTerms(gtid);

        if (!generalTerms.getName().equals(request.getName()))
            generalTerms.setName(request.getName());

        if (!generalTerms.getContent().equals(request.getContent()))
            generalTerms.setContent(request.getContent());

        GeneralTerms save = generalTermsRepo.save(generalTerms);
        return GeneralTermsResponse.builder()
                .code(ResourceConstant.GT_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(generalTermsDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.GT_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GeneralTermsResponse deleteGeneralTerms(UUID gtid) {
        GeneralTerms generalTerms = getGeneralTerms(gtid);
        generalTerms.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        generalTerms.setIsDeleted(SystemConstant.NO_ACTIVE);
        GeneralTerms save = generalTermsRepo.save(generalTerms);
        return GeneralTermsResponse.builder()
                .code(ResourceConstant.GT_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(generalTermsDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.GT_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GeneralTermsResponses findAll(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<GeneralTerms> generalTerms = generalTermsRepo.findAll(pageable);
        List<GeneralTermsDTO> list = generalTerms
                .stream()
                .map(generalTermsDTOMapper)
                .toList();
        return GeneralTermsResponses.builder()
                .code(ResourceConstant.GT_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message(getMessageBundle(ResourceConstant.GT_004))
                .responseTime(baseAmenityUtil.responseTime())
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, generalTerms.getTotalPages()))
                .build();
    }

    private GeneralTerms getGeneralTerms(UUID gtid) {
        return generalTermsRepo.findById(gtid)
                .orElseThrow(() -> new ApiRequestException("find general terms by id " + gtid + " not found!.", ""));
    }
}
