package com.example.graduationthesis.generalterms.repo;

import com.example.graduationthesis.generalterms.entity.GeneralTerms;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GeneralTermsRepo extends JpaRepository<GeneralTerms, UUID> {

    boolean existsByName(String name);
}

