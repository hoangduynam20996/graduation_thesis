package com.example.graduationthesis.generalterms.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GeneralTermsRequest {

    private String name;

    private String content;
}
