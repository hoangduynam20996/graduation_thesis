package com.example.graduationthesis.reviewcategory.controller.member;

import com.example.graduationthesis.reviewcategory.repo.ReviewCategoryRepo;
import com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/review-category")
@RequiredArgsConstructor
public class ReviewCategoryController {

    private final ReviewCategoryRepo reviewCategoryRepo;

    @GetMapping("/test")
    public ResponseEntity<List<StatisticalRatingGroupByName>> tesst(@RequestParam("hid") UUID hid) {
        return new ResponseEntity<>(reviewCategoryRepo.statisticalRatingGroupNameByHotelId(hid).orElse(null), HttpStatus.OK);
    }
}
