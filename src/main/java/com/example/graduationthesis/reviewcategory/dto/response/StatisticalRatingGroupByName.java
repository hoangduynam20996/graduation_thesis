package com.example.graduationthesis.reviewcategory.dto.response;

public record StatisticalRatingGroupByName(
        String categoryName,
        Double totalRating,
        Long count,
        Double averageRating
) {
}
