package com.example.graduationthesis.reviewcategory.dto;

import java.util.UUID;

public record ReviewCategoryDTO(
        UUID reviewCategoryId,
        UUID reviewId,
        UUID categoryId
) {
}
