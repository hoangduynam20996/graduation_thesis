package com.example.graduationthesis.reservationdetails.dto.request;

import com.example.graduationthesis.booking.dto.request.BookingAddRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ReservationDetailAddRequest {

    private LocalDate bookingDate;

    private Integer status;

    private String firstName;

    private String lastName;

    private String email;

    private String country;

    private String phoneNumber;

    private Boolean bookingFor;

    private Boolean businessTravel;

    private String fullName;

    private Integer floorOptions;

    private Boolean electronicConfirmation;

    private Boolean pickupService;

    private Boolean carRental;

    private Boolean taxiBooking;

    private String specialRequirements;

    private String estimatedCheckInTime;

    private Boolean receivePromotionalEmails;

    private Boolean receivePromotionalEmailsTransport;

    private BookingAddRequest bookingAddRequest;

}
