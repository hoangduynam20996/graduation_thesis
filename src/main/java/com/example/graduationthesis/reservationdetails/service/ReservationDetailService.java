package com.example.graduationthesis.reservationdetails.service;

import com.example.graduationthesis.reservationdetails.dto.request.ReservationDetailAddRequest;

public interface ReservationDetailService {

    void addReservationDetail(ReservationDetailAddRequest request);
}
