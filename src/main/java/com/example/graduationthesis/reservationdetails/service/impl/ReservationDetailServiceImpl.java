package com.example.graduationthesis.reservationdetails.service.impl;

import com.example.graduationthesis.reservationdetails.dto.request.ReservationDetailAddRequest;
import com.example.graduationthesis.reservationdetails.repo.ReservationDetailRepo;
import com.example.graduationthesis.reservationdetails.service.ReservationDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ReservationDetailServiceImpl implements ReservationDetailService {

    private final ReservationDetailRepo reservationDetailRepo;

    @Override
    public void addReservationDetail(ReservationDetailAddRequest request) {

    }
}
