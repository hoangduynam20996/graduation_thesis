package com.example.graduationthesis.reservationdetails.controller.publicapi;

import com.example.graduationthesis.constant.RevervationDetaiConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.reservationdetails.service.ReservationDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + RevervationDetaiConstant.API_REVERVATION_DETAIL)
@RequiredArgsConstructor
public class ReservationDetailController {

    private final ReservationDetailService reservationDetailService;
}
