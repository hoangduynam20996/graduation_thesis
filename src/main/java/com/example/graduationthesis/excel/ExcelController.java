package com.example.graduationthesis.excel;

import com.example.graduationthesis.utils.ExcelUtil;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ExcelController {

    private final ExcelExportServiceImpl excelExportService;

    @GetMapping("/public/v1/excel")
    public ResponseEntity<?> exportExcel(HttpServletResponse response) {
        ExcelUtil.configExcelResponse("Demo", ExcelUtil.FILE_FORMAT_XLSX, response);
        excelExportService.exportExcel(response);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
