package com.example.graduationthesis.excel;

import com.example.graduationthesis.hotel.repo.HotelRepo;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class ExcelExport {

    private final HotelRepo hotelRepo;

    private static final Logger log = LoggerFactory.getLogger(ExcelExport.class);

    public static final int COLUMN_INDEX_ID = 0;
    public static final int COLUMN_INDEX_TITLE = 1;
    public static final int COLUMN_INDEX_PRICE = 2;
    public static final int COLUMN_INDEX_QUANTITY = 3;
    public static final int COLUMN_INDEX_TOTAL = 4;
    private static CellStyle cellStyleFormatNumber = null;

    public static void main(String[] args) {
        List<ExcelDemo> excelDemos = List.of(
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3"),
                new ExcelDemo(UUID.randomUUID(), "1", "2", "3")
        );

        writeExcel(excelDemos, "D:/TestUploadImage/demo.xlsx");
    }

    public static void writeExcel(List<ExcelDemo> excelDemos, String path) {
        try {
            Workbook workbook = getWorkbook(path);
            Sheet sheet = workbook.createSheet("Hotels");
            int rowIndex = 0;
            writeHeader(sheet, rowIndex);

            rowIndex++;
            for (ExcelDemo excelDemo : excelDemos) {
                Row row = sheet.createRow(rowIndex);
                writeHotel(excelDemo, row);
                rowIndex++;
            }

            // Write footer
            writeFooter(sheet, rowIndex);

            // Auto resize column witdth
            int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
            autosizeColumn(sheet, numberOfColumn);

            // Create file excel
            createOutputFile(workbook, path);
            System.out.println("Done!!!");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    // Create workbook
    private static Workbook getWorkbook(String excelFilePath) throws IOException {
        Workbook workbook;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    // Write header with format
    private static void writeHeader(Sheet sheet, int rowIndex) {
        // create CellStyle
        CellStyle cellStyle = createStyleForHeader(sheet);

        // Create row
        Row row = sheet.createRow(rowIndex);

        // Create cells
        Cell cell = row.createCell(COLUMN_INDEX_ID);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Id");

        cell = row.createCell(COLUMN_INDEX_TITLE);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Title");

        cell = row.createCell(COLUMN_INDEX_PRICE);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Price");

        cell = row.createCell(COLUMN_INDEX_QUANTITY);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Quantity");
    }

    // Write data
    private static void writeHotel(ExcelDemo excelDemo, Row row) {
        if (cellStyleFormatNumber == null) {
            // Format number
            short format = (short) BuiltinFormats.getBuiltinFormat("#,##0");
            // DataFormat df = workbook.createDataFormat();
            // short format = df.getFormat("#,##0");

            //Create CellStyle
            Workbook workbook = row.getSheet().getWorkbook();
            cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat(format);
        }

        Cell cell = row.createCell(COLUMN_INDEX_ID);
        cell.setCellValue(String.valueOf(excelDemo.getId()));

        cell = row.createCell(COLUMN_INDEX_TITLE);
        cell.setCellValue(excelDemo.getName());

        cell = row.createCell(COLUMN_INDEX_PRICE);
        cell.setCellValue(excelDemo.getEmail());
        cell.setCellStyle(cellStyleFormatNumber);

        cell = row.createCell(COLUMN_INDEX_QUANTITY);
        cell.setCellValue(excelDemo.getPhone());

    }

    // Create CellStyle for header
    private static CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }

    // Write footer
    private static void writeFooter(Sheet sheet, int rowIndex) {
        // Create row
        Row row = sheet.createRow(rowIndex);
        Cell cell = row.createCell(COLUMN_INDEX_TOTAL, CellType.FORMULA);
        cell.setCellFormula("SUM(E2:E6)");
    }

    // Auto resize column width
    private static void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    // Create output file
    private static void createOutputFile(Workbook workbook, String excelFilePath) throws IOException {
        try (OutputStream os = new FileOutputStream(excelFilePath)) {
            workbook.write(os);
        }
    }
}
