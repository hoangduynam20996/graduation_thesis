package com.example.graduationthesis.excel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExcelDemo {

    private UUID id;

    private String name;

    private String email;

    private String phone;
}
