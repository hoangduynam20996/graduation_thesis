package com.example.graduationthesis.excel;

import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import com.example.graduationthesis.utils.ExcelUtil;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ExcelExportServiceImpl {

    private final HotelRepo hotelRepo;

    private final String[] headers = {"Id", "isDeleted", "Status", "Create Date", "Create By",
            "Last Modify Date", "Last Modify By", "Activation Date", "City", "Contact", "Count View", "Description",
            "Hotel Name", "Phone Number", "Phone Number Two", "Postal Code", "Rating", "Street Address", "Hotel Payment Id",
            "Hotel Type Id", "Policy Id", "User Id"};

    private final String[] headerRooms = {"Id", "isDeleted", "Status", "Bed Name", "Room Name", "Max Occupancy"};

    private final String[] headerService = {"Service Room", "Id", "Service Name"};

    public void exportExcel(HttpServletResponse response) {
        try {
            List<Hotel> hotels = hotelRepo.findAll();
            writeExcel(hotels, ExcelUtil.FILE_FORMAT_XLSX, "Hotel Demo", response);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ApiRequestException("", "");
        }
    }

    public void writeExcel(List<Hotel> hotels, String fileFormat, String sheetName, HttpServletResponse response) {
        try (Workbook workbook = ExcelUtil.getWorkbook(fileFormat)) {
            Sheet sheet = workbook.createSheet(sheetName);
            int rowIndex = 0;

            // Create CellStyle for number formatting once
            CellStyle cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat((short) BuiltinFormats.getBuiltinFormat("#,##0"));

            for (Hotel hotel : hotels) {
                // Write hotel headers and data
                rowIndex = ExcelUtil.writeData(sheet, headers, hotelData(hotel), rowIndex);

                if (!hotel.getRooms().isEmpty()) {
                    for (Room room : hotel.getRooms()) {
                        // Write room headers and data
                        rowIndex = ExcelUtil.writeData(sheet, headerRooms, roomData(room), rowIndex);

                        List<ServiceAndAmenityRoom> serviceAndAmenityRooms = room.getServiceAndAmenityRooms();
                        if (!serviceAndAmenityRooms.isEmpty()) {
                            // Write service/amenity headers and data
                            for (ServiceAndAmenityRoom serviceAndAmenityRoom : serviceAndAmenityRooms) {
                                rowIndex = ExcelUtil.writeData(sheet, headerService, serviceData(serviceAndAmenityRoom), rowIndex);
                            }
                        }

                        // Add a blank row after each room
                        rowIndex++;
                    }
                }

                // Add a blank row after each hotel
                rowIndex++;
            }

            // Autosize columns after all data is written
            IntStream.range(0, headers.length).forEach(sheet::autoSizeColumn);

            // Export file
            ExcelUtil.exportWorkbookToHttpResponse(workbook, response);
        } catch (Exception e) {
            log.error("Error during Excel file creation: {}", e.getMessage());
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error during Excel file creation");
            } catch (IOException ioException) {
                log.error("Error sending error response: {}", ioException.getMessage());
            }
        }
    }

    private List<Object> hotelData(Hotel hotel) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return Arrays.asList(
                hotel.getId(),
                hotel.getIsDeleted(),
                hotel.getStatus(),
                hotel.getCreatedDate().format(formatter),
                hotel.getCreatedBy(),
                hotel.getLastModifiedDate().format(formatter),
                hotel.getLastModifiedBy(),
                hotel.getActivationDate(),
                hotel.getCity(),
                hotel.getContactPerson(),
                hotel.getCountView() != null ? hotel.getCountView() : 0,
                hotel.getDescription(),
                hotel.getName(),
                hotel.getPhoneNumber(),
                hotel.getPhoneNumberTwo(),
                hotel.getPostalCode(),
                hotel.getRating(),
                hotel.getStreetAddress(),
                hotel.getHotelPaymentMethod().getId(),
                hotel.getHotelType().getId(),
                hotel.getPolicyConfirmation().getId(),
                hotel.getUser().getId()
        );
    }

    private List<Object> roomData(Room room) {
        return Arrays.asList(
                room.getId(),
                room.getIsDeleted(),
                room.getStatus(),
                room.getBedName(),
                room.getRoomName(),
                room.getMaxOccupancy()
        );
    }

    private List<Object> serviceData(ServiceAndAmenityRoom service) {
        return Arrays.asList(
                "", // Placeholder for "Service Room"
                service.getId(),
                service.getServiceAndAmenity().getName()
        );
    }
}
