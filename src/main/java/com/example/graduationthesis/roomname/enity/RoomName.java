package com.example.graduationthesis.roomname.enity;

import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_room_name")
public class RoomName extends BaseEntityUtil {

    @Column
    private String name;
    @Column
    private String description;
}
