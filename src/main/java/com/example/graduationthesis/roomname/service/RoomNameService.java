package com.example.graduationthesis.roomname.service;

import com.example.graduationthesis.roomname.dto.request.RoomNameAddRequest;
import com.example.graduationthesis.roomname.dto.response.RoomNameResponse;
import com.example.graduationthesis.roomname.dto.response.RoomNameResponses;

import java.util.UUID;

public interface RoomNameService {

    RoomNameResponse addRoomName(RoomNameAddRequest request);

    RoomNameResponse updateRoomName(RoomNameAddRequest request, UUID ronid);

    RoomNameResponse deleteRoomName(UUID ronid);

    RoomNameResponses findAll(int currentPage, int limitPage);
}
