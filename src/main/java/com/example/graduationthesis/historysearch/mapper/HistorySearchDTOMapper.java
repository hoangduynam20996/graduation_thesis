package com.example.graduationthesis.historysearch.mapper;

import com.example.graduationthesis.historysearch.dto.HistorySearchDTO;
import com.example.graduationthesis.historysearch.entity.HistorySearch;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class HistorySearchDTOMapper implements
        Function<HistorySearch, HistorySearchDTO> {

    private final ModelMapper mapper;

    @Override
    public HistorySearchDTO apply(HistorySearch historySearch) {
        return mapper.map(historySearch, HistorySearchDTO.class);
    }
}
