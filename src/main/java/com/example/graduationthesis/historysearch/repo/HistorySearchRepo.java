package com.example.graduationthesis.historysearch.repo;

import com.example.graduationthesis.historysearch.entity.HistorySearch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface HistorySearchRepo extends JpaRepository<HistorySearch, UUID> {
}
