package com.example.graduationthesis.image;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Service
public class ImageService {
    private final String imagePath = getImagePath();

    public String getImagePath() {
        String path = "D:/TestUploadImage/";
        if (!path.endsWith(File.separator)) {
            path += File.separator;
        }
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            System.out.println("Operating System: Windows");
        } else if (os.contains("nix") || os.contains("nux") || os.contains("mac")) {
            System.out.println("Operating System: Linux or macOS");
            path = "/opt/upload/image/";
        } else {
            System.out.println("Operating System: Unknown");
            path = "/opt/upload/image/";
        }
        return path;
    }

    public String saveImage(MultipartFile file) {
        String filename = UUID.randomUUID() + getExtension(Objects.requireNonNull(file.getOriginalFilename()));
        try {
            String filePath = imagePath + filename;
            byte[] bytes = file.getBytes();
            Path path = Paths.get(filePath);
            Files.write(path, bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filename;
    }

    public byte[] loadImage(String filename) throws IOException {
        String filePath = imagePath + filename;
        Path path = Paths.get(filePath);
        return Files.readAllBytes(path);
    }

    private String getExtension(String filename) {
        return filename.substring(filename.lastIndexOf('.'));
    }

}
