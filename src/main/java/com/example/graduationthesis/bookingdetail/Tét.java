package com.example.graduationthesis.bookingdetail;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class Tét {

    public static void main(String[] args) {
//        LocalDate d1 = LocalDate.of(2023, 10, 1);
//        LocalDate d2 = LocalDate.now();
//
//        long totalDay = ChronoUnit.DAYS.between(d1, d2);
//        System.out.println(totalDay);
//
//        System.out.println(Objects.equals(5.0, 5.0));

        YearMonth yearMonth = YearMonth.of(2023, 2);
        System.out.println(yearMonth.lengthOfMonth());
        System.out.println(yearMonth.lengthOfYear());

        System.out.println(Long.toHexString(System.currentTimeMillis()));
    }
}
