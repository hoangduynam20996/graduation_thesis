package com.example.graduationthesis.bookingdetail.entity;

import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_booking_detail")
public class BookingDetail extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private Booking booking;
    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;
    @Column
    private Integer quantityRoomBooking;
    @Column
    private LocalDate checkInDate;
    @Column
    private LocalDate checkOutDate;
    @Column
    private BigDecimal totalPriceRoomOrigin;
    @Column
    private BigDecimal totalPriceRoom;
    @Column
    private Long totalDay;
    @Column
    private String bookingCode;
    @Column
    private String pinCode;
    @Column
    private Double discount;

}
