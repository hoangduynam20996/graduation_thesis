package com.example.graduationthesis.bookingdetail.service;

import com.example.graduationthesis.bookingdetail.dto.BookingDetailDTO;
import com.example.graduationthesis.bookingdetail.dto.response.BookingDetailResponses;

import java.util.List;
import java.util.UUID;

public interface BookingDetailService {

    BookingDetailResponses findAllByBookingId(UUID bid,Integer currentPage,Integer limitPage);
}
