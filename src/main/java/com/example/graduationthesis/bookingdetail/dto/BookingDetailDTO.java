package com.example.graduationthesis.bookingdetail.dto;

import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAmenityRoomResp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public record BookingDetailDTO(
        String roomName,
        Integer quantityRoom,
        LocalDate checkInDate,
        LocalDate checkOutDate,
        BigDecimal totalPriceRoom,
        BigDecimal totalPriceRoomOrigin,
        Long totalDay,
        String bookingCode,
        String pinCode,
        Double discount,
        String bedType,
        String hotelPhoneNumber,
        String emailHotelOwner,
        Integer quantityAdult,
        Integer quantityChild,
        List<ServiceAmenityRoomResp> serviceAmenityRooms
) {
}
