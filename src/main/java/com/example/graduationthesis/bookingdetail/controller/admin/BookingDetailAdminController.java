package com.example.graduationthesis.bookingdetail.controller.admin;

import com.example.graduationthesis.bookingdetail.dto.response.BookingDetailResponses;
import com.example.graduationthesis.bookingdetail.service.BookingDetailService;
import com.example.graduationthesis.constant.BookingDetailConstant;
import com.example.graduationthesis.constant.SystemConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + BookingDetailConstant.API_BOOKING_DETAIL)
@RequiredArgsConstructor
public class BookingDetailAdminController {

    private final BookingDetailService bookingDetailService;

    @GetMapping("/{bid}")
    public ResponseEntity<BookingDetailResponses> findBookingDetailsByBookingId(
            @PathVariable("bid") UUID bid,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(bookingDetailService.findAllByBookingId(bid, currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }
}
