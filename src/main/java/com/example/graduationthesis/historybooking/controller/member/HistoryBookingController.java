package com.example.graduationthesis.historybooking.controller.member;

import com.example.graduationthesis.constant.HistoryBookingConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.historybooking.dto.HistoryBookingDTO;
import com.example.graduationthesis.historybooking.dto.request.HistoryBookingRequest;
import com.example.graduationthesis.historybooking.service.HistoryBookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + HistoryBookingConstant.API_HISTORY_BOOKING)
@RequiredArgsConstructor
public class HistoryBookingController {

    private final HistoryBookingService historyBookingService;


    @PostMapping(HistoryBookingConstant.API_FIND)
    public ResponseEntity<HistoryBookingDTO> findHistoryBooking(
            @Validated @RequestBody HistoryBookingRequest request
    ) {
        return new ResponseEntity<>(historyBookingService.findHistory(request), HttpStatus.OK);
    }
}
