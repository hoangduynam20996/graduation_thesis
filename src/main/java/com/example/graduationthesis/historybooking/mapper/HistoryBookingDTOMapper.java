package com.example.graduationthesis.historybooking.mapper;

import com.example.graduationthesis.historybooking.dto.HistoryBookingDTO;
import com.example.graduationthesis.historybooking.entity.HistoryBooking;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class HistoryBookingDTOMapper implements
        Function<HistoryBooking, HistoryBookingDTO> {

    @Override
    public HistoryBookingDTO apply(HistoryBooking historyBooking) {
        return new HistoryBookingDTO(
                historyBooking.getTotalDay(),
                historyBooking.getFullName(),
                historyBooking.getBookingDate(),
                historyBooking.getRoomName(),
                historyBooking.getBedName(),
                historyBooking.getRoomTypeName()
        );
    }
}
