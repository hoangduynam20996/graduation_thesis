package com.example.graduationthesis.historybooking.repo;

import com.example.graduationthesis.historybooking.entity.HistoryBooking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HistoryBookingRepo extends JpaRepository<HistoryBooking, UUID> {
    Optional<HistoryBooking>
    findHistoryBookingByBookingCodeAndPinCodeAndHotelId(
            String bookingCode,
            String pinCode,
            UUID hotelId
    );

    Optional<List<HistoryBooking>> findHistoryBookingsByBookingDateBefore(LocalDate bookingDate);
}
