package com.example.graduationthesis.historybooking.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HistoryBookingRequest {

    private String bookingCode;

    private String pinCode;

    private UUID hotelId;
}
