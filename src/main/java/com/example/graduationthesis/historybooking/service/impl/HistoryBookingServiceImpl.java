package com.example.graduationthesis.historybooking.service.impl;

import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.historybooking.dto.HistoryBookingDTO;
import com.example.graduationthesis.historybooking.dto.request.HistoryBookingRequest;
import com.example.graduationthesis.historybooking.entity.HistoryBooking;
import com.example.graduationthesis.historybooking.mapper.HistoryBookingDTOMapper;
import com.example.graduationthesis.historybooking.repo.HistoryBookingRepo;
import com.example.graduationthesis.historybooking.service.HistoryBookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class HistoryBookingServiceImpl implements HistoryBookingService {

    private final HistoryBookingRepo historyBookingRepo;

    private final HistoryBookingDTOMapper historyBookingDTOMapper;

    @Override
    public HistoryBookingDTO findHistory(HistoryBookingRequest request) {
        HistoryBooking historyBooking = historyBookingRepo.findHistoryBookingByBookingCodeAndPinCodeAndHotelId(
                request.getBookingCode(),
                request.getPinCode(),
                request.getHotelId()
        ).orElseThrow(() -> new ApiRequestException("find history booking not found!.", ""));

        return historyBookingDTOMapper.apply(historyBooking);
    }
}
