package com.example.graduationthesis.roomtype.service.impl;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.roomtype.dto.RoomTypeDTO;
import com.example.graduationthesis.roomtype.dto.request.RoomTypeRequest;
import com.example.graduationthesis.roomtype.dto.response.RoomTypeResponse;
import com.example.graduationthesis.roomtype.dto.response.RoomTypeResponses;
import com.example.graduationthesis.roomtype.entity.RoomType;
import com.example.graduationthesis.roomtype.mapper.RoomTypeDTOMapper;
import com.example.graduationthesis.roomtype.repo.RoomTypeRepo;
import com.example.graduationthesis.roomtype.service.RoomTypeService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class RoomTypeServiceImpl implements RoomTypeService {

    private final RoomTypeRepo roomTypeRepo;

    private final RoomTypeDTOMapper roomTypeDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public RoomTypeResponse addRoomType(RoomTypeRequest request) {
        if (roomTypeRepo.existsByName(request.getName()))
            throw new ApiRequestException("room type name " + request.getName() + " already exist!.", "");

        RoomType roomType = roomTypeRepo.save(
                RoomType.builder()
                        .name(request.getName())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .description(request.getDescription())
                        .build()
        );

        return RoomTypeResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomTypeDTOMapper.apply(roomType))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomTypeResponse updateRoomType(RoomTypeRequest request, UUID rotid) {
        RoomType roomType = getRoomType(rotid);

        if (roomType.getName() == null || !roomType.getName().equals(request.getName())) {
            roomType.setName(request.getName());
        }

        if (roomType.getDescription() == null || !roomType.getDescription().equals(request.getDescription())) {
            roomType.setDescription(request.getDescription());
        }

        RoomType save = roomTypeRepo.save(roomType);

        return RoomTypeResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomTypeDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomTypeResponse deleteRoomType(UUID rotid) {
        RoomType roomType = getRoomType(rotid);
        roomType.setIsDeleted(SystemConstant.NO_ACTIVE);
        roomType.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        RoomType save = roomTypeRepo.save(roomType);

        return RoomTypeResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomTypeDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomTypeResponses findAllRoomType(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<RoomType> all = roomTypeRepo.findAll(pageable);

        List<RoomTypeDTO> roomTypes = all.stream()
                .map(roomTypeDTOMapper)
                .toList();
        return RoomTypeResponses.builder()
                .code("RT_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomTypes)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("Get rooms type success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private RoomType getRoomType(UUID rotid) {
        return roomTypeRepo.findById(rotid)
                .orElseThrow(() -> new ApiRequestException("", "room type find by id " + rotid + " not found!."));
    }
}
