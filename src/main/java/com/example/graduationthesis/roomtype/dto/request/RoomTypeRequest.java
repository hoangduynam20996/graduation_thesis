package com.example.graduationthesis.roomtype.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RoomTypeRequest {

    @NotNull(message = "Room type name not null")
    private String name;

    private String description;
}
