package com.example.graduationthesis.roomtype.mapper;

import com.example.graduationthesis.roomtype.entity.RoomType;
import com.example.graduationthesis.roomtype.dto.RoomTypeDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class RoomTypeDTOMapper implements Function<RoomType, RoomTypeDTO> {

    private final ModelMapper mapper;

    @Override
    public RoomTypeDTO apply(RoomType roomType) {
        return mapper.map(roomType, RoomTypeDTO.class);
    }
}
