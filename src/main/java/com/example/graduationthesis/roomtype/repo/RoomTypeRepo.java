package com.example.graduationthesis.roomtype.repo;

import com.example.graduationthesis.roomtype.entity.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoomTypeRepo extends JpaRepository<RoomType, UUID> {

    boolean existsByName(String name);

    Optional<RoomType> findRoomTypeByName(String name);

    Optional<List<RoomType>> findAllByStatus(Integer status);
}
