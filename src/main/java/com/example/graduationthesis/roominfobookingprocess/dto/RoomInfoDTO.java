package com.example.graduationthesis.roominfobookingprocess.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
public class RoomInfoDTO {

    private UUID roomId;

    private Integer quantityRoom;

    private String jwtToken;
}
