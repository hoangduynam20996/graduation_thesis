package com.example.graduationthesis.staffschedule;


import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_staff_schedule")
public class StaffSchedule extends BaseIDUtil {

    private UUID id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column
    private Date workDate;
    @Column
    private Date startDateTime;
    @Column
    private Date shiftEndTime;
}
