package com.example.graduationthesis.jwt;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.token.TokenRepo;
import com.example.graduationthesis.user.repo.UserRepo;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtService jwtService;
    @Autowired
    private TokenRepo tokenRepo;
    @Autowired
    private UserRepo userRepo;

    private final HandlerExceptionResolver handlerExceptionResolver;
    @Autowired
    public JwtAuthenticationFilter(HandlerExceptionResolver handlerExceptionResolver) {
        this.handlerExceptionResolver = handlerExceptionResolver;
    }

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain
    ) {
        final String authHeader = request.getHeader(SystemConstant.AUTHORIZATION);
        final String jwt;
        final String username;
        try {
            if (authHeader == null || !authHeader.startsWith(SystemConstant.BEARER)) {
                filterChain.doFilter(request, response);
                return;
            }
            jwt = authHeader.substring(7);
            username = jwtService.extractUsername(jwt);
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userRepo.findUserByEmailAndStatus(username, SystemConstant.STATUS_ACTIVE)
                        .orElseThrow(() -> new UsernameNotFoundException("find user by email " + username + " not found!."));
                var isValidToken = tokenRepo.findByToken(jwt)
                        .map(t -> !t.getExpired() && !t.getRevoked())
                        .orElse(false);
                if (jwtService.isValidToken(jwt, userDetails) && isValidToken) {
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                            userDetails,
                            null,
                            userDetails.getAuthorities()
                    );
                    authToken.setDetails(
                            new WebAuthenticationDetailsSource().buildDetails(request)
                    );
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            handlerExceptionResolver.resolveException(request, response, null, e);
        }
    }
}
