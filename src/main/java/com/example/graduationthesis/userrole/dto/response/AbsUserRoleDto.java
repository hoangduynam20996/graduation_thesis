package com.example.graduationthesis.userrole.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsUserRoleDto {

    private UUID userRoleId;

    private String roleName;

    private String email;

    private String fullName;

    private Boolean isDeleted;

    private String status;
}
