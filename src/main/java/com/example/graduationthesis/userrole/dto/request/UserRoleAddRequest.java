package com.example.graduationthesis.userrole.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserRoleAddRequest {

    private UUID roleId;

    private UUID userId;
}
