package com.example.graduationthesis.userrole.service.impl;


import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.role.entity.Role;
import com.example.graduationthesis.role.repo.RoleRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.repo.UserRepo;
import com.example.graduationthesis.userrole.dto.request.UserRoleAddRequest;
import com.example.graduationthesis.userrole.dto.request.UserRoleUpdateRequest;
import com.example.graduationthesis.userrole.dto.response.UserRoleDto;
import com.example.graduationthesis.userrole.dto.response.UserRoleResponse;
import com.example.graduationthesis.userrole.dto.response.UserRoleResponses;
import com.example.graduationthesis.userrole.entity.UserRole;
import com.example.graduationthesis.userrole.mapper.UserRoleDtoMapper;
import com.example.graduationthesis.userrole.repo.UserRoleRepo;
import com.example.graduationthesis.userrole.service.UserRoleService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepo userRoleRepo;

    private final RoleRepo roleRepo;

    private final UserRepo userRepo;

    private final UserRoleDtoMapper userRoleDtoMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    private User getUser(UUID userId) {
        return userRepo.findUserByIdAndIsDeleted(userId, SystemConstant.ACTIVE)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.USR_002, getMessageBundle(ResourceConstant.USR_002)));
    }

    private Role getRole(UUID roleId) {
        return roleRepo.findById(roleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.RL_001, getMessageBundle(ResourceConstant.RL_001)));
    }

    private UserRole getUserRole(UUID userRoleId) {
        return userRoleRepo.findById(userRoleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.USR_R_001, getMessageBundle(ResourceConstant.USR_R_001)));
    }

    @Override
    public UserRoleResponse addUserRole(UserRoleAddRequest request) {
        Role role = getRole(request.getRoleId());

        User user = getUser(request.getUserId());

        UserRole userRole = userRoleRepo.save(
                UserRole.builder()
                        .role(role)
                        .user(user)
                        .isDeleted(SystemConstant.ACTIVE)
                        .build()
        );

        return UserRoleResponse.builder()
                .code(ResourceConstant.USR_R_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userRoleDtoMapper.apply(userRole))
                .message(getMessageBundle(ResourceConstant.USR_R_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserRoleResponse updateUserRole(UserRoleUpdateRequest request) {
        UserRole userRole = getUserRole(request.getUserRoleId());

        Role role = getRole(request.getRoleId());
        User user = getUser(request.getUserId());

        userRole.setRole(role);
        userRole.setUser(user);

        userRoleRepo.save(userRole);
        return UserRoleResponse.builder()
                .code(ResourceConstant.USR_R_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userRoleDtoMapper.apply(userRole))
                .message(getMessageBundle(ResourceConstant.USR_R_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserRoleResponse deleteUserRole(UUID userRoleId) {
        UserRole userRole = getUserRole(userRoleId);

        userRole.setIsDeleted(SystemConstant.NO_ACTIVE);
        UserRole userRoleSave = userRoleRepo.save(userRole);

        return UserRoleResponse.builder()
                .code(ResourceConstant.USR_R_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userRoleDtoMapper.apply(userRoleSave))
                .message(getMessageBundle(ResourceConstant.USR_R_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserRoleResponses findAllUserRole(Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<UserRole> all = userRoleRepo.findAll(pageable);

        List<UserRoleDto> userRoleDtos = all.stream()
                .map(userRoleDtoMapper)
                .toList();

        return UserRoleResponses.builder()
                .code(ResourceConstant.USR_R_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userRoleDtos)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message(getMessageBundle(ResourceConstant.USR_R_004))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }
}
