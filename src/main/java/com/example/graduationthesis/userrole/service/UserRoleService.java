package com.example.graduationthesis.userrole.service;

import com.example.graduationthesis.userrole.dto.request.UserRoleAddRequest;
import com.example.graduationthesis.userrole.dto.request.UserRoleUpdateRequest;
import com.example.graduationthesis.userrole.dto.response.UserRoleResponse;
import com.example.graduationthesis.userrole.dto.response.UserRoleResponses;

import java.util.UUID;

public interface UserRoleService {

    UserRoleResponse addUserRole(UserRoleAddRequest request);

    UserRoleResponse updateUserRole(UserRoleUpdateRequest request);

    UserRoleResponse deleteUserRole(UUID userRoleId);

    UserRoleResponses findAllUserRole(Integer currentPage, Integer limitPage);
}
