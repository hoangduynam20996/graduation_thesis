package com.example.graduationthesis.user.entity;

import com.example.graduationthesis.billingaddress.BillingAddress;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotelfavourite.entity.HotelFavourite;
import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.staffschedule.StaffSchedule;
import com.example.graduationthesis.token.Token;
import com.example.graduationthesis.userrole.entity.UserRole;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PreRemove;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_user")
public class User extends BaseEntityUtil implements UserDetails {
    @Column
    private String email;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String phoneNumber;
    @Column
    private String password;
    @Column
    private String avatar;
    @Column
    private String gender;
    @Column
    private String address;
    @Column
    private LocalDate dob;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<UserRole> userRoles;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Token> tokens;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<StaffSchedule> staffSchedules;
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Review> reviews;
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<BillingAddress> billingAddresses;
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<HotelFavourite> hotelFavourites;
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Hotel> hotels;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userRoles.stream()
                .flatMap(userRole -> {
                    Set<GrantedAuthority> roleAuthorities = userRole.getRole().getPermissionRoles().stream()
                            .map(permission -> new SimpleGrantedAuthority(permission.getPermission().getPermissionCode()))
                            .collect(Collectors.toSet());

                    // Aggiungi l'autorità del ruolo (con prefisso "ROLE_")
                    roleAuthorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getRole().getRoleName()));
                    return roleAuthorities.stream();
                })
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @PreRemove
    private void removeRolesBeforeRemoveUser() {
        userRoles.clear();
    }
}
