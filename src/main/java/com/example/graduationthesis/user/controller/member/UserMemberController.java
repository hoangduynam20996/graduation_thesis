package com.example.graduationthesis.user.controller.member;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.constant.UserConstant;
import com.example.graduationthesis.user.dto.request.UserUpdateRequest;
import com.example.graduationthesis.user.dto.response.UserResponse;
import com.example.graduationthesis.user.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + UserConstant.API_USER)
@RequiredArgsConstructor
@Tag(name = "User member controller")
public class UserMemberController {

    private final UserService userService;

    @Operation(summary = "Get profile", description = "Find user by token")
    @GetMapping(UserConstant.API_PROFILE)
    public ResponseEntity<UserResponse> findUserByToken(
            @RequestHeader(name = SystemConstant.AUTHORIZATION) String token
    ) {
        return new ResponseEntity<>(userService.findUserByToken(token), HttpStatus.OK);
    }

    @GetMapping(UserConstant.API_PERMISSIONS)
    public ResponseEntity<UserResponse> findPermissionsByToken(
            @RequestHeader(name = SystemConstant.AUTHORIZATION) String token
    ) {
        return new ResponseEntity<>(userService.findPermissionsByToken(token), HttpStatus.OK);
    }

    @GetMapping(UserConstant.API_LOGOUT)
    public ResponseEntity<?> handleLogout() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateUser(
            @RequestHeader(SystemConstant.AUTHORIZATION) String jwtToken,
            @Valid UserUpdateRequest request
    ) {
        return new ResponseEntity<>(userService.updateUser(jwtToken, request), HttpStatus.OK);
    }
}
