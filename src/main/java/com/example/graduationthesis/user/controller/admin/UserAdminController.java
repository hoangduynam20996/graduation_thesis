package com.example.graduationthesis.user.controller.admin;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.constant.UserConstant;
import com.example.graduationthesis.user.dto.response.UserResponses;
import com.example.graduationthesis.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + UserConstant.API_USER)
@RequiredArgsConstructor
public class UserAdminController {

    private final UserService userService;

    @PreAuthorize("hasAnyAuthority(@adminMaster,@adminRead)")
    @GetMapping
    public ResponseEntity<UserResponses> selectAllUser(
            @RequestParam("isHotelOwner") Optional<Boolean> isHotelOwner,
            @RequestParam("status") Optional<String> status,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(userService.findAll(
                isHotelOwner.orElse(false),
                status.orElse(SystemConstant.STATUS_ACTIVE),
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK);
    }
}
