package com.example.graduationthesis.user.controller.publicapi;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.constant.UserConstant;
import com.example.graduationthesis.user.dto.request.EmailRequest;
import com.example.graduationthesis.user.dto.request.UserLoginRequest;
import com.example.graduationthesis.user.dto.request.UserRegisterRequest;
import com.example.graduationthesis.user.dto.response.UserResponse;
import com.example.graduationthesis.user.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + UserConstant.API_USER)
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping(UserConstant.API_OAUTH2_SUCCESS)
    public ResponseEntity<?> authenticate(
            @AuthenticationPrincipal OAuth2User oauth2User
    ) {
        return new ResponseEntity<>(userService.authenticate(oauth2User), HttpStatus.OK);
    }

    @GetMapping(UserConstant.API_OAUTH2_FAIL)
    public ResponseEntity<String> fail() {
        return new ResponseEntity<>(userService.responseLoginOAuth2Fail(), HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(UserConstant.API_LOGIN)
    public ResponseEntity<UserResponse> authenticated(@Valid @RequestBody UserLoginRequest request) {
        return new ResponseEntity<>(userService.authenticate(request), HttpStatus.OK);
    }

    @GetMapping(UserConstant.API_PROFILE)
    public ResponseEntity<UserResponse> findUserByToken(@RequestHeader(name = SystemConstant.AUTHORIZATION) String token) {
        return new ResponseEntity<>(userService.findUserByToken(token), HttpStatus.OK);
    }

    @GetMapping(UserConstant.API_PERMISSIONS)
    public ResponseEntity<UserResponse> findPermissionsByToken(
            @RequestHeader(name = SystemConstant.AUTHORIZATION) String token
    ) {
        return new ResponseEntity<>(userService.findPermissionsByToken(token), HttpStatus.OK);
    }

    @PostMapping(UserConstant.API_REGISTER + "/{key}")
    public ResponseEntity<?> registrationHotelOwner(
            @PathVariable("key") Boolean key,
            @Validated @RequestBody UserRegisterRequest request
    ) {
        return new ResponseEntity<>(userService.registerUser(key, request), HttpStatus.CREATED);
    }

    @GetMapping(UserConstant.API_CONFIRM)
    public ResponseEntity<?> confirmUser(
            @RequestParam("token") String token,
            @RequestParam("owner") Boolean isOwner
    ) {
        return new ResponseEntity<>(userService.confirmationToken(token, isOwner), HttpStatus.OK);
    }

    @PostMapping(UserConstant.API_FORGOT_PASSWORD)
    public ResponseEntity<?> forgotPassword(@RequestParam("email") String email) {
        userService.forgotPassword(email);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/refresh-token")
    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        userService.refreshToken(request, response);
    }

    @PostMapping
    public ResponseEntity<?> checkEmail(@Valid @RequestBody EmailRequest request) {
        return new ResponseEntity<>(userService.checkEmail(request), HttpStatus.OK);
    }
}
