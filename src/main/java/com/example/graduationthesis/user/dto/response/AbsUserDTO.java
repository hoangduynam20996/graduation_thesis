package com.example.graduationthesis.user.dto.response;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsUserDTO extends BaseDTOUtil {

    private String email;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String address;

    private LocalDate dob;

    private String gender;

    private String avatar;

    private String status;

}
