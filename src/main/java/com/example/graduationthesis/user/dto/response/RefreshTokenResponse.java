package com.example.graduationthesis.user.dto.response;

public record RefreshTokenResponse(
        String accessToken,
        Long expiration
) {
}
