package com.example.graduationthesis.user.dto.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRegisterRequest {

    @Email(
            message = "regex email",
            regexp = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                    + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
    )
    private String email;
    @Pattern(
            message = "regex firstname",
            regexp = "^[a-zA-Z\\s]+$"
    )
    private String firstName;
    @Pattern(
            message = "regex lastname",
            regexp = "^[a-zA-Z\\s]+$"
    )
    private String lastName;
    @Pattern(
            message = "regex phone number",
            regexp = "^0\\d{9}$"
    )
    private String phoneNumber;
    @NotNull
    @Pattern(
            message = "regex password",
            regexp = "^.*(?=.{6,})(?=.+[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"
    )
    private String password;

}
