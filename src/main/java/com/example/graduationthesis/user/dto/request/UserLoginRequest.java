package com.example.graduationthesis.user.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginRequest {
    @NotNull(message = "Email not null!")
    private String email;
    @NotNull(message = "Password not null!")
    private String password;
}
