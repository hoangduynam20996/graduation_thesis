package com.example.graduationthesis.user.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class UsersDTOResp extends AbsUserDTO {
}