package com.example.graduationthesis.user.service;

import com.example.graduationthesis.user.dto.request.EmailRequest;
import com.example.graduationthesis.user.dto.request.UserLoginRequest;
import com.example.graduationthesis.user.dto.request.UserRegisterRequest;
import com.example.graduationthesis.user.dto.request.UserUpdateRequest;
import com.example.graduationthesis.user.dto.response.UserResponse;
import com.example.graduationthesis.user.dto.response.UserResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.io.IOException;
import java.util.UUID;

public interface UserService {

    UserResponse registerUser(Boolean key, UserRegisterRequest request);

    UserResponse authenticate(UserLoginRequest request);

    UserResponse findUserByToken(String token);

    UserResponse findPermissionsByToken(String token);

    UserResponse authenticate(OAuth2User request);

    String responseLoginOAuth2Fail();

    UserResponse confirmationToken(String token, Boolean isOwner);

    void updateStatusByEmail(String email);

    void forgotPassword(String email);

    UserResponses findAll(Boolean isHotelOwner,String status, Integer currentPage, Integer limitPage);

    boolean existsUserByEmail(String email);

    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;

    UserResponse checkEmail(EmailRequest request);

    UserResponse updateUser(String jwtToken, UserUpdateRequest request);

}
