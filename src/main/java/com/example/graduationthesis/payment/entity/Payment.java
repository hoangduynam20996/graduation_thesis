package com.example.graduationthesis.payment.entity;

import com.example.graduationthesis.billingaddress.BillingAddress;
import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_payment")
public class Payment extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private Booking booking;
    @Column
    private LocalDateTime paymentDate;
    @Column
    private BigDecimal paymentAmount;
    @Column
    private String paymentMethod;
    @ManyToOne
    @JoinColumn(name = "billing_address_id")
    private BillingAddress billingAddress;
}
