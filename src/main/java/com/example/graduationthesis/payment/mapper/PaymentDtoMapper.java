package com.example.graduationthesis.payment.mapper;

import com.example.graduationthesis.payment.dto.PaymentDTO;
import com.example.graduationthesis.payment.entity.Payment;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class PaymentDtoMapper implements Function<Payment, PaymentDTO> {

    @Override
    public PaymentDTO apply(Payment payment) {
        return PaymentDTO.builder()
                .paymentAmount(payment.getPaymentAmount())
                .paymentMethod(payment.getPaymentMethod() != null ? payment.getPaymentMethod() : "No method payment!..")
                .paymentDate(payment.getPaymentDate())
                .status(payment.getStatus())
                .build();
    }
}
