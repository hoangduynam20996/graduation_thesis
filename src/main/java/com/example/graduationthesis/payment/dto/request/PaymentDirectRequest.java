package com.example.graduationthesis.payment.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class PaymentDirectRequest {

    private UUID hid;

    private UUID bid;

    private BigDecimal amount;

    private String txt_billing_mobile;

    private String txt_billing_email;

    private String txt_billing_fullname;
}
