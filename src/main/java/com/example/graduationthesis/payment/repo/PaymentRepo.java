package com.example.graduationthesis.payment.repo;

import com.example.graduationthesis.payment.dto.response.PaymentRevenueDay;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueMonth;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueYear;
import com.example.graduationthesis.payment.entity.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepo extends JpaRepository<Payment, UUID> {

    @Query("""
                SELECT
                    new com.example.graduationthesis.payment.dto.response.PaymentRevenueDay(
                       CAST(DAY(p.paymentDate) AS INTEGER),
                       CAST(MONTH(p.paymentDate) AS INTEGER),
                       CAST(YEAR(p.paymentDate) AS INTEGER),
                       CAST(SUM(p.paymentAmount) AS DOUBLE)
                    )
                FROM
                    Payment p
                INNER JOIN
                    BillingAddress b ON b.id = p.billingAddress.id
                WHERE
                    MONTH(p.paymentDate) = ?1
                AND
                    YEAR(p.paymentDate) = ?2
                AND
                    b.hotel.id = ?3
                GROUP BY
                    CAST(DAY(p.paymentDate) AS INTEGER),
                    CAST(MONTH(p.paymentDate) AS INTEGER),
                    CAST(YEAR(p.paymentDate) AS INTEGER)
            """)
    Optional<List<PaymentRevenueDay>> revenueReportByDay(Integer month, Integer year, UUID hotelId);

    @Query("""
                SELECT
                    new com.example.graduationthesis.payment.dto.response.PaymentRevenueMonth(
                       CAST(MONTH(p.paymentDate) AS INTEGER),
                       CAST(YEAR(p.paymentDate) AS INTEGER),
                       CAST(SUM(p.paymentAmount) AS DOUBLE)
                    )
                FROM
                    Payment p
                 INNER JOIN
                    BillingAddress b ON b.id = p.billingAddress.id
                WHERE
                    YEAR(p.paymentDate) = ?1
                AND
                    b.hotel.id = ?2
                GROUP BY
                    CAST(MONTH(p.paymentDate) AS INTEGER),
                    CAST(YEAR(p.paymentDate) AS INTEGER)
            """)
    Optional<List<PaymentRevenueMonth>> revenueReportByMonth(Integer year, UUID hotelId);

    @Query("""
                SELECT
                    new com.example.graduationthesis.payment.dto.response.PaymentRevenueYear(
                       CAST(YEAR(p.paymentDate) AS INTEGER),
                       CAST(SUM(p.paymentAmount) AS DOUBLE)
                    )
                FROM
                    Payment p
                 INNER JOIN
                    BillingAddress b ON b.id = p.billingAddress.id
                WHERE
                    b.hotel.id = ?1
                GROUP BY
                    CAST(YEAR(p.paymentDate) AS INTEGER)
            """)
    Optional<List<PaymentRevenueYear>> yearlySummaryStatistics(UUID hotelId);

    Page<Payment> findPaymentsByBookingId(UUID bookingId, Pageable pageable);
}
