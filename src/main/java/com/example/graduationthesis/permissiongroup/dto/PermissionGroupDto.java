package com.example.graduationthesis.permissiongroup.dto;

import com.example.graduationthesis.permission.dto.PermissionDto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
public class PermissionGroupDto {

    private UUID permissionGroupId;

    private String permissionGroupName;

    private Boolean isDeleted;

    private String description;

    private List<PermissionDto> permissions;
}
