package com.example.graduationthesis.permissiongroup.entity;

import com.example.graduationthesis.group.entity.Group;
import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_permission_group")
@Entity
public class PermissionGroup extends BaseEntityUtil {
    @ManyToOne
    @JoinColumn(name = "permission_id")
    private Permission permission;
    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    public PermissionGroup(Group group, Permission permission) {
        this.group = group;
        this.permission = permission;
    }
}
