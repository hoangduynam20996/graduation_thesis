package com.example.graduationthesis.permissiongroup.controller.admin;

import com.example.graduationthesis.permissiongroup.service.PermissionGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class PermissionGroupAdminController {

    private final PermissionGroupService permissionGroupService;
}
