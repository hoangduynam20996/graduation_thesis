package com.example.graduationthesis.hotelpolicy.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HotelPolicyAddRequest {

    private String bookingPolicy;

    private String cancellationPolicy;

    private String petPolicy;

    private String smokingPolicy;

    private String additionalPolices;
}
