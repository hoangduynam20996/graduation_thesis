package com.example.graduationthesis.hotelpolicy.dto.response;

import com.example.graduationthesis.utils.BaseResponsesUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class HotelPolicyResponses extends BaseResponsesUtil {
}
