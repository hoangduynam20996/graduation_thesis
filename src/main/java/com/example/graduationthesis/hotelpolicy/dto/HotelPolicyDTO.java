package com.example.graduationthesis.hotelpolicy.dto;

import com.example.graduationthesis.utils.BaseDTOIDUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class HotelPolicyDTO extends BaseDTOIDUtil {

    private String bookingPolicy;

    private String cancellationPolicy;

    private String petPolicy;

    private String smokingPolicy;

    private String additionalPolices;
}
