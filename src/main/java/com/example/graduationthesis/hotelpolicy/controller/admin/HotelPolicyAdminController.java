package com.example.graduationthesis.hotelpolicy.controller.admin;

import com.example.graduationthesis.constant.HotelPolicyConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotelpolicy.dto.request.HotelPolicyAddRequest;
import com.example.graduationthesis.hotelpolicy.dto.request.HotelPolicyUpdateRequest;
import com.example.graduationthesis.hotelpolicy.dto.response.HotelPolicyResponse;
import com.example.graduationthesis.hotelpolicy.dto.response.HotelPolicyResponses;
import com.example.graduationthesis.hotelpolicy.service.HotelPolicyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + HotelPolicyConstant.API_HOTEL_POLICY)
@RequiredArgsConstructor
public class HotelPolicyAdminController {

    private final HotelPolicyService hotelPolicyService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<HotelPolicyResponse> addHotelPolicy(
            @RequestHeader("jwtToken") String jwtToken,
            @Valid @RequestBody HotelPolicyAddRequest request
    ) {
        return new ResponseEntity<>(hotelPolicyService.addHotelPolicy(jwtToken, request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping(SystemConstant.API_ADD_NEW)
    public ResponseEntity<HotelPolicyResponse> addHotelPolicyNew(
            @Valid @RequestBody HotelPolicyAddRequest request
    ) {
        hotelPolicyService.addHotelPolicyNew(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping("/{hpid}")
    public ResponseEntity<HotelPolicyResponse> updateHotelPolicy(
            @Valid @RequestBody HotelPolicyUpdateRequest request,
            @PathVariable("hpid") UUID hpid
    ) {
        hotelPolicyService.updateHotelPolicy(request, hpid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @DeleteMapping("/{hpid}")
    public ResponseEntity<HotelPolicyResponse> addHotelPolicy(@PathVariable("hpid") UUID hpid) {
        hotelPolicyService.deleteHotelPolicy(hpid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<HotelPolicyResponses> findAllHotelPolicy(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(hotelPolicyService.findAll(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

}
