package com.example.graduationthesis.hotelpolicy.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotelpolicy.dto.HotelPolicyDTO;
import com.example.graduationthesis.hotelpolicy.dto.request.HotelPolicyAddRequest;
import com.example.graduationthesis.hotelpolicy.dto.request.HotelPolicyUpdateRequest;
import com.example.graduationthesis.hotelpolicy.dto.response.HotelPolicyResponse;
import com.example.graduationthesis.hotelpolicy.dto.response.HotelPolicyResponses;
import com.example.graduationthesis.hotelpolicy.entity.HotelPolicy;
import com.example.graduationthesis.hotelpolicy.mapper.HotelPolicyDTOMapper;
import com.example.graduationthesis.hotelpolicy.repo.HotelPolicyRepo;
import com.example.graduationthesis.hotelpolicy.service.HotelPolicyService;
import com.example.graduationthesis.session.common.SessionResponseUtil;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySession;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySessionRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.SessionServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class HotelPolicyServiceImpl implements HotelPolicyService {

    private final HotelPolicyRepo hotelPolicyRepo;

    private final HotelPolicyDTOMapper hotelPolicyDTOMapper;

    private final SessionServiceUtil sessionServiceUtil;

    private final BaseAmenityUtil baseAmenityUtil;

    private final HotelPolicySessionRepo hotelPolicySessionRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public HotelPolicyResponse addHotelPolicy(String jwtToken, HotelPolicyAddRequest request) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        hotelPolicySessionRepo.findHotelPolicySessionByJwtToken(jwtToken).ifPresent(hotelPolicySessionRepo::delete);
        HotelPolicySession hotelPolicySession = hotelPolicySessionRepo.save(
                HotelPolicySession.builder()
                        .bookingPolicy(request.getBookingPolicy())
                        .cancellationPolicy(request.getCancellationPolicy())
                        .petPolicy(request.getPetPolicy())
                        .smokingPolicy(request.getSmokingPolicy())
                        .additionalPolices(request.getAdditionalPolices())
                        .jwtToken(jwtToken)
                        .build()
        );

        return HotelPolicyResponse.builder()
                .code(ResourceConstant.HT_PLC_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("HOTEL_POLICY", hotelPolicySession))
                .message(getMessageBundle(ResourceConstant.HT_PLC_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelPolicyResponse addHotelPolicyNew(HotelPolicyAddRequest request) {
        HotelPolicy policy = getHotelPolicy(request);

        Map<String, Object> value = sessionServiceUtil.getValueMap(SystemConstant.SESSION_REGISTER_HOTEL_OWNER_MORE);

        value.put(SystemConstant.HOTEL_POLICY_REGISTER_CURRENT_MORE, policy);
        sessionServiceUtil.removeValueAndPutValue(SystemConstant.SESSION_REGISTER_HOTEL_OWNER_MORE, value);
        return HotelPolicyResponse.builder()
                .code(ResourceConstant.HT_PLC_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelPolicyDTOMapper.apply(policy))
                .message(getMessageBundle(ResourceConstant.HT_PLC_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelPolicyResponse updateHotelPolicy(HotelPolicyUpdateRequest request, UUID hpid) {
        return null;
    }

    @Override
    public HotelPolicyResponse deleteHotelPolicy(UUID hpid) {
        HotelPolicy hotelPolicy = hotelPolicyRepo.findById(hpid)
                .orElseThrow(() -> new ApiRequestException("find hotel policy by id " + hpid + " not found!.", ""));
        hotelPolicy.setIsDeleted(SystemConstant.NO_ACTIVE);
        hotelPolicy.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        HotelPolicy save = hotelPolicyRepo.save(hotelPolicy);
        return HotelPolicyResponse.builder()
                .code(ResourceConstant.HT_PLC_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(hotelPolicyDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.HT_PLC_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public HotelPolicyResponses findAll(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<HotelPolicy> hotelPolicies = hotelPolicyRepo.findAll(pageable);
        List<HotelPolicyDTO> list = hotelPolicies
                .stream()
                .map(hotelPolicyDTOMapper)
                .toList();
        return HotelPolicyResponses.builder()
                .code(ResourceConstant.HT_PLC_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message(getMessageBundle(ResourceConstant.HT_PLC_004))
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, hotelPolicies.getTotalPages()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private HotelPolicy getHotelPolicy(HotelPolicyAddRequest request) {
        return HotelPolicy.builder()
                .bookingPolicy(request.getBookingPolicy())
                .cancellationPolicy(request.getCancellationPolicy())
                .petPolicy(request.getPetPolicy())
                .smokingPolicy(request.getSmokingPolicy())
                .additionalPolices(request.getAdditionalPolices())
                .isDeleted(SystemConstant.ACTIVE)
                .status(SystemConstant.STATUS_ACTIVE)
                .build();
    }
}
