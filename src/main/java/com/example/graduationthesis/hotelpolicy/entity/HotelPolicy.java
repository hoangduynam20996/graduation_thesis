package com.example.graduationthesis.hotelpolicy.entity;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_hotel_policy")
public class HotelPolicy extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @Column
    private String bookingPolicy;
    @Column
    private String cancellationPolicy;
    @Column
    private String petPolicy;
    @Column
    private String smokingPolicy;
    @Column
    private String additionalPolices;
}
