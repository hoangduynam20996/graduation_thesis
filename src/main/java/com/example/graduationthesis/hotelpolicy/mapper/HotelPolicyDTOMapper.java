package com.example.graduationthesis.hotelpolicy.mapper;

import com.example.graduationthesis.hotelpolicy.dto.HotelPolicyDTO;
import com.example.graduationthesis.hotelpolicy.entity.HotelPolicy;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class HotelPolicyDTOMapper implements
        Function<HotelPolicy, HotelPolicyDTO> {

    private final ModelMapper mapper;

    @Override
    public HotelPolicyDTO apply(HotelPolicy hotelPolicy) {
        return mapper.map(hotelPolicy, HotelPolicyDTO.class);
    }
}
