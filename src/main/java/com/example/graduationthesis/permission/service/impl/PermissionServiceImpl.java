package com.example.graduationthesis.permission.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.group.entity.Group;
import com.example.graduationthesis.group.repo.GroupRepo;
import com.example.graduationthesis.permission.dto.PermissionDto;
import com.example.graduationthesis.permission.dto.request.PermissionRequest;
import com.example.graduationthesis.permission.dto.request.PermissionUpdateRequest;
import com.example.graduationthesis.permission.dto.response.PermissionResponse;
import com.example.graduationthesis.permission.dto.response.PermissionResponses;
import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.permission.mapper.PermissionDtoMapper;
import com.example.graduationthesis.permission.repo.PermissionRepo;
import com.example.graduationthesis.permission.service.PermissionService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.PageableResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepo permissionRepo;

    private final PermissionDtoMapper permissionDtoMapper;

    private final GroupRepo groupRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    private Permission getPermission(UUID permissionId) {
        return permissionRepo.findById(permissionId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.PMS_002, getMessageBundle(ResourceConstant.PMS_002)));
    }

    private Group getGroup(UUID groupId) {
        return groupRepo.findById(groupId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.GR_001, getMessageBundle(ResourceConstant.GR_001)));
    }

    @Override
    public PermissionResponses addPermission(PermissionRequest request) {
        request.getPermissionCodes()
                .forEach(item -> {
                    StringBuilder sb = new StringBuilder();
                    if (permissionRepo.existsByPermissionCode(item.permissionCode())) {
                        sb.append(" ");
                        sb.append(item.permissionCode());
                        sb.append(", ");
                    }
                    if (!sb.isEmpty()) {
                        throw new ApiRequestException(ResourceConstant.PMS_011, getMessageBundle(ResourceConstant.PMS_011) + sb.toString());
                    }
                });

        List<Permission> permissions = request.getPermissionCodes().stream()
                .map(item -> new Permission(
                        item.permissionCode(),
                        SystemConstant.ACTIVE,
                        SystemConstant.STATUS_ACTIVE,
                        item.description()
                ))
                .toList();
        permissionRepo.saveAll(permissions);

        return PermissionResponses.builder()
                .code(ResourceConstant.PMS_003)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(null)
                .message(getMessageBundle(ResourceConstant.PMS_003))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PermissionResponse updatePermission(PermissionUpdateRequest request) {
        Group group = getGroup(request.getPermissionGroupId());

        Permission permission = getPermission(request.getPermissionId());

        if (!permission.getPermissionCode().equals(request.getPermissionCode())) {
            permission.setPermissionCode(request.getPermissionCode());
        }

        if (!permission.getDescription().equals(request.getDescription())) {
            permission.setDescription(request.getDescription());
        }

        Permission permissionSave = permissionRepo.save(permission);

        return PermissionResponse.builder()
                .code(ResourceConstant.PMS_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionDtoMapper.apply(permissionSave))
                .message(getMessageBundle(ResourceConstant.PMS_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PermissionResponse deletePermission(UUID permissionId) {
        Permission permission = getPermission(permissionId);
        permission.setIsDeleted(SystemConstant.ACTIVE);
        Permission permissionSave = permissionRepo.save(permission);

        return PermissionResponse.builder()
                .code(ResourceConstant.PMS_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionDtoMapper.apply(permissionSave))
                .message(getMessageBundle(ResourceConstant.PMS_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public PermissionResponses findAllPermission(String status, Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Permission> all = permissionRepo.findAllByStatusAndNotPermissionCodeMaster(status, pageable);

        List<PermissionDto> permissionDtos = all.stream()
                .map(permissionDtoMapper)
                .toList();

        PageableResponseUtil pageableResponse = baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages());

        return PermissionResponses.builder()
                .code(ResourceConstant.PMS_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionDtos)
                .meta(pageableResponse)
                .message(getMessageBundle(ResourceConstant.PMS_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }
}
