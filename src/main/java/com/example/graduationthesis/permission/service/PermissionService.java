package com.example.graduationthesis.permission.service;


import com.example.graduationthesis.permission.dto.request.PermissionRequest;
import com.example.graduationthesis.permission.dto.request.PermissionUpdateRequest;
import com.example.graduationthesis.permission.dto.response.PermissionResponse;
import com.example.graduationthesis.permission.dto.response.PermissionResponses;

import java.util.UUID;

public interface PermissionService {

    PermissionResponses addPermission(PermissionRequest request);

    PermissionResponse updatePermission(PermissionUpdateRequest request);

    PermissionResponse deletePermission(UUID permissionId);

    PermissionResponses findAllPermission(String status, Integer currentPage, Integer litMitPage);
}
