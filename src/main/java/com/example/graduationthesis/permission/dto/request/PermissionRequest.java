package com.example.graduationthesis.permission.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PermissionRequest {

    private List<PermissionCodeRequest> permissionCodes;

    public record PermissionCodeRequest(
            String permissionCode,
            String description
    ) {
    }
}
