package com.example.graduationthesis.permission.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class PermissionDto extends AbsPermissionDto {
}
