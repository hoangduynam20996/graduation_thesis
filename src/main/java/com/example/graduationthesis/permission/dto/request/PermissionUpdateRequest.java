package com.example.graduationthesis.permission.dto.request;

import com.example.graduationthesis.constant.ResourceConstant;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class PermissionUpdateRequest {
    @NotNull(message = ResourceConstant.MSG_ID_001)
    private UUID permissionGroupId;
    @NotNull(message = ResourceConstant.MSG_ID_001)
    private UUID permissionId;
    @NotBlank(message = ResourceConstant.MSG_E_003)
    private String permissionCode;
    @NotBlank(message = ResourceConstant.MSG_E_003)
    private String description;
}
