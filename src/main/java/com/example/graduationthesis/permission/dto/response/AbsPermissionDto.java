package com.example.graduationthesis.permission.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsPermissionDto {

    private UUID permissionId;

    private String permissionCode;

    private Boolean isDeleted;

    private String status;

    private String description;
}
