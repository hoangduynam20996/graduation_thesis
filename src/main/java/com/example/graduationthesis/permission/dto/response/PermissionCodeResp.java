package com.example.graduationthesis.permission.dto.response;

public record PermissionCodeResp(
        String permissionCode
) {
}
