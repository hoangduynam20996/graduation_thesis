package com.example.graduationthesis.permission.controller.admin;

import com.example.graduationthesis.constant.PermissionConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.permission.dto.request.PermissionRequest;
import com.example.graduationthesis.permission.dto.request.PermissionUpdateRequest;
import com.example.graduationthesis.permission.dto.response.PermissionResponse;
import com.example.graduationthesis.permission.dto.response.PermissionResponses;
import com.example.graduationthesis.permission.service.PermissionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + PermissionConstant.API_PERMISSION)
@RequiredArgsConstructor
@PreAuthorize("hasAnyAuthority('admin_master')")
public class PermissionAdminController {

    private final PermissionService permissionService;

    @GetMapping
    public ResponseEntity<PermissionResponses> findAllPermission(
            @RequestParam(SystemConstant.PARAM_STATUS) Optional<String> status,
            @RequestParam(value = SystemConstant.CURRENT_PAGE, required = false) Optional<Integer> currentPage,
            @RequestParam(value = SystemConstant.LIMIT_PAGE, required = false) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(permissionService.findAllPermission(status.orElse(SystemConstant.STATUS_ACTIVE),
                currentPage.orElse(1), limitPage.orElse(10)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PermissionResponses> addPermission(@RequestBody PermissionRequest request) {
        return new ResponseEntity<>(permissionService.addPermission(request), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<PermissionResponse> updatePermission(
            @Valid @RequestBody PermissionUpdateRequest request
    ) {
        return new ResponseEntity<>(permissionService.updatePermission(request), HttpStatus.OK);
    }

    @DeleteMapping(PermissionConstant.API_PERMISSION_ID)
    public ResponseEntity<PermissionResponse> deletePermission(@PathVariable(PermissionConstant.PATH_PERMISSION_ID) UUID id) {
        return new ResponseEntity<>(permissionService.deletePermission(id), HttpStatus.OK);
    }
}
