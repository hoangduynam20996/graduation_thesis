package com.example.graduationthesis.permission.entity;

import com.example.graduationthesis.permissiongroup.entity.PermissionGroup;
import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_permission")
@Entity
public class Permission extends BaseEntityUtil {

    private String permissionCode;

    private String description;
    @OneToMany(mappedBy = "permission")
    private List<PermissionGroup> permissionGroup;
    @OneToMany(mappedBy = "permission", fetch = FetchType.EAGER)
    private List<PermissionRole> permissionRoles;

    public Permission(String permissionCode, Boolean isDeleted, String status) {
        this.permissionCode = permissionCode;
        super.setIsDeleted(isDeleted);
    }

    public Permission(String permissionCode, Boolean isDeleted, String status, String description) {
        this.permissionCode = permissionCode;
        super.setStatus(status);
        super.setIsDeleted(isDeleted);
        this.description = description;
    }

}
